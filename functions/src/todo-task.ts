import { firestore } from 'firebase-admin';

// import { firestore } from 'firebase/app';

export interface TodoTask {
    name?: string;
    numDone?: number;
    numRep?: number;
    completed?: boolean;
    deadLine?: firestore.Timestamp;
    days?: number[];
    idRoutine?: string;
    percentage?: number;
    reproposed?: boolean;
}
