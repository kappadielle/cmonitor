import { firestore } from 'firebase-admin';

export interface Oiba {
    positive: boolean;
    date: firestore.Timestamp;
    lastTimeUpdated: firestore.Timestamp;
    id?: string;

    situation: string;
    relations?: string[];
    moods: string[];
    perception?: string;
    needs: string[];
    needsId: string[];
    action?: string;
    result?: string;
    disadapted?: boolean;
    empathAlignment?: string;
    perceptiveAlignment?: string;
    mentalPatternID?: string;
    mood_action_result2?: string;
}
