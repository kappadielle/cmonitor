import * as functions from 'firebase-functions';
import * as moment from '../lib/moment-timezone';
import  {TodoTask} from './todo-task';
//import  {Oiba} from './oiba';
// tslint:disable-next-line:no-implicit-dependencies
// import { DocumentReference } from '@google-cloud/firestore';
const admin = require('firebase-admin');
admin.initializeApp();

function getIdForWeekDoc(dayMs: number) {
    return 'year' + moment(dayMs).year() + '_week' + moment(dayMs).weeks();
}
function getIdForMonthDoc(dayMs: number) {
    return 'year' + moment(dayMs).year() + '_month' + moment(dayMs).month();
}
/*
    purpouse: monitoring the tasks completion for every: week, month, 3month, 6month, 1year
    the tendency is to monitor everyday, the yesterday document.
*/
exports.taskMonitor = functions.firestore
    .document('users/{userId}/planning/{todolistId}')
    .onCreate((snap, context) => {
        console.log('dati todolist (' +  context.params.userId + ') appena creata: ', snap.data())
        /*
            toKnowBeforeRead: the trigger is "onCreate(todoList documents)",  these documents can be created:
                1) TODAY FOR TODAY
                2) TODAY FOR TOMORROW
            then we can assume that if today I can create both today and tomorrow, there can be a day that No Document will be created
            for this reason it's useful to have a DB variable that tell us the lastDayMonitored.

            we have to monitor even the days of inactivity with 0 completion, we can know the inactivity days using the lastDayMonitored like this:
                - since every onCreate() monitor the yesterday todolist,
                - since some day may not have called onCreate() => yesterday not monitored,
                - we can check the gap between lastDayMonitored & today to assume that:
                    1) gap == 1day  : today I monitored yesterday,
                    2) gap == 2days : we have to monitor the yesterday day,
                    3) gap == 3days : maybe 1 day of inactivity or maybe the use is steady but 1 day is skipped to create docs cause the day after lastDayMonitored you created docs for 2 days (lastDayMonitored + 1 & lastDayMonitored + 2) => you have to check: yesterday & 2days ago;
                    4) gap >  3days : there's for sure at least 1 day of inactivity. But we have to consider 2 important things: 
                        A) the 2 days after lastDayMonitored aren't inactivity days because lastDayMonitored to be monitored need an onCreate() on the day after lastDayMonitored
                            & that onCreate() could have created a tomorrowDoc that prevents onCreate() to be triggered again the days after (lastDayMonitored + 2days) *check Above;
                        B) yesterday is an inactivity day otherwise the gap between lastDayMonitored & today would have been less then 4.
        */
        // setCyberneticTraining(moment(Number(context.params.todolistId)).utcOffset(offset).day());
        let lastDayMonitored: any;
        let arrayRoutine: any;
        const offset = moment().tz("Europe/Rome").subtract(1, 'day').startOf('day').isDST() ? 2 : 1; // between daylight saving time and winter time
        const yesterdayMs = moment().utcOffset(offset).subtract(1, 'day').startOf('day').valueOf() + '';
        // get routine & lastDayMonitored documents\
        const routineProm = admin.firestore().collection('users').doc(context.params.userId).collection('planning').doc('routine').get()
        const lastDayMonitoredProm = admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('tasks').get()
        return Promise.all([routineProm, lastDayMonitoredProm]).then((docs1) => {

            if (!docs1[1].exists) {
                // lastDayMonitored not setted => set to yesterday even if there's nothing to monitor
                setLastDayMonitored();
                console.log('setting lastDayMonitor 4 first time')
            } else if (docs1[0].exists && docs1[1].exists) {
                console.log(docs1[1].data())
                // let's Go
                arrayRoutine     = docs1[0].data().todolist;
                lastDayMonitored = docs1[1].data().lastDayMonitored;
                const gap = gapFromToday(lastDayMonitored.seconds);
                if (gap == 1) {
                    // all done / nothing to do
                    console.log('yesterday already setted');
                } else if (gap === 2) {
                    // monitor the yesterday day
                    admin.firestore().collection('users').doc(context.params.userId).collection('planning').doc(yesterdayMs).get().then((doc: any) => {
                        if (doc.exists) {
                            setTaskList(doc.data().todolist, Number(yesterdayMs));
                            setLastDayMonitored();
                        } else {
                            console.log("No such document!");
                        }
                    })
                } else if (gap >= 3) {
                    // try to get lastDayMonitored + 1 day Document && lastDayMonitored + 2 days Document & update
                    const oneDayafterLastMonitored  = admin.firestore().collection('users').doc(context.params.userId).collection('planning').doc(moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(1, 'days').valueOf() + '').get()
                    const twoDaysafterLastMonitored = admin.firestore().collection('users').doc(context.params.userId).collection('planning').doc(moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(2, 'days').valueOf() + '').get()
                    // tslint:disable-next-line: no-floating-promises
                    Promise.all([oneDayafterLastMonitored, twoDaysafterLastMonitored]).then((docs) => {
                        if (docs[0].exists && docs[1].exists) {
                            setTaskList(docs[0].data().todolist, moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(1, 'days').valueOf())
                            setTaskList(docs[1].data().todolist, moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(2, 'days').valueOf());
                        } else if (!docs[0].exists && docs[1].exists) {
                            setTaskListEmpty(moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(1, 'days').valueOf(), arrayRoutine);
                            setTaskList(docs[1].data().todolist, moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(2, 'days').valueOf());
                        } else if (docs[0].exists && !docs[1].exists) {
                            setTaskList(docs[0].data().todolist, moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(1, 'days').valueOf());
                            setTaskListEmpty(moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(2, 'days').valueOf(), arrayRoutine);
                        } else if (!docs[0].exists && !docs[1].exists) {
                            setTaskListEmpty(moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(1, 'days').valueOf(), arrayRoutine);
                            setTaskListEmpty(moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(2, 'days').valueOf(), arrayRoutine);
                        }
                    });
                    if (gap > 3) {
                        // fill with 0 completion object the days between (lastDayMonitored + 2) & today
                        // we do 1 call at time cause the name of the docId can change between different dates => avoid to accumulate data in obj
                        for(let i = 3; i < gap; i++) {
                            setTaskListEmpty(moment.unix(lastDayMonitored.seconds).utcOffset(offset).add(i, 'days').valueOf(), arrayRoutine);
                        }
                    }
                    setLastDayMonitored();
            } else {
                console.log("No such document! Nothing to monitor");
            }
        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    })

    // get the number of seconds of the date & return days of gap from today
    function gapFromToday(lastDayMonitor: number): number {
        const lastDay = moment.unix(lastDayMonitor).utcOffset(offset).startOf('day');
        const today   = moment().utcOffset(offset).startOf('day');
        const gap = moment.duration(today.subtract(moment.duration(lastDay.valueOf(), 'ms')).valueOf(), 'ms').asDays();
        //console.log('lastDay subtract 1 day', today.subtract(moment.duration(lastDay.valueOf(), 'ms').asDays(), 'days').valueOf())
        console.log('er gap: ', gap);
        return gap;
    }
    /* 
        params: todolist of the day, routine of the week, ms of the day of the first arg.
        1) it sets the statsWeek & statsMOnth in the DB
        2) it sets the not routine & not completed task to today todolist
    */
    function setTaskList(arrayDay :TodoTask[], dayMs: number) {
        const statsWeek: any = {};
        const statsMonth: any = {};
        const arrayStats: TodoTask[] = []
        const arrayNotCompleted: TodoTask[] = []
        arrayDay.forEach(task => {
            if (task.idRoutine) {
                let percent = 0;
                let numDone = 0;
                let numRep = 0;
                if (task.percentage) { // cause not completed doesn't have percentage
                    percent = task.percentage;
                } else {
                    numDone = task.numDone ? task.numDone : 0;
                    numRep = task.numRep ? task.numRep : 1;
                    percent = (numDone / numRep) * 100;
                }
                arrayStats.push({name: task.name, completed: numDone > 0 || task.completed ? true : false, percentage: percent});
            }
            // fill the  the not-completed task to today todolist
            if (!task.completed && !task.idRoutine) {
                task.reproposed = true;
                arrayNotCompleted.push(task);
            }
        });
        if (arrayNotCompleted.length) {
            admin.firestore().collection('users').doc(context.params.userId).collection('planning').doc(moment().utcOffset(offset).startOf('day').valueOf() + '').update({todolist: admin.firestore.FieldValue.arrayUnion(...arrayNotCompleted)});
            console.log('re-proposed task for ' + context.params.userId , arrayNotCompleted);
        }

        statsWeek['day' + moment(dayMs).utcOffset(offset).day()] = arrayStats;
        statsMonth['day' + moment(dayMs).utcOffset(offset).date()] = arrayStats;
        console.log('uploading new stats for: ' + context.params.userId , statsWeek);
        const batch = admin.firestore().batch();
        batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('tasks').collection('week').doc(getIdForWeekDoc(Number(dayMs))), statsWeek, {merge: true});
        batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('tasks').collection('month').doc(getIdForMonthDoc(Number(dayMs))), statsMonth, {merge: true});
        batch.commit();
    }
    /* 
        params:  ms of the day to monitor, routine of the week.
        it sets the statsWeek with an empty day completion in the DB
    */
    function setTaskListEmpty(dayMs: number, arrRoutine: TodoTask[]) {
        const statsWeek: any = {};
        const statsMonth: any = {};
        const day = moment(dayMs).utcOffset(offset).day();
        const dateOfMonth = moment(dayMs).utcOffset(offset).date();
        const arrEmpty: TodoTask[] = [];
        arrRoutine.forEach((task: TodoTask) => {
            if (task.days && task.days.some(val => val == day)) {
                arrEmpty.push({name: task.name, completed: false, percentage: 0});
            }
        });
        statsWeek['day'  + day] = arrEmpty;
        statsMonth['day' + dateOfMonth] = arrEmpty;
        console.log('uploading new stats for: ' + context.params.userId , statsWeek);
        const batch = admin.firestore().batch();
        batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('tasks').collection('week').doc(getIdForWeekDoc(Number(dayMs))), statsWeek, {merge: true});
        batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('tasks').collection('month').doc(getIdForMonthDoc(Number(dayMs))), statsMonth, {merge: true});
        batch.commit();
    }
    
    function setLastDayMonitored() {

        //set last Day
        admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('tasks').set({
            // lastDayMonitored: {seconds: moment().utcOffset(offset).subtract(1, 'days').startOf('day').unix()}, // timestamp of yesterday starting from day
            lastDayMonitored: moment().utcOffset(offset).subtract(1, 'days').startOf('day').toDate()
        }, {merge: true})
    }
    /*
    function setCyberneticTraining(day: any) {
        // days == 2 because every day creates tomorrow :)
        if(day == 2 && (context.params.userId == 'DPshcQVB7IS2nulDuQBvC7hGTdw1' || context.params.userId == '8EvTXtJrPJckyvNiXGaWS3jdytx1')) {
            admin.firestore().collection('cybernetic').doc('training').get().then((doc: any) => {
                if (doc.exists) {
                    let list = doc.data().list;
                    list = list.filter((val: any) => !val.notATask)
                    list.map((val: any) => {
                        val.numRep = Number(val.num);
                        val.routine = null;
                        val.completed = false;
                        val.numDone = 0;
                        delete val.num;
                    })
                    console.log('uploaded cybernetic tasks', list)
                    admin.firestore().collection('users').doc(context.params.userId).collection('planning').doc(moment().utcOffset(offset).startOf('day').valueOf() + '').update({todolist: admin.firestore.FieldValue.arrayUnion(...list)});
                } else {
                    console.log("No such document!");
                }
            })
        }
    }
    */
});
/*
    Every oiba update, it incremenet/decrement all the values (mentalPatttern, numSatisfied, moods, relations...)
*/
exports.OibaUpdateMonitor = functions.firestore
    .document('users/{userId}/needs/OIBA/list/{OibaId}')
    .onUpdate((change, context) => {

        function arr_differ(arr1, arr2): any[] {
            return arr1.filter(x => !arr2.includes(x))
            .concat(arr2.filter(x => !arr1.includes(x)));
        }

        const decrement = admin.firestore.FieldValue.increment(-1);
        const increment = admin.firestore.FieldValue.increment(1);
        const nothingent = admin.firestore.FieldValue.increment(0);
        const newOiba = change.after.data();
        const oldOiba = change.before.data();
        let statsToUpload = {};
        let moodsStats = {};
        let relationsStats = {};
        let mentalPatternStats: any = null;
        
        if (newOiba && oldOiba) {
            const needsDiff = arr_differ(newOiba.needsId, oldOiba.needsId);

            const needsRemoved   = needsDiff.filter(need => oldOiba.needsId.includes(need));
            const needsAdded     = needsDiff.filter(need => newOiba.needsId.includes(need));
            const needsUnchanged = newOiba.needsId.filter(need => oldOiba.needsId.includes(need));
            
            const moodsDiff      = arr_differ(newOiba.moods, oldOiba.moods);
            const moodsRemoved   = moodsDiff.filter(mood => oldOiba.moods.includes(mood));
            const moodsAdded     = moodsDiff.filter(mood => newOiba.moods.includes(mood));
            const moodsUnchanged = newOiba.moods.filter(mood => oldOiba.moods.includes(mood));

            const relationsDiff      = arr_differ(newOiba.relations, oldOiba.relations);
            const relationsRemoved   = relationsDiff.filter(relation => oldOiba.relations.includes(relation));
            const relationsAdded     = relationsDiff.filter(relation => newOiba.relations.includes(relation));
            const relationsUnchanged = newOiba.relations.filter(relation => oldOiba.relations.includes(relation));

            if (moodsDiff.length || relationsDiff.length || needsDiff.length || oldOiba.mentalPatternID !== newOiba.mentalPatternID) {
                console.log('data : ', {
                    need: {
                        unchanged: needsUnchanged,
                        removed: needsRemoved,
                        added: needsAdded
                    },
                    mood: {
                        unchanged: moodsUnchanged,
                        removed: moodsRemoved,
                        added: moodsAdded
                    },
                    relation: {
                        unchanged: relationsUnchanged,
                        removed: relationsRemoved,
                        added: relationsAdded
                    },
                })

                // needs added => increment every stats (new snapshot, the data that are coming) for the needs added
                newOiba.moods.forEach(mood => {
                    moodsStats = {...moodsStats, [mood]: increment};
                })
                newOiba.relations.forEach(relation => {
                    relationsStats = {...relationsStats, [relation]: increment};
                })
                mentalPatternStats = newOiba.mentalPatternID ? {[newOiba.mentalPatternID]: increment} : null;
                statsToUpload = prepareStatsForNeeds(statsToUpload, newOiba.positive, needsAdded, moodsStats, relationsStats, newOiba.date, mentalPatternStats, admin.firestore.FieldValue.increment(1))

                // needs removed => decrement every stats (old snapshot, what it was before change) for the needs removed
                moodsStats = {};
                relationsStats = {};
                mentalPatternStats = null;
                oldOiba.moods.forEach(mood => {
                    moodsStats = {...moodsStats, [mood]: decrement};
                })
                oldOiba.relations.forEach(relation => {
                    relationsStats = {...relationsStats, [relation]: decrement};
                })
                mentalPatternStats = oldOiba.mentalPatternID ? {[oldOiba.mentalPatternID]: decrement} : null;
                statsToUpload = prepareStatsForNeeds(statsToUpload, newOiba.positive, needsRemoved, moodsStats, relationsStats, newOiba.date, mentalPatternStats, admin.firestore.FieldValue.increment(-1))

                // needs unchanged => increment/decrement every stats (the stats that are changed (relation&mood&mentalPattern(removed&added))) for every needs unchanged
                moodsStats = {};
                relationsStats = {};
                mentalPatternStats = null;
                moodsRemoved.forEach(mood => {
                    moodsStats = {...moodsStats, [mood]: decrement};
                });
                moodsAdded.forEach(mood => {
                    moodsStats = {...moodsStats, [mood]: increment};
                });
                moodsUnchanged.forEach(mood => {
                    moodsStats = {...moodsStats, [mood]: nothingent};
                });
                relationsRemoved.forEach(relation => {
                    relationsStats = {...relationsStats, [relation]: decrement};
                });
                relationsAdded.forEach(relation => {
                    relationsStats = {...relationsStats, [relation]: increment};
                });
                relationsUnchanged.forEach(relation => {
                    relationsStats = {...relationsStats, [relation]: nothingent};
                });

                
                if (oldOiba.mentalPatternID === newOiba.mentalPatternID) {
                    mentalPatternStats = {[newOiba.mentalPatternID]: nothingent}
                } else if (!oldOiba.mentalPatternID) { // added
                    mentalPatternStats = {[newOiba.mentalPatternID]: increment}
                } else if (!newOiba.mentalPatternID) { // removed
                    mentalPatternStats = {[oldOiba.mentalPatternID]: decrement}
                } else { // changed
                    mentalPatternStats = {[oldOiba.mentalPatternID]: decrement, [newOiba.mentalPatternID]: increment}
                }
                statsToUpload = prepareStatsForNeeds(statsToUpload, newOiba.positive, needsUnchanged, moodsStats, relationsStats, newOiba.date, newOiba.mentalPatternID, admin.firestore.FieldValue.increment(0))
                console.log('statsUploaded:  ',statsToUpload)
                const batch = admin.firestore().batch();
                batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('needs').collection('week').doc(getIdForWeekDoc(Number(newOiba.date.seconds * 1000))), statsToUpload, {merge: true});
                batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('needs').collection('month').doc(getIdForMonthDoc(Number(newOiba.date.seconds * 1000))), statsToUpload, {merge: true});
                return batch.commit();
            }
        }
        
    });
    
exports.OibaCreateMonitor = functions.firestore
    .document('users/{userId}/needs/OIBA/list/{OibaId}')
    .onCreate((snap, context) => {
        // const decrement = admin.firestore().FieldValue.increment(-1);
        const increment = admin.firestore.FieldValue.increment(1);
        const newOiba = snap.data();
        let statsToUpload = {};
        let moodsStats = {};
        let relationsStats = {};
        
        if (newOiba) {
            
            newOiba.moods.forEach(mood => {
                moodsStats = {...moodsStats, [mood]: increment};
            })
            newOiba.relations.forEach(relation => {
                relationsStats = {...relationsStats, [relation]: increment};
            })

            statsToUpload = prepareStatsForNeeds(statsToUpload, newOiba.positive, newOiba.needsId, moodsStats, relationsStats, newOiba.date, newOiba.mentalPatternID, admin.firestore.FieldValue.increment(1))
            const batch = admin.firestore().batch();
            batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('needs').collection('week').doc(getIdForWeekDoc(Number(newOiba.date.seconds * 1000))), statsToUpload, {merge: true});
            batch.set(admin.firestore().collection('users').doc(context.params.userId).collection('stats').doc('needs').collection('month').doc(getIdForMonthDoc(Number(newOiba.date.seconds * 1000))), statsToUpload, {merge: true});
            return batch.commit();
        }
    });
    
    /*
        used to create the object to send in the DB, return the obj
        args:
            stats => the object that is merged with the output of the fn
            positive => if the oiba to monitor is positive or neg
            needsId => list of needs to be updated
            moods => list of moods prepared with fieldValue.increment|decremenet
            relations => list of relations prepared with fieldValue.increment|decremenet
            date => to set the lastTimeSatisfied
            mentalPatternID => if there's a mental pattern linked
            add => if true increment -- if false decrement
    */
    function prepareStatsForNeeds(stats :any, positive: boolean, needsId: string[], moods: any, relations: any, date: any, mentalPatternStats: any, action: any) {
        let statsToUpload = stats;
        if (Object.keys(relations).length === 0 && relations.constructor === Object) { // if no relation, don't update relation to avoid overwriting
            if (positive) {
                needsId.forEach(need => {
                    statsToUpload = {...statsToUpload, 
                        [need]: {
                            satisfied: action,
                            lastDateSatisfied: date,
                            moodsPos: moods
                        }
                    }
                })
            } else {
                if (mentalPatternStats) {
                    needsId.forEach(need => {
                        statsToUpload = {...statsToUpload, 
                            [need]: {
                                unsatisfied: action,
                                moodsNeg: moods,
                                mentalPattern: mentalPatternStats // difference between this and else below
                            }
                        }
                    })
                } else {
                    needsId.forEach(need => {
                        statsToUpload = {...statsToUpload, 
                            [need]: {
                                unsatisfied: action,
                                moodsNeg: moods,
                            }
                        }
                    })
                }
            }
        } else { // with relations
            if (positive) {
                needsId.forEach(need => {
                    statsToUpload = {...statsToUpload, 
                        [need]: {
                            satisfied: action,
                            lastDateSatisfied: date,
                            moodsPos: moods,
                            relationsPos: relations,
                        }
                    }
                })
            } else {
                if (mentalPatternStats) {
                    needsId.forEach(need => {
                        statsToUpload = {...statsToUpload, 
                            [need]: {
                                unsatisfied: action,
                                moodsNeg: moods,
                                relationsNeg: relations,
                                mentalPattern: mentalPatternStats // difference between this and else below
                            }
                        }
                    })
                } else {
                    needsId.forEach(need => {
                        statsToUpload = {...statsToUpload, 
                            [need]: {
                                unsatisfied: action,
                                moodsNeg: moods,
                                relationsNeg: relations
                            }
                        }
                    })
                }
            }
        }
        console.log('stats send to DB: ', statsToUpload);
        return statsToUpload;
    }
