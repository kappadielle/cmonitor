import { Component, OnInit, Input } from '@angular/core';
import { MonitorService } from '../../monitor.service';

@Component({
  selector: 'app-behavior',
  templateUrl: './behavior.component.html',
  styleUrls: ['./behavior.component.scss']
})
export class BehaviorComponent implements OnInit {
  @Input() behavior: any;
  habitsArr: any[] = [];
  mentalPatternsArr: any[] = [];
  skillsArr: any[] = [];
  identitiesArr: any[] = [];
  environmentsArr: any[] = [];

  variablesList: any[] = []; // list of variable shown by filtering by type

  modes: any[] = [
    {value: 'toAcquire', viewValue: 'Da Sviluppare'},
    {value: 'toKeep', viewValue: 'Da Continuare'},
    {value: 'toReduce', viewValue: 'Da Ridurre'},
    {value: 'toRemove', viewValue: 'Da Eliminare'},
    {value: 'reduced', viewValue: 'Ridotto'},
    {value: 'removed', viewValue: 'Eliminato'},
  ];
  varSelected: string;
  constructor(public monitorSer: MonitorService) { }

  ngOnInit() {
  }

  /*
    it's used in the template,
    return true||false to the mat-option [disabled] attribute.
  */
  isSelectOptionDisabled(macroType: string, mode: string): boolean {
    if (macroType === 'done') {
      return false;
    } else {
      if (macroType === 'hindering' && (mode === 'toRemove' || mode === 'toReduce')) {
        return false;
      } else if (macroType === 'toImprove' && (mode === 'toAcquire')) {
        return false;
      }
    }
    return true;
  }

  /*
    show variables informations
  */
  expandBehavior() {
    /*
      activated when click on the behavior name
      fetch a behavior from DB when clicked
    */
    if (this.monitorSer.behaviorExpanded !== this.behavior.id) { // if closed
      this.monitorSer.behaviorExpanded = this.behavior.id;
      this.habitsArr         = this.behavior.variables.filter(val => val.type === 'habit');
      this.mentalPatternsArr = this.behavior.variables.filter(val => val.type === 'mentalPattern');
      this.skillsArr         = this.behavior.variables.filter(val => val.type === 'skill');
      this.identitiesArr     = this.behavior.variables.filter(val => val.type === 'identity');
      this.environmentsArr   = this.behavior.variables.filter(val => val.type === 'environment');
    } else {
      this.variablesList = [];
      this.monitorSer.behaviorExpanded = null;
    }
  }

  /*
    activated when click on a type variable button
    it shows the list of variables of that name
  */
  showVarType(list: any[], type: string) {
    if (this.varSelected !== type) {
      // here I should show the variablePicker
      this.variablesList = list;
      this.varSelected = type;
    } else {
      this.variablesList = [];
      this.varSelected = null;
    }
  }
  modifyVarInBehavior(obj: any) {
    if (obj.add) {
      this.behavior.variables.push({...obj.item, type: this.varSelected}); // fake DB update
      this.variablesList.push({...obj.item, type: this.varSelected}); // this does the UI job
    } else {
      this.behavior.variables = this.behavior.variables.filter(val => val.id !== obj.item.id);
      this.variablesList = this.variablesList.filter(val => val.id !== obj.item.id);
    }
  }
}
