import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { MonitorService } from 'src/app/monitor (route)/monitor.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-variable-picker',
  templateUrl: './variable-picker.component.html',
  styleUrls: ['./variable-picker.component.scss']
})
export class VariablePickerComponent implements OnInit {
  @Input() variableType: string;
  @Input() behavior: any;
  @Input() variablesList: any[];
  @Output() moveBagVar = new EventEmitter<any>();
  constructor(public monitorSer: MonitorService) { }

  ngOnInit() {
  }

  moveVariableFromBag(add: boolean, item: any, mode: string) {
    if (add) {
      this.moveBagVar.emit({item: {...item, mode}, add});
    } else {
      this.moveBagVar.emit({item, add});
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    console.log(event)
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
  /*
    return false if the variable in the bag is displayed in the variableList of the behavior open
  */
  attachIcon(id: string) {
    return !this.variablesList.some(val => val.id === id);
  }
}
