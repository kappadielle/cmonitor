import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { MonitorService } from '../monitor.service';

@Component({
  selector: 'app-by-hand-monitor',
  templateUrl: './by-hand-monitor.component.html',
  styleUrls: ['./by-hand-monitor.component.scss']
})
export class ByHandMonitorComponent implements OnInit {
  @Output() editing = new EventEmitter<boolean>();
  constructor(public monitorSer: MonitorService) { }

  ngOnInit() {
    this.emit()
  }
  emit() {
    this.editing.emit(true);
  }
}
