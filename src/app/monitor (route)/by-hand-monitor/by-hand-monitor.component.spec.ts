import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByHandMonitorComponent } from './by-hand-monitor.component';

describe('ByHandMonitorComponent', () => {
  let component: ByHandMonitorComponent;
  let fixture: ComponentFixture<ByHandMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByHandMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByHandMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
