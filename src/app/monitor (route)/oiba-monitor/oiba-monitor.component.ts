import { Component, OnInit, Input, ViewChild, OnChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet, monkeyPatchChartJsTooltip, monkeyPatchChartJsLegend } from 'ng2-charts';
import { NeedsService } from 'src/app/needs (route)/needs.service';
import * as moment from 'moment';

@Component({
  selector: 'app-oiba-monitor',
  templateUrl: './oiba-monitor.component.html',
  styleUrls: ['./oiba-monitor.component.scss'],
})

export class OibaMonitorComponent implements OnInit, OnChanges {
  /*
    this class has the purpouse to show the stats stored by Google Cloud Functions
    ** Listen to @Input changes **
  */
  displayedColumns: string[] = ['name', 'satisfied', 'unsatisfied', 'lastDaySatisfied']; // list of rows in the table
  // tslint:disable-next-line:no-input-rename
  @Input('needDataSource') InputSource: any;
  needDataSource: MatTableDataSource<TableData>; // data for table
  needInfoData: any; // data for needs
  @ViewChild(MatSort, {static: false}) sort: MatSort; // used to make table sorting works; {static: false} cause the data changes
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left'
    }
  };
  // all the pies
  public pieMoodsPosChartLabels: Label[] = [];
  public pieMoodsPosChartData: SingleDataSet = [];
  public pieMoodsNegChartLabels: Label[] = [];
  public pieMoodsNegChartData: SingleDataSet = [];
  public pieRelationsPosChartLabels: Label[] = [];
  public pieRelationsPosChartData: SingleDataSet = [];
  public pieRelationsNegChartLabels: Label[] = [];
  public pieRelationsNegChartData: SingleDataSet = [];

  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  needToShow: string; // id of the need to show the chart
  needNameToShow: string; // name of the need to show the chart
  BVP: any[] = []; // List of BVP to retrieve the name from ID
  constructor(private needSer: NeedsService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit() {
    this.BVP = this.needSer.bisogni.concat(this.needSer.valori).concat(this.needSer.passioni);
    if (!this.BVP.length) {
      this.getNeedsList();
    }
  }
  showNeedInfo(needId: string) {
    this.needToShow = needId;
    this.needInfoData = this.InputSource.needsInfo;
    this.needNameToShow = this.retrieveNameFromNeedID(needId);
    this.updateCharts(this.needInfoData[this.needToShow]);
  }
  private getNeedsList() {
    this.needSer.getNeedsList$().subscribe(doc => { // when the needs come, update the table to avoid to display the id instead of the names
      this.BVP = doc.bisogni.concat(doc.valori).concat(doc.passioni);
      this.updateTable(this.InputSource.table);
    });
  }
  private retrieveNameFromNeedID(id: string): string {
    const index = this.BVP.findIndex(val => val.idNeed === id);
    return index >= 0 ? this.BVP[index].name : id;
  }
  private updateCharts(data: NeedStats) {
    if (data.moodsPos.names.length > 6 || data.moodsNeg.names.length > 6 || data.relationsPos.names.length > 6 || data.relationsNeg.names.length > 6) {
      this.pieChartLegend = false;
    }
    this.pieMoodsPosChartLabels     = data.moodsPos.names;
    this.pieMoodsPosChartData       = data.moodsPos.values;
    this.pieMoodsNegChartLabels     = data.moodsNeg.names;
    this.pieMoodsNegChartData       = data.moodsNeg.values;
    this.pieRelationsPosChartLabels = data.relationsPos.names;
    this.pieRelationsPosChartData   = data.relationsPos.values;
    this.pieRelationsNegChartLabels = data.relationsNeg.names;
    this.pieRelationsNegChartData   = data.relationsNeg.values;
  }
  private updateTable(data: TableData[]) {
    data = data.map(el => {
      return {...el, name: this.retrieveNameFromNeedID(el.id)};
    });
    this.needDataSource = new MatTableDataSource(data);
    this.needDataSource.sort = this.sort;
  }

  ngOnChanges() {
    if (this.InputSource) {
      // update the table
      this.updateTable(this.InputSource.table);

      if (this.needToShow) { // if a need is selected  => update the charts
        this.needInfoData = this.InputSource.needsInfo;
        this.updateCharts(this.needInfoData[this.needToShow]);
      }
    }
  }

}
interface NeedStats {
  moodsPos: ChartData;
  moodsNeg: ChartData;
  relationsPos: ChartData;
  relationsNeg: ChartData;
}
interface ChartData {
  names: Label[];
  values: number [];
}
interface TableData {
  name: string;
  id: string;
  satisfied: number;
  unsatisfied: number;
  lastDaySatisfied: moment.Moment;
}
