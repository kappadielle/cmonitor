import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OibaMonitorComponent } from './oiba-monitor.component';

describe('OibaMonitorComponent', () => {
  let component: OibaMonitorComponent;
  let fixture: ComponentFixture<OibaMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OibaMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OibaMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
