export interface TaskStats {
    name: string;
    val: number;
    max: number;
    percentage: number;
}
