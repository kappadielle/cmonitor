import { Injectable } from '@angular/core';
import { DBManagerService } from '../services/dbmanager.service';
import { AuthenticationService } from '../services/authentication.service';
import { TaskStats } from './task-stats (interface)';
import { Observable, forkJoin } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { take } from 'rxjs/operators';
import { Moment } from 'moment';

@Injectable({
  providedIn: 'root'
})
export class MonitorService {
  /*analisi*/
  variableBag = {
    habit: {
      toAcquire: [
        {name: 'habit is the name', id: 'uudu2ud'},
        {name: 'thieeeeeeame', id: 'uuduu3dsa'},
      ],
      toKeep: [
        {name: 'thissssse name', id: 'uud6uud'},
        {name: 'thiccccaae', id: 'uuduud4sa'},
      ],
      toReduce: [
        {name: 'thissssse name', id: 'uud6uuaaaaaaad'},
        {name: 'thiccccaae', id: 'uuduud4sassssssssss'},
      ],
      toRemove: [
        {name: 'this is the name', id: 'uu6duud'},
        {name: 'ciaoneeeme', id: 'uuduu77dsa'},
      ],
      reduced: [
        {name: 'this pppppp the name', id: 'uud7e8uud'},
        {name: 'sadasda name', id: 'uuduu9s9dsa'},
        {name: 'this pppppp the name', id: 'uudf78uud'},
        {name: 'sadasda name', id: 'uududu99dsa'},
        {name: 'this pppppp the name', id: 'uud78fuud'},
        {name: 'sadasda name', id: 'uuduuc99dsa'},
      ],
      removed: [
        {name: 'this dsadas name', id: 'uu765duud'},
        {name: 'thisciao name', id: 'uuduud7676sa'},
      ],
    },
    mentalPattern: {
      toAcquire: [
        {name: 'mentalPattern is the name', id: 'uudu2ud'},
        {name: 'thieeeeeeame', id: 'uuduu3dsa'},
      ],
      toKeep: [
        {name: 'thissssse name', id: 'uud6uud'},
        {name: 'thiccccaae', id: 'uuduud4sa'},
      ],
      toReduce: [
        {name: 'thissssse name', id: 'uud6uuaaaaaaad'},
        {name: 'thiccccaae', id: 'uuduud4sassssssssss'},
      ],
      toRemove: [
        {name: 'this is the name', id: 'uu6duud'},
        {name: 'ciaoneeeme', id: 'uuduu77dsa'},
      ],
      reduced: [
        {name: 'this pppppp the name', id: 'uud7e8uud'},
        {name: 'sadasda name', id: 'uuduu9s9dsa'},
        {name: 'this pppppp the name', id: 'uudf78uud'},
        {name: 'sadasda name', id: 'uududu99dsa'},
        {name: 'this pppppp the name', id: 'uud78fuud'},
        {name: 'sadasda name', id: 'uuduuc99dsa'},
      ],
      removed: [
        {name: 'this dsadas name', id: 'uu765duud'},
        {name: 'thisciao name', id: 'uuduud7676sa'},
      ],
    },
    skill: {
      toAcquire: [
        {name: 'skill is the name', id: 'uudu2ud'},
        {name: 'thieeeeeeame', id: 'uuduu3dsa'},
      ],
      toKeep: [
        {name: 'thissssse name', id: 'uud6uud'},
        {name: 'thiccccaae', id: 'uuduud4sa'},
      ],
      toReduce: [
        {name: 'thissssse name', id: 'uud6uuaaaaaaad'},
        {name: 'thiccccaae', id: 'uuduud4sassssssssss'},
      ],
      toRemove: [
        {name: 'this is the name', id: 'uu6duud'},
        {name: 'ciaoneeeme', id: 'uuduu77dsa'},
      ],
      reduced: [
        {name: 'this pppppp the name', id: 'uud7e8uud'},
        {name: 'sadasda name', id: 'uuduu9s9dsa'},
        {name: 'this pppppp the name', id: 'uudf78uud'},
        {name: 'sadasda name', id: 'uududu99dsa'},
        {name: 'this pppppp the name', id: 'uud78fuud'},
        {name: 'sadasda name', id: 'uuduuc99dsa'},
      ],
      removed: [
        {name: 'this dsadas name', id: 'uu765duud'},
        {name: 'thisciao name', id: 'uuduud7676sa'},
      ],
    },
    identity: {
      toAcquire: [
        {name: 'identity is the name', id: 'uudu2ud'},
        {name: 'thieeeeeeame', id: 'uuduu3dsa'},
      ],
      toKeep: [
        {name: 'thissssse name', id: 'uud6uud'},
        {name: 'thiccccaae', id: 'uuduud4sa'},
      ],
      toReduce: [
        {name: 'thissssse name', id: 'uud6uuaaaaaaad'},
        {name: 'thiccccaae', id: 'uuduud4sassssssssss'},
      ],
      toRemove: [
        {name: 'this is the name', id: 'uu6duud'},
        {name: 'ciaoneeeme', id: 'uuduu77dsa'},
      ],
      reduced: [
        {name: 'this pppppp the name', id: 'uud7e8uud'},
        {name: 'sadasda name', id: 'uuduu9s9dsa'},
        {name: 'this pppppp the name', id: 'uudf78uud'},
        {name: 'sadasda name', id: 'uududu99dsa'},
        {name: 'this pppppp the name', id: 'uud78fuud'},
        {name: 'sadasda name', id: 'uuduuc99dsa'},
      ],
      removed: [
        {name: 'this dsadas name', id: 'uu765duud'},
        {name: 'thisciao name', id: 'uuduud7676sa'},
      ],
    },
    environment: {
      toAcquire: [
        {name: 'environment is the name', id: 'uudu2ud'},
        {name: 'thieeeeeeame', id: 'uuduu3dsa'},
      ],
      toKeep: [
        {name: 'thissssse name', id: 'uud6uud'},
        {name: 'thiccccaae', id: 'uuduud4sa'},
      ],
      toReduce: [
        {name: 'thissssse name', id: 'uud6uuaaaaaaad'},
        {name: 'thiccccaae', id: 'uuduud4sassssssssss'},
      ],
      toRemove: [
        {name: 'this is the name', id: 'uu6duud'},
        {name: 'ciaoneeeme', id: 'uuduu77dsa'},
      ],
      reduced: [
        {name: 'this pppppp the name', id: 'uud7e8uud'},
        {name: 'sadasda name', id: 'uuduu9s9dsa'},
        {name: 'this pppppp the name', id: 'uudf78uud'},
        {name: 'sadasda name', id: 'uududu99dsa'},
        {name: 'this pppppp the name', id: 'uud78fuud'},
        {name: 'sadasda name', id: 'uuduuc99dsa'},
      ],
      removed: [
        {name: 'this dsadas name', id: 'uu765duud'},
        {name: 'thisciao name', id: 'uuduud7676sa'},
      ],
    },
  }
  behaviorExpanded = null;
  results = [
    {
      name: 'risultato1',
      behaviors: [
        {
          name: 'ho fatto qualcosa di sad',
          macroType: 'hindering', // done || hindering || toImprove
          EI: false,
          mode: 'toRemove',
          id: 'dashiudasjn',
          variables: [
            {
              name: 'schema mentale buono',
              id: 'dsa78das7',
              type: 'mentalPattern',
              mode: 'toAcquire',
            },
            {
              name: 'schema mentale mediocre',
              type: 'mentalPattern',
              mode: 'toKeep',
            },
            {
              name: 'abitudine del cazzo',
              id: 'dsa78das3',
              type: 'habit',
              mode: 'toRemove',
            }
          ]
        },
        {
          name: 'ho fatto qualcosa di buono',
          macroType: 'done', // done || hindering || toImprove
          mode: 'toKeep',
          id: 'sssaaaaaaaaaaaaaa',
          variables: [
            {
              name: 'good habit',
              id: 'saaaaaaaaaaw',
              type: 'habit',
              mode: 'toAcquire',
            },
            {name: 'thieeeeeeame', id: 'uuduu3dsa',type: 'habit',mode: 'toAcquire'},
            {
              name: 'sedia in camera',
              type: 'environment',
              mode: 'toKeep',
            },
            {
              name: 'abitudine del cazzo',
              id: 'dsa78das3',
              type: 'habit',
              mode: 'toRemove',
            }
          ]
        },
        {
          name: 'potrei fare cose belle',
          macroType: 'toImprove', // done || hindering || toImprove
          mode: 'toAquire',
          id: 'sssaaaa34343aaaaaaaaaa',
          variables: [
          ]
        }
      ]
    }
  ];


  /* task & needs */
  data: TaskStats[] = []; // it contains not processed for table
  $obs: Observable<any>; // obs to retrieve information about user's stats
  constructor(private DB: DBManagerService, private authSer: AuthenticationService) { }
  /*
    return obsevable to show the data asked according to (date/mode)
  */
  fetchStatsMonitor(obj: any, collection: string): Observable<any> {
    /*
        return the obs used to:
        - retrieve the data of the this.date selected
        - retieve the num of document(s) based on the mode

      The data are stored: every months 1 document, so => to show 3 months I have to prepare an Observable with 3 docs and for 6 months...
    */
    if (obj.mode === 'week' || obj.mode === 'month') { // single document retrieving
      this.$obs = this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(obj.date, obj.mode)}`);
      return this.$obs;
    } else { // multiple document retrieving, n doc for n months
      const clone = obj.date.clone();
      if (obj.mode === '3months') {
        this.$obs = forkJoin([
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone, '3months')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '3months')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '3months')}`).pipe(take(1))
        ]);
      } else if (obj.mode === '6months') {
        this.$obs = forkJoin([
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone, '6months')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '6months')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '6months')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '6months')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '6months')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '6months')}`).pipe(take(1))
        ]);
      } else if (obj.mode === '1year') {
        this.$obs = forkJoin([
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone, '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1)),
          this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/stats/${collection}/${this.getDocPath(clone.add(1, 'month'), '1year')}`).pipe(take(1))
        ]);
      }
      return this.$obs;
    }
  }

  /*
    gets the date as a moment() and return the correct path based on the mode and date. 
      example: "week/year2019_week34
  */
  private  getDocPath(date: Moment, mode: string) {
    let path = '';
    if (mode === 'week') {
      path = 'week/';
      path += 'year' + date.utcOffset(2).year() + '_week' + date.utcOffset(2).weeks();
    } else {
      path = 'month/';
      path += 'year' + date.utcOffset(2).year() + '_month' + date.utcOffset(2).month();
    }
    return path;
  }
}
