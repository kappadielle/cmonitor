import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskMonitorComponent } from './task-monitor.component';
import { of } from 'rxjs';
import { MaterialModule } from 'src/app/material/material.module';
import { DBManagerService } from 'src/app/services/dbmanager.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

describe('TaskMonitorComponent', () => {
  let component: TaskMonitorComponent;
  let fixture: ComponentFixture<TaskMonitorComponent>;
  let stubDBService = {
    getDoc$: jasmine.createSpy('getDoc$').and.returnValue(of([]))
  }
  const stubAuthenticationService = {
    userAuth: {
      uid: '123ABC'
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule],
      providers: [
        {provide: DBManagerService, useValue: stubDBService},
        {provide: AuthenticationService, useValue: stubAuthenticationService}
      ],
      declarations: [ TaskMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
