import { Component, OnInit, ViewChild, Input, OnChanges } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TaskStats } from '../task-stats (interface)';


@Component({
  selector: 'app-task-monitor',
  templateUrl: './task-monitor.component.html',
  styleUrls: ['./task-monitor.component.scss']
})
export class TaskMonitorComponent implements OnInit, OnChanges {
  /*
    this class has the purpouse to show the stats stored by Google Cloud Functions
    ** Listen to @Input changes ** 
  */

  displayedColumns: string[] = ['name', 'val', 'slash', 'max', 'percentage']; // list of rows in the table
  taskDataSource: MatTableDataSource<TaskStats>; // data for table
  // tslint:disable-next-line:no-input-rename
  @Input('taskDataSource') InputSource: TaskStats[];
  @ViewChild(MatSort, {static: false}) sort: MatSort; // used to make table sorting works; {static: false} cause the data changes
  constructor() { }

  ngOnInit() {
    if (this.InputSource) {
      this.taskDataSource = new MatTableDataSource(this.InputSource);
      this.taskDataSource.sort = this.sort;
    }
  }

  ngOnChanges() {
    if (this.InputSource) {
      this.taskDataSource = new MatTableDataSource(this.InputSource);
      this.taskDataSource.sort = this.sort;
    }
  }
}
