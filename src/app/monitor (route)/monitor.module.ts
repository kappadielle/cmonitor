import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OibaMonitorComponent } from './oiba-monitor/oiba-monitor.component';
import { MonitorSetupComponent } from './monitor-setup/monitor-setup.component';
import { TaskMonitorComponent } from './task-monitor/task-monitor.component';
import { MonitorService } from './monitor.service';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { ByHandMonitorComponent } from './by-hand-monitor/by-hand-monitor.component';
import { BehaviorComponent } from './by-hand-monitor/behavior/behavior.component';
import { VariablePickerComponent } from './by-hand-monitor/behavior/variable-picker/variable-picker.component';

@NgModule({
  declarations: [
    OibaMonitorComponent,
    TaskMonitorComponent,
    MonitorSetupComponent,
    ByHandMonitorComponent,
    BehaviorComponent,
    VariablePickerComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    ChartsModule
  ],
  providers: [MonitorService]
})
export class MonitorModule { }
