import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { MonitorService } from '../monitor.service';
import { MatTableDataSource } from '@angular/material/table';
import { TaskStats } from '../task-stats (interface)';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-monitor-setup',
  templateUrl: './monitor-setup.component.html',
  styleUrls: ['./monitor-setup.component.scss']
})
export class MonitorSetupComponent implements OnInit {
  /*
    this component manage the date and the mode (week|month|3months...) of the monitoring => range // { date: moment, mode: string}
    manage components passing via @Input:
      - Task Monitor => taskDataSource: MatTableDataSource<TaskStats>;
      - Oiba Monitor => oibaDataSource: MatTableDataSource<any>
  */

  subject: string;
  needDataSource: any;
  taskDataSource: TaskStats[]; // data for task
  mode = 'week'; // used to display different ranges of stats -> value is setted from this.ranges
  date: moment.Moment; // date considered when showing the datas
  dateToDisplay: string; // date as string formatted
  monthsTranslator = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre']; // used to show the translation of the months
  ranges: any[] = [ // list ranges that could be select via <mat-select>
    {value: 'week', viewValue: 'Settimanale'},
    {value: 'month', viewValue: 'Mensile'},
    {value: '3months', viewValue: 'Trimestrale'},
    {value: '6months', viewValue: 'Semestrale'},
    {value: '1year', viewValue: 'Annuale'},
  ];

  constructor(private monitorSer: MonitorService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.subject = this.route.snapshot.paramMap.get('subject');
    this.date = moment().day(0); // set to sunday cause it's ought to display the weeks stats
    this.back(); // prepare data to show the last thing filled
    this.fetchSubject();
  }

  byHandEditing($event) {
    // used get from the byhand.component wheter use the edit mode or the fotografia mode
    console.log($event);
  }
  switchSubject(subject: 'tasks' | 'needs') {
    if (subject === 'tasks') {
      this.subject = 'tasks';
      this.router.navigate(['/monitor/tasks']);
    } else if (subject === 'needs') {
      this.subject = 'needs';
      this.router.navigate(['/monitor/needs']);
    } else if (subject === 'by-hand') {
      this.subject = 'by-hand';
      this.router.navigate(['/monitor/by-hand']);
    }
    this.fetchSubject();
  }
  fetchSubject() {
    if (this.subject === 'tasks') {
      this.fetchTaskMonitor();
    } else if (this.subject === 'needs') {
      this.fetchNeedMonitor();
    } else if (this.subject === 'by-hand') {
      // nothing at the moment
    }
  }

  /*
    get and subscribe to the observable given by the service.
    it arrange the data to be displayed and passed via @Input to task-monitor.component
  */
  fetchTaskMonitor() {
    const $obs = this.monitorSer.fetchStatsMonitor({date: this.date, mode: this.mode}, 'tasks');
    if (this.mode === 'week' || this.mode === 'month') {
      $obs.subscribe(stats => {
        const data = [];
        for (const key in stats) { // looping an object
          if (stats.hasOwnProperty(key)) {
              stats[key].forEach(day => { // looping every array in the object
                const index = data.findIndex(el => el.name === day.name);
                if (index >= 0) {
                  data[index].val += day.completed ? 1 : 0;
                  data[index].max += 1;
                  data[index].percentage += day.percentage;
                } else {
                  data.push({name: day.name, val: day.completed ? 1 : 0, max: 1, percentage: day && day.percentage ? day.percentage : 0});
                }
              });
          }
        }
        this.taskDataSource = data;
      });
    } else {
      $obs.subscribe(docs => { // receive an array of n elements according to the mode: 3months => arr.lenth == 3
        const data = [];
        docs.forEach(doc => { // looping the array of docs
          if (doc) { // probably undefined cause the user will check the months not monitored
            for (const key in doc) { // looping the object in the doc
              if (doc.hasOwnProperty(key)) {
                doc[key].forEach(day => { // looping every array in the doc
                    const index = data.findIndex(el => el.name === day.name);
                    if (index >= 0) {
                      data[index].val += day.completed ? 1 : 0;
                      data[index].max += 1;
                      data[index].percentage += day.percentage;
                    } else {
                      data.push({name: day.name, val: day.completed ? 1 : 0, max: 1, percentage: day && day.percentage ? day.percentage : 0});
                    }
                });
              }
            }
          }
        });
        this.taskDataSource = data;
      });
    }
  }

  /*
    get and subscribe to the observable given by the service.
    it arrange the data to be displayed and passed via @Input to oiba-monitor.component
  */
  fetchNeedMonitor() {
    const $obs = this.monitorSer.fetchStatsMonitor({date: this.date, mode: this.mode}, 'needs');
    const needsInfo = {};
    if (this.mode === 'week' || this.mode === 'month') {
      $obs.subscribe(stats => {
        const data = [];
        for (const needId in stats) { // looping an object
          if (stats.hasOwnProperty(needId)) {
            const index = data.findIndex(el => el.id === needId); // check if it is already in the table
            if (index >= 0) {
              data[index].satisfied += stats[needId].satisfied ? stats[needId].satisfied : 0;
              data[index].unsatisfied += stats[needId].unsatisfied ? stats[needId].unsatisfied : 0;
              data[index].lastDaySatisfied = stats[needId].lastDateSatisfied.seconds > data[index].lastDaySatisfied.valueOf() / 1000 ? new Date(stats[needId].lastDateSatisfied.seconds) : data[index].lastDaySatisfied;
            } else {
              data.push({
                id: needId,
                satisfied: stats[needId].satisfied ? stats[needId].satisfied : 0,
                unsatisfied: stats[needId].unsatisfied ? stats[needId].unsatisfied : 0,
                lastDaySatisfied: stats[needId].lastDateSatisfied ? new Date(stats[needId].lastDateSatisfied.seconds * 1000) : null});
            }
            const moodsNeg = {names: [], values: []};
            const moodsPos = {names: [], values: []};
            const relationsNeg = {names: [], values: []};
            const relationsPos = {names: [], values: []};
            for (const mood in stats[needId].moodsPos) {
              if (stats[needId].moodsPos.hasOwnProperty(mood)) {
                moodsPos.names.push(mood);
                moodsPos.values.push(stats[needId].moodsPos[mood]);
              }
            }
            for (const mood in stats[needId].moodsNeg) {
              if (stats[needId].moodsNeg.hasOwnProperty(mood)) {
                moodsNeg.names.push(mood);
                moodsNeg.values.push(stats[needId].moodsNeg[mood]);
              }
            }
            for (const relation in stats[needId].relationsPos) {
              if (stats[needId].relationsPos.hasOwnProperty(relation)) {
                relationsPos.names.push(relation);
                relationsPos.values.push(stats[needId].relationsPos[relation]);
              }
            }
            for (const relation in stats[needId].relationsNeg) {
              if (stats[needId].relationsNeg.hasOwnProperty(relation)) {
                relationsNeg.names.push(relation);
                relationsNeg.values.push(stats[needId].relationsNeg[relation]);
              }
            }
            needsInfo[needId] = {
              moodsPos,
              moodsNeg,
              relationsPos,
              relationsNeg
            };
          }
        }
        this.needDataSource = {table: data, needsInfo};
      });
    } else {
      $obs.subscribe(docs => { // receive an array of n elements according to the mode: 3months => arr.lenth == 3
        const data = [];
        docs.forEach(stats => { // looping the array of docs
          if (stats) { // probably undefined cause the user will check the months not monitored
            for (const needId in stats) { // looping an object
              if (stats.hasOwnProperty(needId)) {
                const index = data.findIndex(el => el.id === needId); // check if it is already in the table
                if (index >= 0) {
                  data[index].satisfied += stats[needId].satisfied ? stats[needId].satisfied : 0;
                  data[index].unsatisfied += stats[needId].unsatisfied ? stats[needId].unsatisfied : 0;
                  data[index].lastDaySatisfied = stats[needId].lastDateSatisfied.seconds > data[index].lastDaySatisfied.valueOf() / 1000 ? new Date(stats[needId].lastDateSatisfied.seconds) : data[index].lastDaySatisfied;
                } else {
                  data.push({
                    id: needId,
                    satisfied: stats[needId].satisfied ? stats[needId].satisfied : 0,
                    unsatisfied: stats[needId].unsatisfied ? stats[needId].unsatisfied : 0,
                    lastDaySatisfied: stats[needId].lastDateSatisfied ? new Date(stats[needId].lastDateSatisfied.seconds * 1000) : null});
                }
                const moodsNeg = {names: [], values: []};
                const moodsPos = {names: [], values: []};
                const relationsNeg = {names: [], values: []};
                const relationsPos = {names: [], values: []};
                for (const mood in stats[needId].moodsPos) {
                  if (stats[needId].moodsPos.hasOwnProperty(mood)) {
                    moodsPos.names.push(mood);
                    moodsPos.values.push(stats[needId].moodsPos[mood]);
                  }
                }
                for (const mood in stats[needId].moodsNeg) {
                  if (stats[needId].moodsNeg.hasOwnProperty(mood)) {
                    moodsNeg.names.push(mood);
                    moodsNeg.values.push(stats[needId].moodsNeg[mood]);
                  }
                }
                for (const relation in stats[needId].relationsPos) {
                  if (stats[needId].relationsPos.hasOwnProperty(relation)) {
                    relationsPos.names.push(relation);
                    relationsPos.values.push(stats[needId].relationsPos[relation]);
                  }
                }
                for (const relation in stats[needId].relationsNeg) {
                  if (stats[needId].relationsNeg.hasOwnProperty(relation)) {
                    relationsNeg.names.push(relation);
                    relationsNeg.values.push(stats[needId].relationsNeg[relation]);
                  }
                }
                needsInfo[needId] = {
                  moodsPos,
                  moodsNeg,
                  relationsPos,
                  relationsNeg
                };
              }
            }
          }
        });
        this.needDataSource = {table: data, needsInfo};
      })
    }
  }

  /*
    used to move back of 1 step (depending on the mode):
      week: 1 week, month: 1 month, others: 1 month

      1) it updates this.date and this.dateToDisplay
      2) call prepareData()
  */
  back() {
    let clone; // used to display the date range without keep the reference => restore date not necessary

    // used to get and set the date and dateToDisplay(clone)
    let month;
    let week;
    let year;

    switch (this.mode) {
      case 'week':
        week = this.date.week(); // get
        this.date = this.date.week(week - 1); // set
        clone = this.date.clone(); // get the clone
        this.dateToDisplay = `Dal ${clone.format('DD/MM')} al ${clone.week(week).format('DD/MM - YYYY')}`; // set the ranges
        break;
      case 'month':
        month = this.date.month();
        this.date = this.date.month(month - 1);
        this.dateToDisplay = `${this.monthsTranslator[Math.abs(month - 1) % 12]} ${this.date.format('YYYY')}`;
        break;
      case '3months':
        year = this.date.year();
        month = this.date.month();
        this.date = this.date.month(month - 1);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[Math.abs(month - 1) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 2) % 12]} ${clone.year(year).month(month + 2).format('YYYY')}`;
        break;
      case '6months':
          year = this.date.year();
          month = this.date.month();
          this.date = this.date.month(month - 1);
          clone = this.date.clone();
          this.dateToDisplay = `${this.monthsTranslator[Math.abs(month - 1) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 5) % 12]} ${clone.year(year).month(month + 5).format('YYYY')}`;
          break;
      case '1year':
          year = this.date.year();
          month = this.date.month();
          this.date = this.date.month(month - 1);
          clone = this.date.clone();
          this.dateToDisplay = `${this.monthsTranslator[Math.abs(month - 1) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 11) % 12]} ${clone.year(year + 1).format('YYYY')}`;
          break;
      default:
        break;
    }
    this.fetchSubject();
  }

  /*
    used to move back of n step (depending on the mode):
      week: disabled, month: disabled, 3 months: 3 months, 6 months: 6 months, 1 year: 1 year

      1) it updates this.date and this.dateToDisplay
      2) call prepareData()
  */
  backNMonths() {
    let clone; // used to display the date range without keep the reference => restore date not necessary

    // used to get and set the date and dateToDisplay(clone)
    let month;
    let year;
    switch (this.mode) {
      case '3months':
        year = this.date.year();
        month = this.date.month();
        this.date.month(month - 3);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[Math.abs(month - 3) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[month]} ${clone.year(year).month(month).format('YYYY')}`;
        break;
      case '6months':
        year = this.date.year();
        month = this.date.month();
        this.date.month(month - 6);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[Math.abs(month - 6) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[month]} ${clone.year(year).month(month).format('YYYY')}`;
        break;
      case '1year':
        year = this.date.year();
        month = this.date.month();
        this.date.year(year - 1);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[month]} ${clone.format('YYYY')} - ${this.monthsTranslator[month]} ${clone.year(year).format('YYYY')}`;
        break;
      default:
        break;
    }
    this.fetchSubject();
  }

  /*
    used to move forward of n step (depending on the mode):
      week: disabled, month: disabled, 3 months: 3 months, 6 months: 6 months, 1 year: 1 year

      1) it updates this.date and this.dateToDisplay
      2) call prepareData()
  */
  forwardNMonths() {
    let clone; // used to display the date range without keep the reference => restore date not necessary

    // used to get and set the date and dateToDisplay(clone)
    let month;
    let year;
    switch (this.mode) {
      case '3months':
        year = this.date.year();
        month = this.date.month();
        this.date.month(month + 3);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[(month + 3) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 6) % 12]} ${clone.year(year).month(month + 6).format('YYYY')}`;
        break;
      case '6months':
        year = this.date.year();
        month = this.date.month();
        this.date.month(month + 6);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[(month + 6) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 12) % 12]} ${clone.year(year).month(month + 12).format('YYYY')}`;
        break;
      case '1year':
        year = this.date.year();
        month = this.date.month();
        this.date.year(year + 1);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[month]} ${clone.format('YYYY')} - ${this.monthsTranslator[month]} ${clone.year(year + 2).format('YYYY')}`;
        break;
      default:
      break;
    }
    this.fetchSubject();
  }

  /*
    used to move forward of 1 step (depending on the mode):
      week: 1 week, month: 1 month, others: 1 month

      1) it updates this.date and this.dateToDisplay
      2) call prepareData()
  */
  forward() {
    let clone; // used to display the date range without keep the reference => restore date not necessary

    // used to get and set the date and dateToDisplay(clone)
    let month;
    let week;
    let year;

    switch (this.mode) {
      case 'week':
        week = this.date.week();
        this.date.week(week + 1)
        clone = this.date.clone();
        this.dateToDisplay = `Dal ${clone.format('DD/MM')} al ${clone.week(week + 2).format('DD/MM - YYYY')}`;
        break;
      case 'month':
        month = this.date.month();
        this.date = this.date.month(month + 1);
        this.dateToDisplay = `${this.monthsTranslator[(month + 1) % 12]} ${this.date.format('YYYY')}`;
        break;
      case '3months':
        year = this.date.year();
        month = this.date.month();
        this.date.month(month + 1);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[(month + 1) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 4) % 12]} ${clone.year(year).month(month + 4).format('YYYY')}`;
        break;
      case '6months':
        year = this.date.year();
        month = this.date.month();
        this.date.month(month + 1);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[(month + 1) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 7) % 12]} ${clone.year(year).month(month + 7).format('YYYY')}`;
        break;
      case '1year':
        year = this.date.year();
        month = this.date.month();
        this.date.month(month + 1);
        clone = this.date.clone();
        this.dateToDisplay = `${this.monthsTranslator[(month + 1) % 12]} ${clone.format('YYYY')} - ${this.monthsTranslator[(month + 13) % 12]} ${clone.year(year + 1).format('YYYY')}`;
        break;
      default:
        break;
    }
    this.fetchSubject();
  }
  /*
      used when selected something from select:
        1) sets the value of the mode
        2) reset data to be displayed
    */
  switchMode(mode: any) {
    this.mode = mode.value;
    this.date = moment().day(0);
    this.back();
    this.fetchSubject();
  }
}
