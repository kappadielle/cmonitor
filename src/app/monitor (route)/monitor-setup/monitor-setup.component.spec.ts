import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorSetupComponent } from './monitor-setup.component';

describe('MonitorSetupComponent', () => {
  let component: MonitorSetupComponent;
  let fixture: ComponentFixture<MonitorSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
