import { Component, OnInit } from '@angular/core';
import { StrategyService } from '../strategy.service';

@Component({
  selector: 'app-strategy',
  templateUrl: './strategy.component.html',
  styleUrls: ['./strategy.component.scss']
})
export class StrategyComponent implements OnInit {
  relation: string;
  constructor(private strategySer: StrategyService) { }

  ngOnInit() {
  }
  addNewRelation() {
    this.strategySer.addNewRelation(this.relation);
    this.relation = '';
  }
}
