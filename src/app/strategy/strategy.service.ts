import { DBManagerService } from '../services/dbmanager.service';
import { AuthenticationService } from '../services/authentication.service';
import { firestore } from 'firebase/app';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()

export class StrategyService {

  constructor(private DB: DBManagerService, private authSer: AuthenticationService) { }

  addNewRelation(relation: string): void {
    this.DB.updateDoc(`users/${this.authSer.userAuth.uid}/strategy/relations`, {list: firestore.FieldValue.arrayUnion(relation)});
  }

  getRelations(): Observable<any> {
    return this.DB.getDoc$(`users/${this.authSer.userAuth.uid}/strategy/relations`);
  }
}
