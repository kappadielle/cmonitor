import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StrategyComponent } from './strategy/strategy.component';
import { StrategyService } from './strategy.service';
import { MaterialModule } from '../material/material.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [StrategyComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  providers: [StrategyService]
})
export class StrategyModule { }
