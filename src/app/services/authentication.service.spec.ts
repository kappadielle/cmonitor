import { TestBed, fakeAsync, tick } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { TestScheduler } from 'rxjs/testing';

describe('AuthenticationService', () => {
  let authSer;
  let router;
  let authAf;

  const credentialsMock = {
    email: 'abc@123.com',
    password: 'password'
  };
  const userMock = {
    uid: 'ABC123',
    email: credentialsMock.email,
  };
  const fakeAuthState = new BehaviorSubject(null); // <= Pay attention to this guy
  const fakeSignInHandler = (email, password): Promise<any> => {
    fakeAuthState.next(userMock);
    return Promise.resolve(userMock);
  };
  const fakeSignOutHandler = (): Promise<any> => {
    fakeAuthState.next(null);
    return Promise.resolve(null);
  };
  const fakefetchSignInMethodsForEmailHandler = (email): Promise<any> => {
    return Promise.resolve(email ? ['methodToSignIn'] : []);
  };
  const angularFireAuthStub = {
    authState: fakeAuthState,
    auth: {
      createUserWithEmailAndPassword: jasmine
        .createSpy('createUserWithEmailAndPassword')
        .and
        .callFake(fakeSignInHandler),
      signInWithEmailAndPassword: jasmine
        .createSpy('signInWithEmailAndPassword')
        .and
        .callFake(fakeSignInHandler),
      fetchSignInMethodsForEmail: jasmine
        .createSpy('fetchSignInMethodsForEmail')
        .and
        .callFake(fakefetchSignInMethodsForEmailHandler),
      signOut: jasmine
        .createSpy('signOut')
        .and
        .callFake(fakeSignOutHandler),
    },
  };

  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthenticationService,
        { provide: AngularFireAuth, useValue: angularFireAuthStub },
        { provide: Router, useValue: routerSpy }
      ]
    });
    authSer = TestBed.get(AuthenticationService);
    authAf = TestBed.get(AngularFireAuth);
    router = TestBed.get(Router);
  });

  it('should be created', () => {
    expect(authSer).toBeTruthy();
  });

  describe('dispatch user with email', () => {
    it('redirect anyway to /profile', fakeAsync(() => {
      authSer.verifyIfMailAlreadyExists(null);
      tick();
      expect(router.navigate).toHaveBeenCalledWith(['/profile']);
    }));
    it('existing user -> sharedEmail', fakeAsync(() => {
      authSer.verifyIfMailAlreadyExists(credentialsMock.email);
      tick();
      expect(authSer.sharedEmail).toEqual({email: credentialsMock.email, userExists: true});
    }));
    it('NOT existing user -> sharedEmail', fakeAsync(() => {
      authSer.verifyIfMailAlreadyExists(null);
      tick();
      expect(authSer.sharedEmail).toEqual({email: null, userExists: false});
    }));
  });

  describe('Shallow AngularFireAuth functions', () => {
    it('LogOut Works', () => {
      authSer.logout();
      expect(authAf.auth.signOut).toHaveBeenCalled();
    });
    it('LogIn Works', () => {
      authSer.emailLogin(credentialsMock.email, 'password');
      expect(authAf.auth.signInWithEmailAndPassword).toHaveBeenCalledWith(credentialsMock.email, 'password');
    });
    it('SignIn Works', () => {
      authSer.emailSignUp(credentialsMock.email, 'password');
      expect(authAf.auth.createUserWithEmailAndPassword).toHaveBeenCalledWith(credentialsMock.email, 'password');
    });
  });
  xit('MARBLE TEST NON FATTO', () => {});

  describe('User Observables stuff', () => {

    const testScheduler = new TestScheduler((actual, expected) => {
      // asserting the two objects are equal
      expect(actual).toEqual(expected);
    });

    it('should prepare global Observable at first', () => {
      expect(authSer.$userAuth).toBeTruthy();
    });

    it('$userAuth asyncPipe assign value to userAuth', () => {
      testScheduler.run(({cold, expectObservable, flush}) => {
        const obs1 = cold('-a', { a: userMock});
        const result = obs1.pipe(tap((val) => { authSer.userAuth = val; }));
        expectObservable(result).toBe('-a', { a: userMock});
        flush();
        expect(authSer.userAuth).toBe(userMock);
      });
    });
  });
});
