import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { firestore } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class DBManagerService {

  /*
    Class used to interface with the DB and to manage errors that comes out from https
    THE CLASS WILL CHECKS FOR ERRORS AND DISPLAY IT TO USER/ MANAGE IT
  */

  constructor(private db: AngularFirestore) {
  }
  getDoc(path: string): AngularFirestoreDocument<any> {
    return this.db.doc(path);
  }
  getDoc$(path: string): Observable<any> {
    return this.getDoc(path).valueChanges();
  }
  getCol(path: string): AngularFirestoreCollection<any> {
    return this.db.collection(path);
  }
  getCol$(path: string | AngularFirestoreCollection<any>): Observable<any> {
    if (typeof(path) == 'string') {
      return this.getCol(path).valueChanges({ idField: 'id'});
    } else {
      return path.valueChanges({ idField: 'id'});
    }
  }
  getCollectionAndQuery(path: string, method: string, op: any, value: any): AngularFirestoreCollection<any> {
    return this.db.collection(path, ref => ref.where(method, op , value));
  }
  getCollectionOrderAndLimit(path: string, orderBy: string, order: any, limit: number): AngularFirestoreCollection<any> {
    return this.db.collection(path, ref => ref.orderBy(orderBy, order).limit(limit));
  }
  getCollectionQueriesAndOrder(path: string, orderBy: string, order: any, method: string, op: any, value: any, method2: string, op2: any, value2: any): AngularFirestoreCollection<any> {
    return this.db.collection(path, ref => ref.where(method, op , value).where(method2, op2 , value2).orderBy(orderBy, order));
  }
  obsOfDoc(doc: AngularFirestoreDocument): Observable<any> {
    return doc.valueChanges();
  }
  setDoc(doc: AngularFirestoreDocument | string, value: object): void {
    if (typeof(doc) == 'string') {
      this.db.doc(doc).set(value)
    } else {
      doc.set(value);
    }
  }
  updateDoc(doc: AngularFirestoreDocument | string, value: object): any {
    if (typeof(doc) == 'string') {
      return this.db.doc(doc).update(value);
    } else {
      return doc.update(value);
    }
  }
  obsOfCol(col: AngularFirestoreCollection): Observable<any> {
    return col.valueChanges({ idField: 'id'});
  }
  addToCol(col: AngularFirestoreCollection | string, value: object): any {
    if (typeof(col) == 'string') {
      return this.db.collection(col).add(value)
    } else {
      return col.add(value);
    }
  }
}
