import { TestBed } from '@angular/core/testing';

import { DBManagerService } from './dbmanager.service';
import { Observable, of } from 'rxjs';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';

describe('DBManagerService', () => {
  
  const fakeangularFirestore = {
    collection: jasmine
      .createSpy('collection')
      .and
      .returnValue([{num: 1}, {num: 2}, {num: 3}]),
    doc: jasmine
      .createSpy('doc')
      .and
      .returnValue({num: 1}),
  }

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: AngularFirestore, useValue: fakeangularFirestore },
    ]
  }));

  /*
    const FirestoreStub = {
    collection: (name: string) => ({
      doc: (_id: string) => ({
        valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
        set: (_d: any) => new Promise((resolve, _reject) => resolve()),
      }),
    }),
  };
  */
  it('should be created', () => {
    const service: DBManagerService = TestBed.get(DBManagerService);
    expect(service).toBeTruthy();
  });
});
