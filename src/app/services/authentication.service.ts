import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, of } from 'rxjs';
import { User } from 'firebase/app';
import { tap, share, first, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SharedEmail } from '../home (route)/shared-email (interface)';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  /*
    manage the firebase Authentication for user
  */
  sharedEmail: SharedEmail; // object shared between profile and home that contains: {email & !!userExist}. it comes from home emailfield
  userAuth: User; // needed at authGuard
  $userAuth: Observable<User>; // Obs which gives the state of auth (auto login)
  redirectUrl: string; // it store the previous route from [authGuard]
  constructor(private afAuth: AngularFireAuth, private route: Router) {
    this.createAndPipeGlobal$();
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  /*
    1) navigate to '/profile'
    2) create an object that contains: {email & !!userExist} to pass to profile -> login/signup
  */
  verifyIfMailAlreadyExists(email: string) {
    this.afAuth.auth.fetchSignInMethodsForEmail(email)
    .then(arr => {
      // arr = array that contains all the ways that user used to authenticate
      if (arr.length) {
        this.sharedEmail = {email, userExists: true};
      } else {
        this.sharedEmail = {email, userExists: false};
      }
      this.route.navigate(['/profile']);
    });
  }

  /*
    1) creates the observable used in the system
      / common use: $userAuth
    2) fill this.userAuth in the pipe()
  */
  createAndPipeGlobal$() {
    // remember that when you assign an observable to another, if you want the final Obs piped, you have to do the assignment and finally pipe the last one
    this.$userAuth = this.afAuth.authState;
    this.$userAuth = this.$userAuth.pipe(
      tap(val => {
        console.log(val);
        this.userAuth = val;
      }),
      shareReplay()
    );
  }

  emailSignUp(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  emailLogin(email: string, password: string) {
    const val = this.afAuth.auth.signInWithEmailAndPassword(email, password);
    val.then(_ => this.route.navigate(['/home']));
    return val;
  }
}
