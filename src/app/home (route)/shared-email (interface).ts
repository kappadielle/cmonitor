export interface SharedEmail {
    email: string;
    userExists: boolean;
}
