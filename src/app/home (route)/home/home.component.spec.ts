import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { AuthenticationService } from '../services/authentication.service';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { click } from '../testing/clickHelper';
import { DisplayCollectionIdsPipe } from '../pin-up/display-collection-ids.pipe';
import { ChipsInfo } from '../interfaces/chips.info';
import { Input, Component } from '@angular/core';
import { Router } from '@angular/router';
import { DBManagerService } from '../services/dbmanager.service';

@Component({selector: 'app-chip-list', template: ''})
class StubChipListComponent {
  @Input() selectedAndInserted: Subject<string>;
  @Input() chipsInfo: ChipsInfo;
}
@Component({selector: 'app-cybernetic-training', template: ''})
class StubCyberneticTrainingComponent {}

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  let authSer;

  let input;
  let button;

  let Mockuser = {
    uid: 'ABC123',
    email: 'ijnusadf@sad.dsa',
  }

  const RouterSpy = jasmine.createSpyObj('Router', ['navigate']);
  const DBServiceSpy = {
    getCol$: (path: string) => of([{id: 'I'}, {id: 'Am'}, {id: 'Lucia'}]),
  }

  beforeEach(async(() => {
    const stubAuthenticationService = {
      $userAuth: of(Mockuser),
      userAuth: {
        uid: 'ABC123',
        email: 'ijnusadf@sad.dsa',
      },
      redirectUrl: '/OIBA',
      verifyIfMailAlreadyExists: jasmine.createSpy()
    };
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, MaterialModule],
      declarations: [ HomeComponent, DisplayCollectionIdsPipe, StubChipListComponent, StubCyberneticTrainingComponent ],
      providers: [
        { provide: AuthenticationService, useValue: stubAuthenticationService },
        { provide: Router, useValue: RouterSpy },
        { provide: DBManagerService, useValue: DBServiceSpy }
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authSer = TestBed.get(AuthenticationService);

    input = fixture.debugElement.nativeElement.querySelector('input');
    button = fixture.debugElement.nativeElement.querySelector('button[type=\'submit\']');

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('FORM testing', () => {
    // NO USER NEEDED TO ACTIVATE THE FORM
    Mockuser = null;

    it('Button disabled with invalid/No Email', () => {
      input.value = 'abc23.it';
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
      input.value = '';
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
    });
    it('Button available with validate Email', () => {
      input.value = 'abc@123.it';
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(false);
    });
    it('Call the function after click, with the right args', () => {
      input.value = 'abc@123.it';
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      click(button);
      expect(authSer.verifyIfMailAlreadyExists).toHaveBeenCalledWith('abc@123.it');
    });
  });
});
