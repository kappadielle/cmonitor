import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';
import { ChipsInfo } from 'src/app/pin-up/chips.info (interface)';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DBManagerService } from 'src/app/services/dbmanager.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  /*
    entry page which can change based on user logged/notLogged
      -logged:
      -notLogged: shows a field for email that redirect in the right place
  */
  $userAuth: Observable<firebase.User>;
  $outline: Observable<ChipsInfo[]>; // Obs which gives the list of things from the state of auth (auto login)
  emailForm: any;
  userID: string;
  constructor(private route: Router, private authSer: AuthenticationService, private formBuilder: FormBuilder, private DB: DBManagerService) { }
  ngOnInit() {

    this.emailForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        // tslint:disable-next-line:max-line-length
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ]]
    }, {
      updateOn: 'change',
    });
    this.$userAuth = this.authSer.$userAuth.pipe(
      tap(val => {
        if (this.authSer.userAuth) {
          this.userID = this.authSer.userAuth.uid;
          this.$outline = this.DB.getCol$(`users/${this.authSer.userAuth.uid}/outlines`).pipe(
            take(1), // otherwise it will reload every Input to firebase noSql
          );
        }
        /*
          when a page with [authGuard] is loaded & the user has already logged in:
            it normally redirect to home cause $userAuth is slower than syncronous [authGuard];
            this function use the route saved by authGuard before redirecting to REredirect in the previous route!
            finally it set to NULL the route saved by authGuard
        */
        if (this.authSer.redirectUrl && val) { // if there's a previous route saved (f5) and there's a user, otherwise it will crash redirecting
          this.route.navigate([this.authSer.redirectUrl]); // 3) redirect autologged user to the correct page when 'f5' instead of returning to home 'cause [authGuard]
          this.authSer.redirectUrl = null; // clean the previous route
        }
      }),
    );

  }

  get email() { return this.emailForm.value.email; }

  verifyIfMailAlreadyExists() {
    this.authSer.verifyIfMailAlreadyExists(this.email);
  }
}
