import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app-component/app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Environments
import { environment } from 'src/environments/environment';

// AngularFire 2
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import 'hammerjs'; // for gesture support (e. g. slide, tooltip)
import { LoginComponent } from './profile (route)/login/login.component';
import { SignupComponent } from './profile (route)/signup/signup.component';
import { MaterialModule } from './material/material.module';
import { RouterLinkDirective } from './testing/router-link.directive';

import { AngularFirePerformanceModule } from '@angular/fire/performance';
import { TodolistModule } from './todolist (route)/todolist.module';
import { PinUpModule } from './pin-up/pin-up.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NeedsModule } from './needs (route)/needs.module';
import { AppRoutingModule } from './routing/app-routing.module';
import { HomeComponent } from './home (route)/home/home.component';
import { ProfileComponent } from './profile (route)/profile/profile.component';
import { MentalPatternModule } from './mental-pattern/mental-pattern.module';
import { StrategyModule } from './strategy/strategy.module';
import { MonitorModule } from './monitor (route)/monitor.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,
    SignupComponent,
    RouterLinkDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    TodolistModule,
    PinUpModule,
    NeedsModule,
    AngularFirePerformanceModule,
    MentalPatternModule,
    StrategyModule,
    MonitorModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
