import { Component} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, share, tap, shareReplay } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { User } from 'firebase';
import { OverlayContainer } from '@angular/cdk/overlay';
import { RouterOutlet } from '@angular/router';
import { animation, style, animate, trigger, transition, useAnimation, query, animateChild, group } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('routeAnimations', [
      transition('* <=> *', [
        // Set a default  style for enter and leave
        query(':enter, :leave', [
          style({
            position: 'absolute',
            width: 'calc(100% - 2em)', // used to prevent overflow-x
            opacity: 0,
          }),
        ], {optional: true}),
        // Animate the new page in
        query(':enter', [
          animate('600ms ease', style({ opacity: 1, })),
        ], {optional: true})
      ]),
    ])
  ]
})
export class AppComponent {
  title = 'CMonitor';
  theme = 'light'; // theme stored as string, not boolean 'cause it cause problem in local storage
  $userAuth: Observable<User>; // currentUser authenticated
  myStorage = window.localStorage;
  overlayContainerClasses: DOMTokenList; // contains all the classes in the cdk overlay

  constructor(private breakpointObserver: BreakpointObserver,
              private authSer: AuthenticationService,
              private overlayContainer: OverlayContainer) {
    this.$userAuth = this.authSer.$userAuth;
    this.setTheme();
  }

  /*
    observable that listen to width of display
      /return true if handset & false if desktop
  */
  isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 599px)')
  .pipe(
    tap(a => console.log(a)),
    map(result => result.matches),
    shareReplay(),
  );

  /*
    switch between light and dark theme saving them in cache.
    it's called by the view (click)="swapTheme()"
  */
  swapTheme() {
    if (this.theme === 'light') {
      this.theme = 'dark';
      this.overlayContainerClasses.remove('light');
      this.overlayContainerClasses.add('dark');
    } else if (this.theme === 'dark') {
      this.theme = 'light';
      this.overlayContainerClasses.remove('dark');
      this.overlayContainerClasses.add('light');
    }
    this.overlayContainerClasses.remove();
    this.overlayContainerClasses.add();
    this.myStorage.setItem('theme', this.theme);
  }

  /* get the theme saved in local (if there is one) and set it
      / in <app-root> with [ngclass]
      / in cdk overlay manually below
  */
  setTheme(): any {
    const themeCached = localStorage.getItem('theme');
    this.theme = themeCached ? themeCached : this.theme;

    // set the theme in the cdk overlay --> it's not a child in app-root --> then you have to add the theme manually
    this.overlayContainerClasses = this.overlayContainer.getContainerElement().classList; // get class list
    // tslint:disable-next-line:max-line-length
    const themeClassesToRemove = Array.from(this.overlayContainerClasses).filter((item: string) => item.includes('light') || item.includes('dark'));
    if (themeClassesToRemove.length) { // remove .dark & .light
      this.overlayContainerClasses.remove(...themeClassesToRemove);
    }
    this.overlayContainerClasses.add(this.theme); // add current theme
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['title'];
  }
}
