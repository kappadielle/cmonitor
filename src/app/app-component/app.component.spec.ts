import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { AuthenticationService } from '../services/authentication.service';
import { Observable, from } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { MaterialModule } from '../material/material.module';
import { RouterLinkDirective } from '../testing/router-link.directive';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { click } from '../testing/clickHelper';
import { RouterTestingModule } from '@angular/router/testing';

// tslint:disable-next-line:component-selector
@Component({selector: 'router-outlet', template: ''})
class RouterOutletStubComponent {
}

describe('AppComponent', () => {
  let fixture;
  let app;

  const stubAuthenticationService = {
    $userAuth: Observable,
  };
  const matchObj = [ // initially all are false
    { matchStr: '(max-width: 599.99px)', result: false },
    // { matchStr: '(max-width: 959.99px)', result: false }
  ];
  const fakeObserve = (s: string[]): Observable<BreakpointState> => from(matchObj).pipe(
      filter(match => match.matchStr === s[0]),
      map(match => <BreakpointState> { matches: match.result, breakpoints: {} })
  );
  const StubBreakpointObserver = jasmine.createSpyObj('BreakpointObserver', ['observe']);
  StubBreakpointObserver.observe.and.callFake(fakeObserve);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        RouterLinkDirective,
        RouterOutletStubComponent
      ],
      providers: [
        { provide: BreakpointObserver, useValue: StubBreakpointObserver },
        // { provide: OverlayContainer, useValue: stubOverlayContainer },
        { provide: OverlayContainer, useFactory: () => {
          const overlayContainerElement = document.createElement('div');
          return { getContainerElement: () => overlayContainerElement };
        }},
        { provide: AuthenticationService, useValue: stubAuthenticationService } ]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
  }));

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  describe('Theming', () => {
    it('set theme cached if any', () => {
      const themeCached = localStorage.getItem('theme') || app.theme;
      app.setTheme();
      expect(app.theme).toBe(themeCached);
      expect(app.overlayContainerClasses).toContain(themeCached);
    });
    it('switch theme', () => {
      app.theme = 'light';
      app.swapTheme();
      expect(app.theme).toBe('dark');
      expect(app.overlayContainerClasses).toContain('dark');
      expect(localStorage.getItem('theme')).toContain('dark');
      app.theme = 'dark';
      app.swapTheme();
      expect(app.theme).toBe('light');
      expect(app.overlayContainerClasses).toContain('light');
      expect(localStorage.getItem('theme')).toContain('light');
    });
  });
  describe('routerLink Tests', () => {
    let linkDes;
    let routerLinks;
    beforeEach(() => {
      fixture.detectChanges(); // trigger initial data binding

      // find DebugElements with an attached RouterLinkStubDirective
      linkDes = fixture.debugElement
        .queryAll(By.directive(RouterLinkDirective));

      // get attached link directive instances
      // using each DebugElement's injector
      routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirective));
    });
    it('all filled', () => {
      expect(routerLinks.length).toBe(5, 'should have 5 routerLinks');
      expect(routerLinks[0].linkParams[0]).toBe('/home');
      expect(routerLinks[1].linkParams[0]).toBe('/todoList');
      expect(routerLinks[2].linkParams[0]).toBe('/OIBA');
      expect(routerLinks[3].linkParams[0]).toBe('/monitor');
      expect(routerLinks[4].linkParams[0]).toBe('/profile');
    });
    it('click redirect good', () => {
      for (let i = 0; i < 4; i += 1) {
        const heroesLinkDe = linkDes[i];   // heroes link DebugElement
        const heroesLink = routerLinks[i]; // heroes link directive

        expect(heroesLink.navigatedTo).toBeNull('should not have navigated yet');

        click(heroesLinkDe);
        fixture.detectChanges();

        expect(heroesLink.navigatedTo).toBe(heroesLink.linkParams);
      }
    })
  });
});
