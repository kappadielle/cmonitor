import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {ENTER} from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { DBManagerService } from '../../services/dbmanager.service';
import { AuthenticationService } from '../../services/authentication.service';
import { ChipsInfo } from '../chips.info (interface)';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-chip-list',
  templateUrl: './chip-list.component.html',
  styleUrls: ['./chip-list.component.scss']
})
export class ChipListComponent implements OnInit {
  @Input() selectedAndInserted: Subject<boolean>;
  @Input() chipsInfo: ChipsInfo; // value passed from parent template that contains array of tasks and mode(static|dynamic)
  @Output() selected = new EventEmitter();                                              //  Interface ChipsInfo
  visible = true;                                                                       //   id: string;
  addOnBlur = true;                                                                     //   list: [string];
  removable = true;                                                                     //   static?: boolean;
  addable = true;
  multiple = true;
  indexSelected: number[] = []; // indexes of chipsInfo.list which are selected
  readonly separatorKeysCodes: number[] = [ENTER];
  constructor(private DB: DBManagerService, private authSer: AuthenticationService) { }
  /*
    displays a list of chips that could be static or dynamic:
      1) static = chip can be only selectable one at time
      2) dynamic = chip can be selected, created, removed

    requisites: ngif from the template where called so "ngOnInit()" can detect static chip list
  */
  ngOnInit() {
    if (this.chipsInfo.static) {
      this.removable = false;
      this.addable = false;
      this.multiple = false;
      this.selectedAndInserted.subscribe(val => { // val is always true
        if (this.indexSelected.length == 1) {
          this.remove(this.indexSelected[0]);
          this.indexSelected = [];
        }
      })
    }
    // every time a value is emitted, A task is inserted, so the chips that match with the task.name can be removed
  }
  
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our item
    if ((value || '').trim()) {
      this.chipsInfo.list.push(value.trim());
      /* I got authSer.userAuth cause the component initialize when:
        1) home component async obs in template emits -> tap(val => userAuth = val)
        2) the component who contain this is protected with guard for authUser :)
      */
      this.DB.updateDoc(this.DB.getDoc(`users/${this.authSer.userAuth.uid}/outlines/${this.chipsInfo.id}`), { list: this.chipsInfo.list });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  /*
      assign items to an array of selected items
  */
  selectMe(item: string) {
    const index = this.chipsInfo.list.indexOf(item); // index of item selected
    if (!this.amISelected(index)) { // if index is not stored in selected array
      if (index >= 0) {
        if (!this.multiple) { // if 1 item at time can be selected
          this.indexSelected = [];
        }
        this.indexSelected.push(index);
        this.selected.emit(item);
      }
    } else {
      this.indexSelected.splice(this.indexSelected.indexOf(index), 1);
    }
  }

  // return true if there's the value in the array of index selected
  amISelected(index): boolean {
    return this.indexSelected.some(el => el === index);
  }

  remove(index: number): void {

    // const index = this.chipsInfo.list.indexOf(item);

    if (index >= 0) {
      this.chipsInfo.list.splice(index, 1);
      this.DB.updateDoc(this.DB.getDoc(`users/${this.authSer.userAuth.uid}/outlines/${this.chipsInfo.id}`), { list: this.chipsInfo.list });
    }
  }
}
