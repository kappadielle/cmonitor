import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChipListComponent } from './chip-list.component';
import { MaterialModule } from '../../material/material.module';
import { DBManagerService } from '../../services/dbmanager.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Subject } from 'rxjs';

describe('ChipListComponent', () => {
  let component: ChipListComponent;
  let fixture: ComponentFixture<ChipListComponent>;
  let DB;
  const stubAuthenticationService = {
    userAuth: {
      uid: '123ABC'
    }
  };
  const stubDBService = jasmine.createSpyObj('DB', ['updateDoc','getDoc'])

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule],
      providers: [
        {provide: DBManagerService, useValue: stubDBService},
        {provide: AuthenticationService, useValue: stubAuthenticationService}
      ],
      declarations: [ ChipListComponent ]
    })
    .compileComponents();

    DB = TestBed.get(DBManagerService)
    fixture = TestBed.createComponent(ChipListComponent);
    component = fixture.componentInstance;

    // needed to make the component works ==> @Input
    component.selectedAndInserted = new Subject();
    component.indexSelected = [0];
    component.chipsInfo = {
      id : 'container_task',
      list: ['hi', 'I', 'Am', 'Lucia'],
      static: false
    };

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('add', () => {
    spyOn(component, 'add').and.callThrough();

    component.chipsInfo.static = false;
    let input = fixture.debugElement.nativeElement.querySelector('input');
    input.value = 'new chip';
    input.dispatchEvent(new Event('blur'));
    expect(component.add).toHaveBeenCalledTimes(1);
    console.log(component.chipsInfo.list);
    expect(component.chipsInfo.list[component.chipsInfo.list.length - 1]).toBe('new chip')
    expect(DB.updateDoc).toHaveBeenCalled()

    component.chipsInfo.static = true;
    component.ngOnInit();
    fixture.detectChanges();
    input = fixture.debugElement.nativeElement.querySelector('input');
    try {
      input.value = 'cannot add'; 
      input.dispatchEvent(new Event('blur'));
    } catch (error) {}
    expect(component.add).toHaveBeenCalledTimes(1);
    expect(component.chipsInfo.list[component.chipsInfo.list.length - 1]).toBe('new chip')

  });
  it('amISelected', () => {
    let val = component.amISelected(0);
    expect(val).toBeTruthy();

    val = component.amISelected(2);
    expect(val).toBeFalsy();
  });

  it('selectMe', () => {
    component.selectMe('Am');
    expect(component.indexSelected.some(val => val == 2)).toBeTruthy();

    component.selectMe('Am');
    expect(component.indexSelected.some(val => val == 2)).toBeFalsy();
    
    component.chipsInfo.static = true;
    component.ngOnInit();
    component.selectMe('Am');
    expect(component.indexSelected.some(val => val == 2)).toBeTruthy();
    expect(component.indexSelected.some(val => val == 0)).toBeFalsy();

    component.selectMe('Am');
    expect(component.indexSelected.some(val => val == 2)).toBeFalsy();
  });

  it('remove', () => {
    let chip = component.chipsInfo.list[1];
    component.remove(1);
    expect(component.chipsInfo.list.some(val => val == chip)).toBeFalsy();
    expect(DB.updateDoc).toHaveBeenCalled()
  });

  it('selectAndInserted removing', () => {
    component.chipsInfo.static = true;
    let chip = component.chipsInfo.list[0];
    component.indexSelected = [0];
    component.ngOnInit();
    fixture.detectChanges();
    
    component.selectedAndInserted.next(true);
    expect(component.indexSelected.length).toBe(0);
    expect(component.chipsInfo.list.some(val => val == chip)).toBeFalsy();

    // reset default val for random tests
    component.indexSelected = [0];
  });

});
