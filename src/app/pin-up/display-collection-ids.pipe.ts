import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayCollectionIds'
})
export class DisplayCollectionIdsPipe implements PipeTransform {

  transform(value: string, args?: any): string {
    const regex1 = new RegExp('0', 'g');
    const regex2 = new RegExp('_', 'g');
    const regex3 = new RegExp('7', 'g');
    value = value.replace(regex1, '\'');
    value = value.replace(regex2, ' ');
    value = value.replace(regex3, '/');
    return value;
  }

}
