import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChipListComponent } from './chip-list/chip-list.component';
import { MaterialModule } from '../material/material.module';
import { DisplayCollectionIdsPipe } from './display-collection-ids.pipe';

@NgModule({
  declarations: [ChipListComponent, DisplayCollectionIdsPipe],
  exports: [ChipListComponent, DisplayCollectionIdsPipe],
  imports: [
    CommonModule,
    MaterialModule,
  ],
})
export class PinUpModule { }
