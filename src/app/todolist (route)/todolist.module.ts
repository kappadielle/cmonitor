import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TaskEditorComponent } from './task-editor/task-editor.component';
import { TodolistService } from './todolist.service';
import { MaterialModule } from '../material/material.module';
import { PinUpModule } from '../pin-up/pin-up.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [TodoListComponent, TaskEditorComponent],
  imports: [
    MaterialModule,
    CommonModule,
    PinUpModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
  ],
  providers: [TodolistService]
})
export class TodolistModule { }
