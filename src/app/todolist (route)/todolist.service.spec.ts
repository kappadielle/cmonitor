import { TestBed } from '@angular/core/testing';
import * as moment from 'moment';
import { TodolistService } from './todolist.service';
import { DBManagerService } from '../services/dbmanager.service';
import { AuthenticationService } from '../services/authentication.service';
import { TodoTask } from 'functions/src/todo-task';
import { AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { of } from 'rxjs';

describe('TodolistService', () => {
  let authSer;
  let DB;
  let todolistSer;
  
  const stubAuthenticationService = {
    userAuth: {
      uid: '123ABC'
    }
  };
  const stubDBService = {
    getDoc: (path: string) => AngularFirestoreDocument,
    getDoc$: (path: string) => of({}),
    setDoc: jasmine.createSpy('setDoc'),
    getCol: (path: string) => AngularFirestoreCollection,
    getCol$: (path: string) => of({}),
    obsOfDoc: (doc: AngularFirestoreDocument) => of({}),
    obsOfCol: (col: AngularFirestoreCollection) => of({})
  }
  
  const todolist: TodoTask[] = [
    {name: '', numRep: 1},
  ]

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TodolistService,
        {provide: DBManagerService, useValue: stubDBService},
        {provide: AuthenticationService, useValue: stubAuthenticationService}
      ]
    })
    authSer = TestBed.get(AuthenticationService);
    DB = TestBed.get(DBManagerService);
    todolistSer = TestBed.get(TodolistService);
    todolistSer.init();
    todolistSer.routine = [
      {name: 'routine1_expired', numRep: 1, routine: true, days: [1, 2, 3, 4], deadLine: {nanoseconds: 0, seconds: 1072476400}},
      {name: 'routine2', numRep: 2, routine: true, days: [0, 2, 3, 5], deadLine: {nanoseconds: 0, seconds: 1578476400}},
      {name: 'task1', numRep: 1, routine: true, days: [0, 1, 2, 3, 4, 5, 6], deadLine: {nanoseconds: 0, seconds: 1592476400}},
      {name: 'routine4_neverExpire', numRep: 4, routine: true, days: [1, 3, 6]},
    ]
    todolistSer.todoList = [
      {name: 'task1', numRep: 1, routine: true, completed: true, numDone: 0},
      {name: 'task2', numRep: 2, routine: false, completed: false, numDone: 0},
      {name: 'task3', numRep: 3, routine: false, completed: false, numDone: 0}
    ]
    todolistSer.tomorrowTodoList = [
      {name: 'task11', numRep: 1, routine: true, completed: false, numDone: 0},
      {name: 'task22', numRep: 2, routine: false, completed: true, numDone: 2},
      {name: 'task33', numRep: 3, routine: true, completed: false, numDone: 0},
      {name: 'task1', numRep: 1, routine: true, completed: true, numDone: 0},
    ]
  });

  it('should be created', () => {
    expect(todolistSer).toBeTruthy();
  });

  it('setModifyObj', () => {
    todolistSer.setModifyObj(false);
    expect(todolistSer.nextClickCancelModify).toBeFalsy();
    expect(todolistSer.modifyTask.indexOtherList).toBeFalsy();
    expect(todolistSer.modifyTask.indexNormalList).toBeFalsy();
    expect(todolistSer.modifyTask.indexForRoutine).toBeFalsy();
    expect(todolistSer.modifyTask.modify).toBeFalsy();
    
    todolistSer.setModifyObj(true);
    expect(todolistSer.modifyTask.modify).toBeTruthy();

    todolistSer.setModifyObj(true, 1);
    expect(todolistSer.modifyTask.modify).toBeTruthy();
    expect(todolistSer.modifyTask.indexNormalList).toBe(1);

    todolistSer.setModifyObj(true, 1 , 2);
    expect(todolistSer.modifyTask.modify).toBeTruthy();
    expect(todolistSer.modifyTask.indexNormalList).toBe(1);
    expect(todolistSer.modifyTask.indexForRoutine).toBe(2);

    todolistSer.setModifyObj(true, 1 , 2, 3);
    expect(todolistSer.modifyTask.modify).toBeTruthy();
    expect(todolistSer.modifyTask.indexNormalList).toBe(1);
    expect(todolistSer.modifyTask.indexForRoutine).toBe(2);
    expect(todolistSer.modifyTask.indexOtherList).toBe(3);
  });

  it('init', (done: DoneFn) => {
    todolistSer.$obj.subscribe(obj => {
      expect(obj).toBeTruthy()
      done();
    })
  })
  describe('saveCompilation', () => {
    it('create routine', () => {
      todolistSer.modifyTask.modify = false;
      let numCalls = DB.setDoc.calls.count();

      todolistSer.saveCompilation({name: 'new_routine', numRep: 5, routine: true, days: [1, 3, 6]})
      numCalls += 1; // routine
      if ([1, 3, 6].includes(moment().day())) {
        expect(todolistSer.todoList.some(val => val.name == 'new_routine')).toBeTruthy();
        numCalls += 1; // today
        expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);
      }
      if ([1, 3, 6].includes(moment().add(1, 'day').day())) {
        expect(todolistSer.tomorrowTodoList.some(val => val.name == 'new_routine')).toBeTruthy();
        numCalls += 1; // tomorrow
        expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);
      }
      expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);
    });
    it('modify routine', () => {
      todolistSer.modifyTask.modify = true;
      let indexForRoutine = 2;
      todolistSer.modifyTask.indexForRoutine = indexForRoutine;
      todolistSer.modifyTask.indexNormalList = 0;
      todolistSer.modifyTask.indexOtherList = 3;
      todolistSer.showTomorrow = false;
      let numCalls = DB.setDoc.calls.count();

      todolistSer.saveCompilation({name: 'ACAB Routine', numRep: 2, routine: true, days: [3, 6]})
      numCalls += 1; // routine
      console.log('todolistSer.routine[todolistSer.modifyTask.indexForRoutine]', todolistSer.routine[todolistSer.modifyTask.indexForRoutine])
      console.log('todolistSer.modifyTask.indexForRoutine', todolistSer.modifyTask.indexForRoutine)
      expect(isEquivalent(todolistSer.routine[indexForRoutine], {name: 'ACAB Routine', numRep: 2, routine: true, days: [3, 6]})).toBeTruthy('routine sostituita')
      expect(todolistSer.todoList.some(val => val.name == 'task1')).toBeFalsy('routine eliminata da todolist');
      expect(todolistSer.tomorrowTodoList.some(val => val.name == 'task1')).toBeFalsy('routine eliminata da tomorrowList');
      if ([3, 6].includes(moment().day())) {
        expect(todolistSer.todoList.some(val => val.name == 'ACAB Routine')).toBeTruthy();
      }
      if ([3, 6].includes(moment().add(1, 'day').day())) {
        expect(todolistSer.tomorrowTodoList.some(val => val.name == 'ACAB Routine')).toBeTruthy();
      }
      numCalls += 2;
      expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);
    });
    it('create normal today & tomorrow', () => {
      // today
      todolistSer.modifyTask.modify = false;
      todolistSer.showTomorrow = false;
      let numCalls = DB.setDoc.calls.count();

      todolistSer.saveCompilation({name: 'new normal task', numRep: 2, routine: false})
      numCalls += 1;
      expect(todolistSer.todoList.some(val => val.name == 'new normal task')).toBeTruthy('added new task to normal today');
      expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);

      // tomorrow
      todolistSer.showTomorrow = true;
      todolistSer.saveCompilation({name: 'new normal tomorrow task', numRep: 2, routine: false})
      numCalls += 1;
      expect(todolistSer.tomorrowTodoList.some(val => val.name == 'new normal tomorrow task')).toBeTruthy('added new task to normal tomorrow');
      expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);
    });
    it('modify normal today & tomorrow', () => {
      // tomorrow
      todolistSer.modifyTask.modify = true;
      let indexNormalList = 1
      todolistSer.modifyTask.indexNormalList = indexNormalList;
      todolistSer.showTomorrow = true;
      let numCalls = DB.setDoc.calls.count();

      todolistSer.saveCompilation({name: 'task modified', numRep: 4, routine: false, completed: true, numDone: 2})
      numCalls += 1;
      expect(isEquivalent(todolistSer.tomorrowTodoList[indexNormalList], {name: 'task modified', numRep: 4, routine: false, completed: true, numDone: 2}))
      .toBeTruthy('modified normal task tomorrow');
      expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);

      // today
      todolistSer.modifyTask.modify = true;
      indexNormalList = 2
      todolistSer.modifyTask.indexNormalList = indexNormalList;
      todolistSer.showTomorrow = false;
      numCalls = DB.setDoc.calls.count();

      todolistSer.saveCompilation({name: 'task modified', numRep: 2, routine: false, completed: false, numDone: 0})
      numCalls += 1;
      expect(isEquivalent(todolistSer.todoList[indexNormalList], {name: 'task modified', numRep: 2, routine: false, completed: false, numDone: 0}))
      .toBeTruthy('modified normal task today');
      expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);
      
    });
  })

  it('should remove Tasks if expired', () => {
    const numCalls = DB.setDoc.calls.count();
    let routineUpdated = todolistSer.removeIfExpired(todolistSer.routine);
    expect(todolistSer.routine).toBe(routineUpdated, 'return value & argument modified is the same');
    expect(todolistSer.routine.some(val => val.name == 'routine1_expired')).toBeFalsy('routine expired not deleted');
    expect(todolistSer.routine.length).toBe(3);
    expect(DB.setDoc).toHaveBeenCalledTimes(numCalls + 1);
    routineUpdated = todolistSer.removeIfExpired(todolistSer.routine);
    expect(todolistSer.routine.length).toBe(3);
    expect(DB.setDoc).toHaveBeenCalledTimes(numCalls + 1);
  });

  it('FormatTask4day', () => {
    const routineTask = {name: 'routine2', numRep: 2, routine: true, days: [0, 2, 3, 5], deadLine: {nanoseconds: 0, seconds: 1578476400}}
    const dayTask = todolistSer.formatTask4Day(routineTask);
    expect(dayTask).not.toBe(routineTask, 'deep value copy, not by reference');
    expect(dayTask).toEqual({name: 'routine2', numRep: 2, routine: true, completed: false, numDone: 0});
  });
  it('FormatTask4routine', () => {
    const dayTask = {name: 'routine2', numRep: 2, routine: true, completed: false, numDone: 0}
    const routineTask = todolistSer.formatTask4Routine(dayTask);
    expect(dayTask).not.toBe(routineTask, 'deep value copy, not by reference');
    expect(routineTask).toEqual({name: 'routine2', numRep: 2, routine: true});
  });

  it('getDailyTaskIfRoutineDaysMatches', () => {
    const routineTask = {name: 'routine2', numRep: 2, routine: true, days: [0, 1, 3], deadLine: {nanoseconds: 0, seconds: 1578476400}};
    let offset = 0;
    let dayTask = todolistSer.getDailyTaskIfRoutineDaysMatches(routineTask, offset);
    if (moment().add(offset, 'days').day() == 0 || moment().add(offset, 'days').day() == 1 || moment().add(offset, 'days').day() == 3) {
      expect(dayTask).toEqual(todolistSer.formatTask4Day(routineTask))
    } else {
      expect(dayTask).toBe(null)
    }
    offset = 1;
    dayTask = todolistSer.getDailyTaskIfRoutineDaysMatches(routineTask, offset);
    if (moment().add(offset, 'days').day() == 0 || moment().add(offset, 'days').day() == 1 || moment().add(offset, 'days').day() == 3) {
      expect(dayTask).toEqual(todolistSer.formatTask4Day(routineTask))
    } else {
      expect(dayTask).toBe(null)
    }
  });

  it('setDailyTaskIfRoutineDaysMatches', () => {
    const routineTask = {name: 'routine2', numRep: 2, routine: true, days: [0, 1, 3], deadLine: {nanoseconds: 0, seconds: 1578476400}};
    let offset = 0;
    todolistSer.setDailyTaskIfRoutineDaysMatches(routineTask, offset);
    if (moment().add(offset, 'days').day() == 0 || moment().add(offset, 'days').day() == 1 || moment().add(offset, 'days').day() == 3) {
      expect(todolistSer.todoList.length).toBe(4);
      expect(todolistSer.todoList.some(val => isEquivalent(val, todolistSer.formatTask4Day(routineTask)))).toBeTruthy('today');
    } else {
      expect(todolistSer.todoList.length).toBe(3);
    }
    offset = 1;
    todolistSer.setDailyTaskIfRoutineDaysMatches(routineTask, offset);
    if (moment().add(offset, 'days').day() == 0 || moment().add(offset, 'days').day() == 1 || moment().add(offset, 'days').day() == 3) {
      expect(todolistSer.tomorrowTodoList.length).toBe(5);
      expect(todolistSer.tomorrowTodoList.some(val => isEquivalent(val, todolistSer.formatTask4Day(routineTask)))).toBeTruthy('tomorrow');
    } else {
      expect(todolistSer.tomorrowTodoList.length).toBe(4);
    }
  });

  it('taskClicked', () => {
    // should not activate cause modify is on
    todolistSer.taskCompilation = {name: 'sameName', numRep: 2}
    let task = {name: 'sameName', numRep: 2, routine: true, completed: false, numDone: 0};
    const numCalls = DB.setDoc.calls.count();
    todolistSer.taskClicked(task);
    expect(task.numDone).toBe(0);
    expect(DB.setDoc).toHaveBeenCalledTimes(numCalls);

    todolistSer.taskCompilation = {name: '', numRep: 1}

    // composed
    task = {name: 'taskToClick', numRep: 2, routine: true, completed: false, numDone: 0};
    todolistSer.taskClicked(task);
    expect(task.numDone).toBe(1);
    todolistSer.taskClicked(task);
    expect(task.numDone).toBe(2);
    expect(task.completed).toBeTruthy();

    // normal task
    task = {name: 'taskToClick', numRep: 1, routine: true, completed: false, numDone: 0};
    todolistSer.taskClicked(task);
    expect(task.completed).toBeTruthy();

    expect(DB.setDoc).toHaveBeenCalledTimes(numCalls + 3);
  });
  describe('modify', () => {
    it('normal today', () => {
      todolistSer.showTomorrow = false;
      todolistSer.modify({name: 'task3', numRep: 3, routine: false, completed: false, numDone: 0}, 2);
      expect(isEquivalent(todolistSer.taskCompilation, todolistSer.todoList[2])).toBeTruthy('task Compilation normal today');
      expect(isEquivalent({name: 'task3', numRep: 3, routine: false, completed: false, numDone: 0}, todolistSer.todoList[2]))
      .toBeTruthy('task Compilation normal today');
      expect(todolistSer.modifyTask.indexNormalList).toBe(2);
      expect(todolistSer.modifyTask.modify).toBeTruthy('last modify');
    });
    it('routine', () => {
      // routine
      todolistSer.showTomorrow = false;
      todolistSer.modify({name: 'task1', numRep: 1, routine: true, completed: true, numDone: 0}, 0);
      expect(isEquivalent(todolistSer.taskCompilation, {name: 'task1', numRep: 1, routine: true, days: [0, 1, 2, 3, 4, 5, 6], deadLine: {nanoseconds: 0, seconds: 1592476400}}))
      .toBeTruthy('task Compilation routine');
      expect(isEquivalent(todolistSer.routine[todolistSer.modifyTask.indexForRoutine], todolistSer.taskCompilation))
      .toBeTruthy('task Compilation routine');
      console.log('modify:  ', todolistSer.modifyTask)
      expect(todolistSer.modifyTask.indexForRoutine).toBe(2);
      expect(todolistSer.modifyTask.indexNormalList).toBe(0);
      expect(todolistSer.modifyTask.indexOtherList).toBe(3);
      expect(todolistSer.modifyTask.modify).toBeTruthy('last modify');
    })
    it('normal tomorrow', () => {
      todolistSer.showTomorrow = true;
      todolistSer.modify({name: 'task22', numRep: 2, routine: false, completed: true, numDone: 2}, 1);
      expect(isEquivalent(todolistSer.taskCompilation, todolistSer.tomorrowTodoList[1])).toBeTruthy('task Compilation normal tomorrow');
      expect(isEquivalent({name: 'task22', numRep: 2, routine: false, completed: true, numDone: 2}, todolistSer.tomorrowTodoList[1]))
      .toBeTruthy('task Compilation normal tomorrow');
      expect(todolistSer.modifyTask.indexNormalList).toBe(1);
      expect(todolistSer.modifyTask.modify).toBeTruthy('last modify');
    })
  })

});

function isEquivalent(a, b) {
  // Create arrays of property names
  var aProps = Object.getOwnPropertyNames(a);
  var bProps = Object.getOwnPropertyNames(b);

  // If number of properties is different,
  // objects are not equivalent
  if (aProps.length != bProps.length) {
      return false;
  }

  for (var i = 0; i < aProps.length; i++) {
    var propName = aProps[i];
    if (typeof(a[propName]) == 'object') { // Viva la ricorsione
      if (!isEquivalent(a[propName],b[propName])) {return false;}
    } else if (a[propName] !== b[propName]) {
      return false;
    }
  }

  // If we made it this far, objects
  // are considered equivalent
  return true;
}