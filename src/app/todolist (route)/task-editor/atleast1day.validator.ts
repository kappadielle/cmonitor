import { FormGroup } from '@angular/forms';

// custom validator to check that two fields match
export function atleast1Day(mon: string, tue: string, wed: string, thu: string, fri: string, sat: string, sun: string, routine: string) {
    return (formGroup: FormGroup) => {
        const control = [   formGroup.controls[mon].value,
                            formGroup.controls[tue].value,
                            formGroup.controls[wed].value,
                            formGroup.controls[thu].value,
                            formGroup.controls[fri].value,
                            formGroup.controls[sat].value,
                            formGroup.controls[sun].value
        ]
        const validateRoutine = formGroup.controls[routine];
        // set error on matchingControl if validation fails
        if (!validateRoutine.value) {
            return;
        }
        if (control.some(val => val == true)) {
            validateRoutine.setErrors(null);
        } else {
            validateRoutine.setErrors({ atLeast1Day: true });
        }
    }
}