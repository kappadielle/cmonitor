import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { TaskEditorComponent } from './task-editor.component';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TodolistService } from '../todolist.service';
import { Input } from '@angular/core';
import { click } from 'src/app/testing/clickHelper';
import { By } from '@angular/platform-browser';

describe('TaskEditorComponent', () => {
  let component: TaskEditorComponent;
  let fixture: ComponentFixture<TaskEditorComponent>;
  let todoSer;

  const StubTodoListService = {
    saveCompilation: jasmine.createSpy('saveCompilation'),
    modifyTask: {modify: false, indexForRoutine: null, indexNormalList: null},
    taskCompilation: {name: 'ciao', numRep: 1},
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, ReactiveFormsModule],
      providers: [{provide: TodolistService, useValue: StubTodoListService}],
      declarations: [ TaskEditorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaskEditorComponent);
    component = fixture.componentInstance;
    todoSer = TestBed.get(TodolistService);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('formatWeekDays', () => {
    let val = component.formatWeekDays(false, true, true, true, true, true, false);
    expect(val.length).toBe(5);
    expect(val.every(val => val > 0 && val < 6)).toBe(true);
    
    val = component.formatWeekDays(false, true, true, false, false, true, false);
    expect(val.length).toBe(3);
    expect(val.some(val => val == 0)).toBeFalsy();
    expect(val.some(val => val == 6)).toBeFalsy();
    expect(val.some(val => val == 4)).toBeFalsy();
    expect(val.some(val => val == 3)).toBeFalsy();
  });

  it('completeTask', async(() => {
    // component.init();
    spyOn(component, 'saveTask').and.callThrough();
    spyOn(component, 'init').and.callThrough();
    let name = fixture.nativeElement.querySelector('input[formControlName=\'name\']');
    expect(name.value).toBe(component.task.name);

    name.value = 'new task';
    name.dispatchEvent(new Event('input'));
    const numRep = fixture.nativeElement.querySelector('input[formControlName=\'numRep\']');
    numRep.value = 3;
    numRep.dispatchEvent(new Event('input'));

    /*let routine = fixture.debugElement.query(By.css('mat-slide-toggle[formControlName=\'routine\']'));
    routine.triggerEventHandler('change', null)
    fixture.detectChanges();

    console.log('MIODIOIOO', fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'sun\']'))
    // days of week
    let sun = fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'sun\']');
    sun.value = false;
    sun.dispatchEvent(new Event('input'));
    let mon = fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'mon\']');
    mon.value = true;
    mon.dispatchEvent(new Event('input'));
    let tue = fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'tue\']');
    tue.value = true;
    tue.dispatchEvent(new Event('input'));
    let wed = fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'wed\']');
    wed.value = true;
    wed.dispatchEvent(new Event('input'));
    let thu = fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'thu\']');
    thu.value = true;
    thu.dispatchEvent(new Event('input'));
    let fri = fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'fri\']');
    fri.value = true;
    fri.dispatchEvent(new Event('input'));
    let sat = fixture.nativeElement.querySelector('mat-checkbox[formControlName=\'sat\']');
    sat.value = false;
    sat.dispatchEvent(new Event('input'));
    */

    fixture.detectChanges();

    let btnSubmit = fixture.debugElement.nativeElement.querySelector('button[type=\'submit\']');
    expect(component.taskForm.valid).toBeTruthy();
    expect(component.task.name).toBe('new task');
    click(btnSubmit);
    fixture.detectChanges()
    expect(component.saveTask).toHaveBeenCalledTimes(1);
    expect(todoSer.saveCompilation).toHaveBeenCalledTimes(1);
    expect(component.task.numRep).toBe(3);
    expect(component.task.idRoutine).toBeFalsy();
    expect(component.task.completed).toBeFalsy();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.task.name).toBe('');
      expect(component.task.numRep).toBe(1);
      expect(component.init).toHaveBeenCalledTimes(1);
    });
  }));
  it('init', () => {
    todoSer.taskCompilation = {name: '', numRep: 1};
    component.init();
    fixture.detectChanges();
    console.log(component.taskForm)
    expect(component.taskForm.valid).toBeFalsy();
  });
});
