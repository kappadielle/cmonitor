import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TodoTask } from '../todo-task (interface)';
import { trigger, transition, animate, style} from '@angular/animations';
import { atleast1Day } from './atleast1day.validator';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { firestore } from 'firebase/app';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import { ModifyTask } from '../modifyTask (interface)';
import { TodolistService } from '../todolist.service';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@Component({
  selector: 'app-task-editor',
  templateUrl: './task-editor.component.html',
  styleUrls: ['./task-editor.component.scss'],
  animations: [
    trigger('transition', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(300, style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate(300, style({ opacity: 0 }))
      ]),
      // state('*', style({ opacity: 1 })),
    ])
  ],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    {provide: MAT_DATE_LOCALE, useValue: 'it'},

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})

export class TaskEditorComponent implements OnInit {
  /*
    class used to compile tasks
  */
  task: TodoTask;
  modifyTask: ModifyTask;
  tomorrow = moment().add(1, 'days').toDate();
  taskForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private todoSer: TodolistService) { }
  ngOnInit() {
    this.init();
  }


  // initialize the form based on this.todoSer.modify && this.todoSer.taskCompilation
  init() {
    // gets info from the service
    this.modifyTask = this.todoSer.modifyTask;
    this.task = this.todoSer.taskCompilation;

    this.task = this.task ? this.task : {name: '', numRep: 1, idRoutine: null, days: [], deadLine: null};
    this.taskForm = this.formBuilder.group({
      name: [this.task.name ? this.task.name : '', [
        Validators.required,
        Validators.minLength(2)
      ]],
      numRep: [this.task.numRep ? this.task.numRep : 1, []],
      routine: [ this.task.idRoutine ? true : null, []],
      mon: [ this.task.days && this.task.days.some(val => val === 1) ? true : false, []],
      tue: [ this.task.days && this.task.days.some(val => val === 2) ? true : false, []],
      wed: [ this.task.days && this.task.days.some(val => val === 3) ? true : false, []],
      thu: [ this.task.days && this.task.days.some(val => val === 4) ? true : false, []],
      fri: [ this.task.days && this.task.days.some(val => val === 5) ? true : false, []],
      sat: [ this.task.days && this.task.days.some(val => val === 6) ? true : false, []],
      sun: [ this.task.days && this.task.days.some(val => val === 0) ? true : false, []],
      date: [ this.task.deadLine ? moment(this.task.deadLine.seconds * 1000).toDate() : null, []],
    }, {
      validator: atleast1Day('mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun', 'routine')
    });
  }

  /*
    creates the task object from form value and pass it to todoSer.saveCompilation(task)
  */
  saveTask() {
    if (this.taskForm.valid) {

      // prepare data to be emitted
      const taskToEmit: TodoTask = this.task;
      if (this.taskForm.get('routine').value) {
        // task routine
        taskToEmit.idRoutine = taskToEmit.idRoutine ? taskToEmit.idRoutine : `routine_${new Date().valueOf()}`;
        taskToEmit.days = this.formatWeekDays(  this.taskForm.get('sun').value,
                                                this.taskForm.get('mon').value,
                                                this.taskForm.get('tue').value,
                                                this.taskForm.get('wed').value,
                                                this.taskForm.get('thu').value,
                                                this.taskForm.get('fri').value,
                                                this.taskForm.get('sat').value);
        const momentDeadLine = this.taskForm.get('date').value;
        if (momentDeadLine) { // set the deadline using Timestamp
          taskToEmit.deadLine = firestore.Timestamp.fromMillis(momentDeadLine.valueOf());
        }
      } else {
        // daily task
        taskToEmit.idRoutine = null;
      }
      taskToEmit.completed = false;
      delete taskToEmit.percentage;
      taskToEmit.numRep = this.taskForm.get('numRep').value;
      if (taskToEmit.numRep > 1) {
        taskToEmit.numDone = 0;
      }

      // emit
      this.todoSer.saveCompilation(JSON.parse(JSON.stringify(taskToEmit)));
      /*
        passed with this funky JSON.job() because it's needed to get passed by value and not by reference
        if it get passed by reference the setName below will reset name value and :((((
      */

      setTimeout(() => {
        try {
          this.init(); // reinitialize the component so the right data can be displayed
        } catch (err) {
          // console.log(err);
        }
      }, 20); // set later to give time to taskEditor to fill with the right taskCompilation
      // clean form
      this.todoSer.taskCompilation = {name: '', numRep: 1};
      this.taskForm.get('name').setValue('');
      this.taskForm.get('mon').setValue(false)
      this.taskForm.get('tue').setValue(false)
      this.taskForm.get('wed').setValue(false)
      this.taskForm.get('thu').setValue(false)
      this.taskForm.get('fri').setValue(false)
      this.taskForm.get('sat').setValue(false)
      this.taskForm.get('sun').setValue(false)
      // setTimeout(_ => this.taskForm.get('name').setValue(''), 0)
      this.taskForm.get('date').setValue('');
      // this.taskForm.updateValueAndValidity();
    }
  }
  /*
    tranform an array of boolean to an array of number | null: [1, 3, 5]
  */
  formatWeekDays(sun, mon, tue, wed, thu, fri, sat): number[] {
    let weekDays = [ sun, mon, tue, wed, thu, fri, sat ];
    for (let i = 0; i < 7; i++) {
      weekDays[i] = (weekDays[i] ? i : null);
    }
    weekDays = weekDays.filter(val => val !== null);
    return weekDays;
  }
  get f() { return this.taskForm.controls; }
  get routineVal(): boolean { return this.taskForm.get('routine').value; }
  set routineVal(val: boolean) { this.taskForm.get('routine').setValue(val); } // don't know why, but setting the value like this will set the opposite of asked

}
