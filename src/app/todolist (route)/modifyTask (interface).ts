export interface ModifyTask {
    modify: boolean;
    indexForRoutine: number;
    indexOtherList: number;
    indexNormalList: number;
}
