import { Injectable } from '@angular/core';
import { TodoTask } from './todo-task (interface)';
import * as moment from 'moment';
import { AuthenticationService } from '../services/authentication.service';
import { DBManagerService } from '../services/dbmanager.service';
import { Subject, Observable, forkJoin, of } from 'rxjs';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { ModifyTask } from './modifyTask (interface)';
import { take, map, takeWhile, timeout, catchError } from 'rxjs/operators';

@Injectable()
export class TodolistService {

  /*
    requirements: needs to have the authSer.authUser before initialize => for this reason we have auth guard

    glossary:
    - tasks = routine & normal task 
    - routine = a task that is going to be reproposed until it expire and if it's the right week day
    - composed = a task that needs n° steps to be completed

    What the class does:
    1) manages today & tomorrow tasks
    2) creates tasks
    3) modify tasks
    4) complete or make progress (task could be "composed") on a task

  */
  todoList: TodoTask[] = []; // used to keep track of todolist, so it can be updated with this Obj
  tomorrowTodoList: TodoTask[] = []; // used to keep track of todolist, so it can be updated with this Obj
  showTomorrow = false;
  $selectedAndInserted: Subject<boolean>; // used to eliminate chips when task is inserted with chip selection
  $todayList: Observable<TodoTask[]>; // used to display and use todolist
  taskCompilation: TodoTask = {name: '', numRep: 1}; // object which contains task to create or task to edit
  routine: TodoTask[]; // contains routine
  docTodayPlanning: AngularFirestoreDocument<any>; // needed to do updates to todolist
  docTomorrowPlanning: AngularFirestoreDocument<any>; // needed to do updates to tomorrow todolist
  docRotuine: AngularFirestoreDocument<any>; // needed to do updates to todolist
  docContainerTask: AngularFirestoreDocument<any>;
  modifyTask: ModifyTask = {modify: false, indexForRoutine: null, indexNormalList: null, indexOtherList: null}; // obj used to modify
  $obj: Observable<any>; // observable that comes out from forkJoin
  nextClickCancelModify = false; // neeeded to know when a click on the task is used to stop modifying
  waitUpdateFromGFunc = {val: false}; // used to unsubscribe the todolist after reproposed task comes, it's an object to make available the pass by reference
  wait1time = false; // used to unsubscribe the todolist after reproposed task comes
  timeout: any;
  updatesBuffer: TodoTask[] = []; // gather all the task that has to be updated later

  constructor(private authSer: AuthenticationService, private DB: DBManagerService) {}

  /*
    initialize tomorrowList, todolist, Obj4ContainerTask($selectedAndInserted)
    return them
  */
  init(): any {
    this.$selectedAndInserted = new Subject<boolean>();

    // prepare document
    this.docContainerTask = this.DB.getDoc(`users/${this.authSer.userAuth.uid}/outlines/container_Task`);
    this.docTodayPlanning = this.DB.getDoc(`users/${this.authSer.userAuth.uid}/planning/${moment().startOf('day').valueOf()}`);
    this.docTomorrowPlanning = this.DB.getDoc(`users/${this.authSer.userAuth.uid}/planning/${moment().add(1, 'day').startOf('day').valueOf()}`);
    this.docRotuine = this.DB.getDoc(`users/${this.authSer.userAuth.uid}/planning/routine`);

    // reset var
    this.tomorrowTodoList = [];
    this.todoList = [];
    
    this.$obj = forkJoin({
      routine: this.DB.obsOfDoc(this.docRotuine).pipe(take(1)),
      tomorrowList: this.DB.obsOfDoc(this.docTomorrowPlanning).pipe(take(1)),
      containerTask: this.DB.obsOfDoc(this.docContainerTask).pipe(take(1))
    }).pipe(
      map(obj => {
        this.routine = obj.routine.todolist;
        obj.containerTask = {...obj.containerTask, static: true, id: 'container_Task'}; // customize container task to be properly set

        if (!obj.tomorrowList) { // there's no document for today todolist
          this.waitUpdateFromGFunc.val = true;
          this.routine = this.removeIfExpired(this.routine);
          this.routine.forEach(task => {
            // return the tomorrow.todolist filled with routines tasks cause the document was empty
            if (!task.deadLine || !moment(task.deadLine.seconds * 1000).isBefore(moment().add(1, 'day').startOf('day'))) { // prevent task that expires tomorrow to be added in tomorrow.todolist => can't delete it cause needed in today.todolist
              this.setDailyTaskIfRoutineDaysMatches(task, 1);
            }
          });
          obj.tomorrowList = {todolist: this.tomorrowTodoList}; // pass by reference to make list upgradable
          this.DB.setDoc(this.docTomorrowPlanning, obj.tomorrowList);  // even if the todoList is not filled, it sets the correct value cause this.todolist is initialized as []
        } else { // there's already a document for tomorrow todolist
          this.tomorrowTodoList = obj.tomorrowList.todolist; // pass by reference to make list upgradable
        }
        return obj;
      })
    );
    // todayList does not complet on the first emission cause updates could come from GFunc
    this.$todayList = this.DB.obsOfDoc(this.docTodayPlanning).pipe(
      /* when document isn't already created, we should wait update from GFuncs
        then it works realtime at first time document created and like a promise when there's already one*/
      takeWhile(_ => {
        if (this.waitUpdateFromGFunc.val) { // yes document and ready to receive last reproposed value
          return true;
        } else if (!this.waitUpdateFromGFunc.val) { // no update waited
          return false;
        }
      }, true), // true as second parameter means that the first value that didn't pass the predicate passes
      timeout(10000), // max time that reproposed task has to join the todolist
      catchError(err => {
        if (err.name === 'TimeoutError') {
          this.waitUpdateFromGFunc.val = false;
          this.flushAndSendUpdateBuffer();
          return of({todolist: this.todoList});
        }
      }),
      map(todayList => { // the routine is always ready cause "async pipe" for today is nested in "obj" above
        if (!todayList) { // there's no document for today todolist NEVER HAPPEN
          this.routine.forEach(task => {
            // return the todayList.todolist filled with routines tasks cause the document was empty
            this.setDailyTaskIfRoutineDaysMatches(task);
          });
          todayList = {todolist: this.todoList}; // pass by reference to make list upgradable
          this.DB.setDoc(this.docTodayPlanning, todayList);
        } else if (todayList && !this.waitUpdateFromGFunc.val) { // there's already a document for today todolist and there's no update to wait for
          this.todoList = todayList.todolist; // pass by reference to make list upgradable
        } else if (todayList && this.waitUpdateFromGFunc.val) { // when from first doc created you want all the emissions
          todayList.todolist.forEach((item: TodoTask) => {
            if (item.reproposed) {
              delete item.reproposed;
              this.waitUpdateFromGFunc.val = false;
            }
          });
          this.todoList = todayList.todolist; // pass by reference to make list upgradable
          if (!this.waitUpdateFromGFunc.val) {
            this.flushAndSendUpdateBuffer();
          }
        }
        return todayList;
      }),
    );
    return {obj: this.$obj, todayList: this.$todayList, selsub: this.$selectedAndInserted };
  }

  /*
    arg: routine
    return: routine without expired task

    side effects: update the routine in the DB if something expires tomorrow
  */
  private removeIfExpired(tasks: TodoTask[]): TodoTask[] {
    let somethingExpired = false;
    tasks.forEach((task, i) => {
      if (task.deadLine) {
        if (moment(task.deadLine.seconds * 1000).isBefore(moment().add(1, 'day').startOf('day'))) { // if the deadline day is before today
          tasks.splice(i, 1);
          somethingExpired = true;
        }
      }
    })
    if (somethingExpired) {this.DB.updateDoc(this.docRotuine, {todolist: tasks})}
    return tasks;
  }

  /*
                                                                    if() Structure :
    saves and modify                                                  -routine Yes
    normal & routine tasks                                                -modify Yes
                                                                              -tomorrow
                                                                              -today
    requirements:                                                         -modify No
    - modifyObj compiled properly                                             -tomorrow&today
      if you need to modify something                                 -routine No
                                                                          -modify Yes
                                                                              -tomorrow
                                                                              -today
                                                                          -modify No
                                                                              -tomorrow
                                                                              -today
  */
  saveCompilation(task: TodoTask) {
    if (task.idRoutine) {
      // routine
      if (this.modifyTask.modify) {
        // modify routine
        this.routine[this.modifyTask.indexForRoutine] = this.formatTask4Routine(task);

        // eliminate the old one clicked whether or not it's inserted in the current view (today|tomorrow) cause it's going to be re-inserted
        if (this.showTomorrow) {
          this.tomorrowTodoList.splice(this.modifyTask.indexNormalList, 1);
          if (this.modifyTask.indexOtherList === 0 || this.modifyTask.indexOtherList > 0) { // delete the routine in the other list with the index set from modify
            this.todoList.splice(this.modifyTask.indexOtherList, 1);
          }
        } else {
          this.todoList.splice(this.modifyTask.indexNormalList, 1);
          if (this.modifyTask.indexOtherList === 0 || this.modifyTask.indexOtherList > 0) { // delete the routine in the other list with the index set from modify
            this.tomorrowTodoList.splice(this.modifyTask.indexOtherList, 1);
          }
        }
        this.setDailyTaskIfRoutineDaysMatches(task, 1);
        this.DB.updateDoc(this.docTomorrowPlanning, {todolist: this.tomorrowTodoList});

        this.setDailyTaskIfRoutineDaysMatches(task);
        this.DB.updateDoc(this.docTodayPlanning, {todolist: this.todoList});
        this.setModifyObj(false);
      } else {
        // create routine
        this.routine.push(this.formatTask4Routine(task));

        // checks if routine task that is going to be saved is contained in today||tomorrow => set to DB
        const todayTasks = this.getDailyTaskIfRoutineDaysMatches(task);
        if (todayTasks) {
          this.todoList.push(todayTasks);
          this.DB.updateDoc(this.docTodayPlanning, {todolist: this.todoList});
        }
        const tomorrowTasks = this.getDailyTaskIfRoutineDaysMatches(task, 1);
        if (tomorrowTasks) {
          this.tomorrowTodoList.push(tomorrowTasks)
          this.DB.updateDoc(this.docTomorrowPlanning, {todolist: this.tomorrowTodoList});
        }
      }
      this.DB.updateDoc(this.docRotuine, {todolist: this.routine}); // saves routine to DB
    } else {
      // normal
      if (this.modifyTask.modify) {
        // modify daily task
        if (!this.showTomorrow) {
          // today
          this.todoList[this.modifyTask.indexNormalList] = task;
          this.DB.updateDoc(this.docTodayPlanning, {todolist: this.todoList});
        } else {
          // tomorrow
          this.tomorrowTodoList[this.modifyTask.indexNormalList] = task;
          this.DB.updateDoc(this.docTomorrowPlanning, {todolist: this.tomorrowTodoList});
        }
        this.setModifyObj(false);
      } else {
        // create daily task
        if (!this.showTomorrow) {
          // today
          this.todoList.push(this.formatTask4Day(task));
          this.DB.updateDoc(this.docTodayPlanning, {todolist: this.todoList});
        } else {
          // tomorrow
          this.tomorrowTodoList.push(this.formatTask4Day(task));
          this.DB.updateDoc(this.docTomorrowPlanning, {todolist: this.tomorrowTodoList});
        }
      }
    }

    // pass obs that emits selected&inserted values to chipscomponent, so it can eliminate that chip inserted
    this.$selectedAndInserted.next(true);
  }

  private formatTask4Routine(task: TodoTask): TodoTask {
    // it creates a deep copy of the variable, so it doesn't affect the normal behavior of the list
    const taskReturned = JSON.parse(JSON.stringify(task));
    delete taskReturned.completed;
    delete taskReturned.numDone;
    return taskReturned;
  }
  private formatTask4Day(task: TodoTask): TodoTask {
    // it creates a deep copy of the variable, so it doesn't affect the normal behavior of the list
    const taskReturned = JSON.parse(JSON.stringify(task));
    taskReturned.completed = task.completed ? task.completed : false;
    taskReturned.idRoutine = task.idRoutine ? task.idRoutine : null;
    taskReturned.numDone = taskReturned.numDone ? taskReturned.numDone : 0;
    delete taskReturned.days;
    delete taskReturned.deadLine;
    return taskReturned;
  }

  /*
    this fn checks if a routine task has to be added to the day selected

    args:
      1) 1 routine task
      2) days to add from today to select the correct day (0 = today, 1 = tomorrow ....)

    return:
      null => task doesn't have to be added
      task => the task that has to be added
  */
  private getDailyTaskIfRoutineDaysMatches(task: TodoTask, offset = 0 ): TodoTask {
    let val = null;
    // checks if routineTask.weekdays contains the weekday of the day selected
    if (task.days.some(day => day === moment().add(offset, 'days').day())) { // check if routine has to be added to todayTasks
      val = this.formatTask4Day(task);
    }
    return val;
  }

  /*
    this fn adds in tomorrow & today todolist the routine task if it should be added

    args:
      1) 1 routine task
      2) days to add from today to select the correct day (0 = today, 1 = tomorrow ....)

    side effects:
      it pushes the task in this.todoList & this.tomorrowTodoList
  */
  private setDailyTaskIfRoutineDaysMatches(task: TodoTask, offset = 0 ) {
    const dayTask = this.getDailyTaskIfRoutineDaysMatches(task, offset);
    if (dayTask) {
      if (offset == 0) {
        this.todoList.push(dayTask);
      } else if (offset == 1) {
        this.tomorrowTodoList.push(dayTask);
      }
    }
  }

/*
    activable with a long press on task.
    it gets ready the component which has the purpouse of modify

    args:
      1) task to modify
      2) index in the local array

    side effects:
      - compile the taskCompilation => task that is passed to taskEditor
      - compile the modifyObj
  */
  modify(task: TodoTask, i: number) {
    this.setModifyObj(true, i);
    if (task.idRoutine) { // modify routine task
      // checks if the other todolist has the routine in the list
      let indexOtherList;
      if (this.showTomorrow) {
        indexOtherList = this.todoList.findIndex(val => val.idRoutine == task.idRoutine);
      } else {
        indexOtherList = this.tomorrowTodoList.findIndex(val => val.idRoutine == task.idRoutine);
      }
      if (indexOtherList < 0) {
        indexOtherList = null;
      }
      this.setModifyObj(true, i, this.routine.findIndex(val => val.idRoutine == task.idRoutine), indexOtherList);
      this.taskCompilation = this.routine[this.modifyTask.indexForRoutine];
      this.taskCompilation.idRoutine = task.idRoutine;  // useless
    } else { // modifying normal task
      if (this.showTomorrow) { // tomorrow
        this.taskCompilation = this.tomorrowTodoList[this.modifyTask.indexNormalList];
      } else { // today
        this.taskCompilation = this.todoList[this.modifyTask.indexNormalList];
      }
      this.taskCompilation.idRoutine = null;
    }
  }
  /*
    used to Modify this.modifyTask obj

    if first arg == false => all others values is resetted to null
    every other args: if you pass a value "!= undefined" the value will be setted, otherwise nothing happens
  */
  setModifyObj(val: boolean, indexNormal?: number, indexRoutine?: number, indexOtherList?: number) {
    this.modifyTask.modify = val;
    if (indexNormal != undefined) {
      this.modifyTask.indexNormalList = indexNormal;
    }
    if (indexRoutine != undefined) {
      this.modifyTask.indexForRoutine = indexRoutine;
    }
    if (indexOtherList != undefined) {
      this.modifyTask.indexOtherList = indexOtherList;
    }
    if (!val) {
      this.nextClickCancelModify = false;
      this.modifyTask = {modify: false, indexForRoutine: null, indexNormalList: null, indexOtherList: null};
    }
  }

  /*
    it's used when user want to update the progression of a task
    Only today task can be completed | progressed

    side effects: fill the updatesBuffer when new data is coming from Cloud Functions instead of updating the DB
  */
  taskProgress(task, fromTomorrow = false, index: number) {
    if (!fromTomorrow) {
      if (task.numRep > 1) { // composed task
        if (task.numDone < task.numRep) {
          task.numDone += 1;
          if (task.numDone === task.numRep) {
            task.completed = true;
            task.percentage = 100;
          }
        }
      } else { // DoneOrNot task
        if (!task.completed) {
          task.completed = true;
          task.numDone = 1;
          task.percentage = 100;
        }
      }
      // updates might coming, so do not send the updates to the DB to avoid overwrite
      if (this.waitUpdateFromGFunc.val) {
        const val = JSON.parse(JSON.stringify(task)); // need to lost the reference
        this.updatesBuffer[index] = val;
      } else { // no update waiting, normal behavior
        // this clear and set timeout make less likely that the same todolist document is update (from cloudFunc and from App) twice in the same moment
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          this.DB.updateDoc(this.docTodayPlanning, {todolist: this.todoList});
        }, 1000); // delay the update so when the todolist is realtime the animation can behave normally
      }
    }
  }

  /*
    checks if the updates buffer contains value
    if yes, merge it with todolist and update the doc
  */
  flushAndSendUpdateBuffer() {
    if (this.updatesBuffer.length) {
      this.updatesBuffer.forEach((val, j) => {
        this.todoList[j] = val;
      });
      setTimeout(() => { // set little delay to prevent the same document to be updated twice at the same moment
        this.DB.updateDoc(this.docTodayPlanning, {todolist: this.todoList}).then(_ => this.updatesBuffer = []);
      }, 500);
    }
  }

  /*
    used to set the percentage of completing when a task is completed
  */
  updatePercentage(task: TodoTask, val) {
    if (val >= 0 && task.percentage !== val) {
      task.percentage = Number(val);
      this.DB.updateDoc(this.docTodayPlanning, {todolist: this.todoList});
    }
  }
}