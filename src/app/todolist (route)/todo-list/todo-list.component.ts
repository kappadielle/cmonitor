import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TodoTask } from '../todo-task (interface)';
import { trigger, transition, style, animate} from '@angular/animations';
import { TaskEditorComponent } from '../task-editor/task-editor.component';
import { TodolistService } from '../todolist.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  animations: [
    trigger('transition', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(150, style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate(150, style({ opacity: 0 }))
      ]),
    ]),
    trigger('transition2', [
      transition(':leave', [
        animate(150, style({ opacity: 0 }))
      ]),
    ]),
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ height: 0, opacity: 0 }),
            animate('350ms ease-out', 
                    style({ height: 50, opacity: 1 }))
          ]
        ),
      ]
    ),
    trigger(
      'loadingAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ height: 0, opacity: 0 }),
            animate('350ms ease-out', 
                    style({ height: 50, opacity: 1 }))
          ]
        ),
        transition(
          ':enter', 
          [
            style({ height: 50, opacity: 0 }),
            animate('350ms ease-out', 
                    style({ height: 0, opacity: 1 }))
          ]
        ),
      ]
    ),
  ],
})
export class TodoListComponent implements OnInit, OnDestroy {
  @ViewChild(TaskEditorComponent, {static: false}) taskEditorComp: TaskEditorComponent;
  showTaskEditor = false;
  $selectedAndInserted: Subject<boolean>; // chips selected and inserted with task editor
  $todayList: Observable<TodoTask[]>; // used to display and use todolist
  showTomorrow = false; // used to compile the tomorrow todolist
  $obj: Observable<any>;
  waitUpdateFromGFunc = {val: false};
  constructor(private todoSer: TodolistService) { }

  ngOnInit() {
    const todoBigObj = this.todoSer.init();
    this.$obj = todoBigObj.obj;
    this.$todayList = todoBigObj.todayList;
    this.$selectedAndInserted = todoBigObj.selsub;
    this.todoSer.setModifyObj(false);
    this.todoSer.showTomorrow = this.showTomorrow;
    this.waitUpdateFromGFunc = this.todoSer.waitUpdateFromGFunc;
  }

  ngOnDestroy() {
    // it has to be unsubscribed here cause here is where the service stops working and all 3 instances of it is linked by reference.
    if (this.$selectedAndInserted) {
      this.$selectedAndInserted.unsubscribe();
    }
  }

  // called when user clicks on 1 chip
  chipSelected(name: string) {
    this.todoSer.taskCompilation.name = name;
  }
  // return an array which length is "num", used for composed tasks *ngFor
  getArrayFromNumber(num): any[] {
    return new Array(num);
  }

  /*
    open the task editor component
  */
  modify(task: TodoTask, i: number) {
    this.todoSer.modify(task, i);
    this.showTaskEditor = true;
    try {
      this.taskEditorComp.init();
    } catch (err) {
      // console.log(err);
    }
  }
  taskClicked(task: TodoTask, fromTomorrow = false, i: number) {
    // we do this funky job here cause the modifyFn it's called with long-press while task progress is called with click
    // click is composed of down & up. this if() has the aim to prevent item that you want to modify to be completed|progressed
    if (this.todoSer.taskCompilation && this.todoSer.modifyTask.indexNormalList == i) {
      console.log(this.todoSer.nextClickCancelModify)
      if (this.todoSer.nextClickCancelModify) {
        this.openCloseTaskEditor(false); // also reset modify obj and todoser.nextClickCancelModify to false
      } else {
        this.todoSer.nextClickCancelModify = true;
      }
      // when the task sended to taskEditor(this.taskCompilation) has the same index of the task clicked (task from args)
      // it should be the task that I am trying to edit, then don't update with click()
    } else {
      this.todoSer.taskProgress(task, fromTomorrow, i);
    }
  }
  updatePercentage(task, val) {
    this.todoSer.updatePercentage(task, val);
  }
  switchTodayTomorrow() {
    this.showTomorrow = !this.showTomorrow;
    this.todoSer.showTomorrow = this.showTomorrow;
    this.openCloseTaskEditor(false);
  }
  openCloseTaskEditor(val: boolean) {
    this.todoSer.taskCompilation =  {name: '', numRep: 1};
    try {
      this.taskEditorComp.init();
    } catch (err) {
      // console.log(err);
    }
    this.todoSer.setModifyObj(false);
    this.showTaskEditor = val;
  }

  /*
    STYLE HELPERS
  */
  // return style passed to [ngStyle] in template to decorate properly every secondary task
  calculateStyle(index, task) {
    let background_c = '';
    // this workaround is used to make nested div colors 'rgba(177,177,177,0.2)' when trying to modify
    let indexList = this.todoSer.todoList.findIndex(val => val.name == task.name && val.numRep == task.numRep);
    if (this.todoSer.modifyTask.modify && this.todoSer.modifyTask.indexNormalList == indexList && (index < task.numDone)) {
      background_c = 'rgba(177,177,177,0.2)';
    } else if (index < task.numDone) {
      background_c = '#fa8142';
    } else {
      background_c = '';
    }
    return {
      'width.%': 100 / task.numRep,
      'left.%': (100 / task.numRep) * index,
      'background-color': background_c
    };
  }
  trackByFn(index, item) {
    return index;
  }

  // assign right background-color to every completed/notCompleted task
  getStyle4CompletedTask(task: TodoTask, i: number) {
    return {
      'background-color': task.completed ? '#fa8142' : '',
      'animation-name': this.todoSer.modifyTask.modify && i == this.todoSer.modifyTask.indexNormalList ? 'modify' : '',
    };
  }
}

