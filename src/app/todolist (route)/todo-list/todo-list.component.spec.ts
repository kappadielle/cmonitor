import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListComponent } from './todo-list.component';
import { MaterialModule } from '../../material/material.module';
import { Component, Input } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { ChipsInfo } from 'src/app/pin-up/chips.info (interface)';
import { TodolistService } from '../todolist.service';
import { By } from '@angular/platform-browser';

@Component({selector: 'app-chip-list', template: ''})
class StubChipListComponent {
  @Input() selectedAndInserted: Subject<string>;
  @Input() chipsInfo: ChipsInfo;
}

@Component({selector: 'app-task-editor', template: ''})
class StubTaskEditorComponent {
  init = jasmine.createSpy('init');
}

const StubTodoListService = {
  init: jasmine.createSpy('init'),
  modify: jasmine.createSpy('modify'),
  taskClicked: jasmine.createSpy('taskClicked'),
  setModifyObj: jasmine.createSpy('setModifyObj'),
  $obj: of({
    routine: {todolist:[]} ,
    todayList: {todolist: []},
    containerTask: {list: ['hi', 'I', 'Am', 'Lucia'], static: true, id: 'container_Task'}
  }),
  $tomorrowTodoList: of({todolist:[]}),
  $selectedAndInserted: of('', '', '', '', '', '', ''),
  modifyTask : {modify: false, indexForRoutine: null, indexNormalList: null},
  taskCompilation: {name: '', numRep: 1},
  showTomorrow: false,
  todoList: [
    {name: 'task1', numRep: 1, routine: true, completed: true, numDone: 0},
    {name: 'task2', numRep: 2, routine: false, completed: false, numDone: 0},
    {name: 'task3', numRep: 3, routine: false, completed: false, numDone: 0}
  ]
}

describe('TodoListComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;
  let todoSer;
  let taskEditorComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ MaterialModule ],
      declarations: [
        TodoListComponent,
        StubTaskEditorComponent,
        StubChipListComponent
      ],
      providers: [{provide: TodolistService, useValue: StubTodoListService}]
      // schemas: [NO_ERRORS_SCHEMA]
    })
    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    todoSer = fixture.debugElement.injector.get(TodolistService);
    taskEditorComponent = new StubTaskEditorComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('chipSelected', () => {
    component.chipSelected('crabby patty');
    expect(todoSer.taskCompilation.name).toBe('crabby patty');
  });
  it('getArrayFromNumber', () => {
    const arr = component.getArrayFromNumber(3);
    expect(arr.length).toBe(3);
  });
  it('modify', () => {
    component.modify({name: 'cicci', numRep: 2, completed: false}, 2);
    expect(todoSer.modify).toHaveBeenCalledWith({name: 'cicci', numRep: 2, completed: false}, 2);
    expect(component.showTaskEditor).toBe(true);
    fixture.detectChanges();
  });
  it('taskClicked', () => {
    component.taskClicked({name: 'cicci', numRep: 2, completed: false});
    expect(todoSer.taskClicked).toHaveBeenCalledWith({name: 'cicci', numRep: 2, completed: false}, todoSer.showTomorrow);
  });
  it('switchTodayTomorrow', () => {
    spyOn(component, 'openCloseTaskEditor');
    component.switchTodayTomorrow();
    expect(todoSer.showTomorrow).toBeTruthy('show tomorrow is true');
    expect(component.showTomorrow).toBeTruthy('show tomorrow is true');

    component.switchTodayTomorrow();
    expect(todoSer.showTomorrow).toBeFalsy('show tomorrow is true');
    expect(component.showTomorrow).toBeFalsy('show tomorrow is true');
    expect(component.openCloseTaskEditor).toHaveBeenCalledWith(false);
    expect(component.openCloseTaskEditor).toHaveBeenCalledTimes(2);
  });
  it('switchTodayTomorrow', () => {
    component.openCloseTaskEditor(true);
    expect(todoSer.taskCompilation.name).toBe('');
    expect(todoSer.taskCompilation.numRep).toBe(1);

    expect(todoSer.setModifyObj).toHaveBeenCalledWith(false);
    expect(component.showTaskEditor).toBe(true);
  });
  it('calculateStyle', () => {
    let val = component.calculateStyle(2, {name: 'task3', numRep: 3, routine: false, completed: false, numDone: 0});
    expect(val["background-color"]).toBe('');
    expect(val["width.%"]).toBe(100 / 3);
    expect(val["left.%"]).toBe((100 / 3) * 2);

    val = component.calculateStyle(1, {name: 'task3', numRep: 3, routine: false, completed: false, numDone: 2});
    expect(val["background-color"]).toBe('#fa8142');
    expect(val["width.%"]).toBe(100 / 3);
    expect(val["left.%"]).toBe((100 / 3) * 1);

    todoSer.modifyTask.modify = true;
    todoSer.modifyTask.indexNormalList = 2;
    val = component.calculateStyle(1, {name: 'task3', numRep: 3, routine: false, completed: false, numDone: 2});
    expect(val["background-color"]).toBe('rgba(177,177,177,0.2)');
    expect(val["width.%"]).toBe(100 / 3);
    expect(val["left.%"]).toBe((100 / 3) * 1);
  });
  it('trackByFn', () => {
    let val = component.trackByFn(1,  {name: 'task1', numRep: 3})
    expect(val).toBe('task1');

    val = component.trackByFn(1,  null)
    expect(val).toBe(null);
  });
  it('getStyle4CompletedTask', () => {
    todoSer.modifyTask.modify = false;
    let val = component.getStyle4CompletedTask({name: 'task3', numRep: 3, routine: false, completed: true, numDone: 2}, 2) 
    expect(val["background-color"]).toBe('#fa8142');
    expect(val["animation-name"]).toBe('');

    val = component.getStyle4CompletedTask({name: 'task3', numRep: 3, routine: false, completed: false, numDone: 2}, 2) 
    expect(val["background-color"]).toBe('');
    expect(val["animation-name"]).toBe('');

    todoSer.modifyTask.modify = true;
    todoSer.modifyTask.indexNormalList = 2;
    val = component.getStyle4CompletedTask({name: 'task3', numRep: 3, routine: false, completed: true, numDone: 2}, 2) 
    expect(val["background-color"]).toBe('#fa8142');
    expect(val["animation-name"]).toBe('modify');
  });
});
