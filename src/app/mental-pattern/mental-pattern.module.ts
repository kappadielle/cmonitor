import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MentalPatternService } from './mental-pattern.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [MentalPatternService]
})
export class MentalPatternModule { }
