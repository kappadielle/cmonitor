import { Mantra } from './mantra (interface)';
import { firestore } from 'firebase/app';

export interface MentalPattern {
    category: 'keep' | 'begin' | 'remove' | 'reduce' | 'removed';
    relations: string[];
    name: string;
    mantras?: Mantra[];
    updated?: firestore.Timestamp;
}
