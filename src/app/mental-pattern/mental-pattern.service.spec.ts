import { TestBed } from '@angular/core/testing';

import { MentalPatternService } from './mental-pattern.service';

describe('MentalPatternService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MentalPatternService = TestBed.get(MentalPatternService);
    expect(service).toBeTruthy();
  });
});
