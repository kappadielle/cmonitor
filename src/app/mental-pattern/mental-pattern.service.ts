import { Injectable } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { DBManagerService } from '../services/dbmanager.service';
import { AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { firestore } from 'firebase/app';
import { MentalPattern } from './mental-pattern (interface)';

@Injectable()

export class MentalPatternService {
  
  docMentalLightList: AngularFirestoreDocument<any>;
  colMentalList: AngularFirestoreCollection<any>;

  mentalLightList: any;

  constructor(private authSer: AuthenticationService, private DB: DBManagerService) {
    this.colMentalList = this.DB.getCol(`users/${this.authSer.userAuth.uid}/mentalPatterns`);
    this.docMentalLightList = this.DB.getDoc(`users/${this.authSer.userAuth.uid}/mentalPatterns/list`);
  }

  getMentalPatternLightList$(): Observable<any> {
    return this.DB.obsOfDoc(this.docMentalLightList).pipe(
      take(1),
      tap(obj => this.mentalLightList = obj)
    );
  }

  addNewMentalPattern(newMentalPattern: MentalPattern): any {
    return this.colMentalList.add({name: newMentalPattern.name, relations: newMentalPattern.relations});
  }
  addNewMentalPatternLightList(newMentalPattern: MentalPattern, uid: string) {
    this.DB.updateDoc(this.docMentalLightList, {[newMentalPattern.category]: firestore.FieldValue.arrayUnion({name: newMentalPattern.name, id: uid})});
  }
  updateMentalPatternRelations(id: string, relations: string[]) {
    this.DB.getDoc(`users/${this.authSer.userAuth.uid}/mentalPatterns/${id}`).get().toPromise().then(doc => {
      const obj = doc.data();
      const relationsToAdd = [];
      relations.forEach(argRel => {
        if (!obj.relations.some(dbRel => dbRel === argRel)) { // if dbRelations hasn't this argsRelation, update the DB
          relationsToAdd.push(argRel);
        }
      });
      if (relationsToAdd.length) {
        this.DB.updateDoc(`users/${this.authSer.userAuth.uid}/mentalPatterns/${id}`, { relations: firestore.FieldValue.arrayUnion(...relationsToAdd)});
      }
    });
  }
}
