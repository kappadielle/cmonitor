export interface Need {
    idNeed: string;
    name: string;
    whyImportant: string;
    meaning: string;

    // utility values
    areYouSure?: boolean;
    // definitive?: boolean;
}
