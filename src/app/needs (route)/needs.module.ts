import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { NeedsComponent } from './needs/needs.component';
import { NeedsSetupComponent } from './needs-setup/needs-setup.component';
import { OIBAFormComponent } from './oiba-form/oiba-form.component';
import { OIBAListComponent } from './oiba-list/oiba-list.component';
import { NeedsService } from './needs.service';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxAudioPlayerModule } from 'ngx-audio-player';

@NgModule({
  declarations: [NeedsComponent, NeedsSetupComponent, OIBAFormComponent, OIBAListComponent],
  imports: [
    CommonModule,
    NgxAudioPlayerModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  providers: [NeedsService]
})
export class NeedsModule { }
