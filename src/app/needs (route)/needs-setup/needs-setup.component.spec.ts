import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedsSetupComponent } from './needs-setup.component';

describe('NeedsSetupComponent', () => {
  let component: NeedsSetupComponent;
  let fixture: ComponentFixture<NeedsSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeedsSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedsSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
