import { Component, OnInit } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { NeedsService } from '../needs.service';
import { Observable } from 'rxjs';
import { Need } from '../need';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-needs-setup',
  templateUrl: './needs-setup.component.html',
  styleUrls: ['./needs-setup.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ height: 0, opacity: 0 }),
            animate('350ms ease-out', 
                    style({ height: 48, opacity: 1 }))
          ]
        ),
      ]
    ),
  ]
})
export class NeedsSetupComponent implements OnInit {
  $needs: Observable<any>;

  /*
    This component is used to set the needs (bisogni/valori/passioni)
    and modify them
  */
  constructor(private needSer: NeedsService) { }

  ngOnInit() {
    this.$needs = this.needSer.getNeedsList$();
  }

  /*
    - - - called from template - - - when drop the drag
    checks what is moved from and where and put things in right place
    then save all
  */
  drop(event: CdkDragDrop<any[]>) {
    const array = this.getTheRightArr(event.container.id);
    const previousArray = this.getTheRightArr(event.previousContainer.id);
    if (event.previousContainer === event.container) {
      array_move(array,  event.previousIndex, event.currentIndex);
      this.needSer.saveNeed();
    } else {
      array.splice(event.currentIndex, 0, previousArray[event.previousIndex]);
      previousArray.splice(event.previousIndex, 1);
      this.needSer.saveNeed();
    }
    // switch position of items in an array
    function array_move(arr: any[], old_index: number, new_index: number) {
      if (new_index >= arr.length) {
          let k = new_index - arr.length + 1;
          while (k--) {
            arr.push(undefined);
          }
      }
      arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    }
  }

  /*
    takes a string as argument and return the needsService array related to 'bisogni' | 'valori' | 'passioni'
  */
  getTheRightArr(type: string): any[] {
    let arr = [];
    if (type === 'bisogni') {
      arr = this.needSer.bisogni;
    } else if (type === 'valori') {
      arr = this.needSer.valori;
    } else if (type === 'passioni') {
      arr = this.needSer.passioni;
    }
    return arr;
  }

  /*
    - - - called from template - - - when click the new need cards
    add new empty item to array
  */
  addNewNeed(type: string) {
    const arr = this.getTheRightArr(type);
    arr.push({whyImportant: '', name: '' , idNeed: `need_${new Date().valueOf()}`, meaning: ''});
  }

  /* used from template to avoid the click to close expansion panel */
  preventClose(event) {
    event.stopPropagation();
  }

  /*
    - - - called from template - - - when close expansion panel
    delete utility values from item and save the BVP
  */
  saveNeed(item) {
    delete item.areYouSure;
    delete item.open;
    this.needSer.saveNeed();
  }

  /*
    - - - called from template - - -
    delete an item from array of BVP, then save it
    params:
    1) item: item to delete
    2) type: which array to delete from
    3) definitive: if yes => delete; if not => item.areYouSure? = true
    4) event is used to stop propagation of the click into the expansion panel
  */
  delete(item: Need, type: string, definitive: boolean, event) {
    event.stopPropagation();
    const arr = this.getTheRightArr(type);
    if (definitive) {
      const index = arr.findIndex(val => val.name == item.name && val.areYouSure);
      if (index >= 0) {
        arr.splice(index, 1);
        this.needSer.saveNeed();
      }
    } else {
      item.areYouSure = true;
    }
  }
}
