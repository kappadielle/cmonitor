import { FormGroup } from '@angular/forms';

// custom validator to check that two fields match
export function oiba() {
    return (formGroup: FormGroup) => {
        if (true) {
            return;
        }
        if ('no errors') {
            formGroup.setErrors(null);
        } else {
            formGroup.setErrors({ oiba: true });
        }
    }
}