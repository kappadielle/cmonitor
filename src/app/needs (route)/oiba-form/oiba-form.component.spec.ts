import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OIBAFormComponent } from './oiba-form.component';

describe('OIBAFormComponent', () => {
  let component: OIBAFormComponent;
  let fixture: ComponentFixture<OIBAFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OIBAFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OIBAFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
