import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';
import { Oiba } from './oiba (interface)';
import { NeedsService } from '../needs.service';
import { firestore } from 'firebase/app';
import { take, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MentalPattern } from 'src/app/mental-pattern/mental-pattern (interface)';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-oiba-form',
  templateUrl: './oiba-form.component.html',
  styleUrls: ['./oiba-form.component.scss'],
  animations: [
    trigger(
      'stepAnimation',
      [
        transition(
          ':enter', 
          [
            style({ height: 0, opacity: 0 }),
            animate('350ms ease-out', 
                    style({ height: 500, opacity: 1 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ height: 500, opacity: 1 }),
            animate('550ms ease-in', 
                    style({ height: 0, opacity: 0 }))
          ]
        )
      ]
    ),
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ left: '-500px', opacity: 0 }),
            animate('350ms ease-out',
                    style({ left: '-204px', opacity: 1 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ left: '-204px', opacity: 1 }),
            animate('350ms ease-in',
                    style({ left: '-500px', opacity: 0 }))
          ]
        )
      ]
    ),
    trigger(
      'newMentalAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ left: '400px', opacity: 1 }),
            animate('350ms ease-in',
                    style({ left: '0px', opacity: 0 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ left: '0px', opacity: 1 }),
            animate('350ms ease-in',
                    style({ left: '400px', opacity: 0 }))
          ]
        )
      ]
    ),
  ]
})
export class OIBAFormComponent implements OnInit {
  showNewMood = false; // show the input to add a new mood to the DB list
  showNewMentalPattern = false; // show the fields to add a new mental Pattern to the DB list

  // Forms
  macroForm: FormGroup;
  actionresultForm: FormGroup;
  alignmentForm: FormGroup;
  mentalPatternForm: FormGroup;

  // when emit, close the form and return to list, called with save and back btn
  @Output() doneWithForm = new EventEmitter<boolean>();

  oiba: Oiba; // obj used to fill the form

  needs: any;
  moodsNeg: any[];
  moodsPos: any[];
  mentalList: any;
  mentalListForSelect: any;

  newMentalPattern: MentalPattern = null; // used to store the new mental pattern created

  // subscribed from template, fill the selects in the form
  relationList$: Observable<any>;

  audioTitle = 'Audio Title';
  audioUrl = 'https://www.filepicker.io/api/file/1zU7eaIPRNGZP2zYvgcs';
  constructor(private formBuilder: FormBuilder, private needSer: NeedsService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.oiba = this.needSer.oibaCompilation;
    this.getMoods();
    this.getMentalPatterns();
    this.relationList$ = this.needSer.getRelationsList$().pipe(map(val => val.list));
    this.needs = [{list: this.needSer.bisogni, category: 'Bisogni'}, {list: this.needSer.valori, category: 'Valori'}, {list: this.needSer.passioni, category: 'Passioni'}];

    // if obj obtained is empty, fill manual

    // then fill every form control with ternary operator
    this.macroForm = this.formBuilder.group({
      situation: [this.oiba.situation ? this.oiba.situation : '', [
        Validators.required,
        Validators.minLength(6),
      ]],
      relations: [this.oiba.relations ? this.oiba.relations : [], []],
      moods: [this.oiba.moods ? this.oiba.moods : [], [
        Validators.required,
      ]],
      newMood: ['', [Validators.minLength(3)]],
      perception: [this.oiba.perception ? this.oiba.perception : '', [
        Validators.minLength(4),
      ]],
      needs: [this.oiba.needsId ? this.oiba.needsId : [], [
        Validators.required,
      ]],
    });
    this.actionresultForm = this.formBuilder.group({
      action: [this.oiba.action ? this.oiba.action : '', []],
      result: [this.oiba.result ? this.oiba.result : '', []],
      disadapted: [this.oiba.disadapted ? this.oiba.disadapted : false, []],
    });
    this.mentalPatternForm = this.formBuilder.group({
      mentalPattern: [this.oiba.mentalPatternID ? this.oiba.mentalPatternID : ''],
      newMentalPattern: ['', Validators.minLength(3)],
      categoryNewMentalPattern: [''],
    });
    this.alignmentForm = this.formBuilder.group({
      empathAlignment: [this.oiba.empathAlignment ? this.oiba.empathAlignment : '', []],
      perceptiveAlignment: [this.oiba.perceptiveAlignment ? this.oiba.perceptiveAlignment : '', []],
      mood_action_result2: [this.oiba.mood_action_result2 ? this.oiba.mood_action_result2 : '', []],
    }, {
      // validator: oiba()
    });
  }

  /*
    gets the moods from DB and merge them with local:
      - this.moodsPos
      - this.moodsNeg
  */
  private getMoods() {
    const dbMoods = this.needSer.getMoods$();
    dbMoods.pipe(take(1)).subscribe(obj => {
      this.moodsPos = obj.positive.concat(this.needSer.moodsPos);
      this.moodsNeg = obj.negative.concat(this.needSer.moodsNeg);
      this.moodsPos.map(val => ({display: val, value: val}))
      this.moodsNeg.map(val => ({display: val, value: val}))
    });
  }

  /*
    gets the mentalPatterns from DB and map them filling the mentalList variable
  */
  private getMentalPatterns() {
    const dbMentalPatterns = this.needSer.getMentalLightList$();
    dbMentalPatterns.pipe(take(1)).subscribe(obj => {
      this.mentalListForSelect = [
        { list: obj.begin,  categoria: 'Iniziare', category: 'begin' },
        { list: obj.reduce, categoria: 'Ridurre', category: 'reduce' },
        { list: obj.remove, categoria: 'Eliminare', category: 'remove' },
      ];
      this.mentalList = [ obj.begin, obj.reduce, obj.remove, obj.keep, obj.removed ];
    });
  }


  /*
    - - - called by template - - - 
    1) switch between addNewOne and select Moods
    2) add a new Mood when:
      - clicks the button
      - called with input dirty
    3) shows error when there's already a mood with the same name
    4) call the fn of the service used to save new Moods
  */
  openAddNewMood() {
    let arr = [];
    arr = this.oiba.positive ? this.moodsPos : this.moodsNeg;
    if (this.showNewMood) {
      if (this.macroF.newMood.value === '') { // if val is empty
        this.showNewMood = false;
        return;
      }
      if (this.macroF.newMood.valid) {
        if (!arr.some(el => el.value === this.macroF.newMood.value)  && this.macroF.newMood.value !== '') { // form is valid and newMood isn't in the list
          this.needSer.addNewMood({display: this.macroF.newMood.value.toUpperCase(), value: this.macroF.newMood.value.toUpperCase()});
          arr.push({display: this.macroF.newMood.value.toUpperCase(), value: this.macroF.newMood.value.toUpperCase()});
          this.oiba.moods.push(this.macroF.newMood.value.toUpperCase());
          this.macroF.newMood.setValue('');
          this.macroF.newMood.setErrors({alreadyIn: false});
          this.showNewMood = false;
        } else if (this.macroF.newMood.value !== '') { // if valid and it's not already in list
          this.macroF.newMood.setErrors({alreadyIn: true});
        }
      }
    } else {
      this.showNewMood = true; // show the input used to add new moods
    }
  }

  /*
    - - - called by template - - -
    1) switch between addNewOne and select MentalPattern
    2) add a new Mood when:
      - clicks the button
      - called with input & radio dirty
    3) shows error when there's already a Mental Pattern with the same name
    4) save the mental pattern only here in local
  */
  openAddNewMentalPattern() {
    if (this.showNewMentalPattern) {
      // check if forms are valide and if there's already a mentalPattern with that name
      if (this.mentalPatternF.newMentalPattern.value === '') { // if val is empty
        this.showNewMentalPattern = false;
        return;
      }
      let alreadyIn = false;

      alreadyIn = this.mentalList[0].some(val => val.name === this.mentalPatternF.newMentalPattern.value)
              || this.mentalList[1].some(val => val.name === this.mentalPatternF.newMentalPattern.value)
              || this.mentalList[2].some(val => val.name === this.mentalPatternF.newMentalPattern.value)
              || this.mentalList[3].some(val => val.name === this.mentalPatternF.newMentalPattern.value)
              || this.mentalList[4].some(val => val.name === this.mentalPatternF.newMentalPattern.value);
      if (this.mentalPatternF.newMentalPattern.valid) {
        if (alreadyIn) {
          this.mentalPatternF.newMentalPattern.setErrors({alreadyIn: true});
        } else if (!this.mentalPatternF.categoryNewMentalPattern.value) {
          this.mentalPatternF.newMentalPattern.setErrors({category: true});
        } else { // save just in local
          this.mentalListForSelect[this.mentalListForSelect.findIndex(val => val.category === this.mentalPatternF.categoryNewMentalPattern.value)].list.push({name: this.mentalPatternF.newMentalPattern.value, id: 'newMentalPattern'})
          this.newMentalPattern = {name: this.mentalPatternF.newMentalPattern.value, category: this.mentalPatternF.categoryNewMentalPattern.value, relations: this.macroF.relations.value ? this.macroF.relations.value : []};
          this.mentalPatternF.newMentalPattern.setValue('');
          this.mentalPatternF.mentalPattern.setValue('newMentalPattern');
          this.showNewMentalPattern = false;
        }
      }

    } else {
      this.showNewMentalPattern = true;
    }
  }


  /*
    - - - called by template - - - with save button
    it saves and prepare the oiba Obj to be saved
  */
  saveOiba() {
    this.backToList();

    // prepare the oiba Obj
    if (!this.oiba.date) {
      this.oiba.date = firestore.Timestamp.now();
    }
    this.oiba.lastTimeUpdated = firestore.Timestamp.now();
    this.oiba.situation = this.macroF.situation.value;
    this.oiba.relations = this.macroF.relations.value;
    this.oiba.moods = this.macroF.moods.value;

    this.oiba.needsId = this.macroF.needs.value;
    this.oiba.needs = [];
    // find the name related to the NeedsID
    this.oiba.needsId.forEach(idNeed => {
      let index = -1;
      let name = null;
      index = this.needSer.bisogni.findIndex(val => idNeed === val.idNeed);
      name = index >= 0 ? this.needSer.bisogni[index].name : name;
      index = this.needSer.valori.findIndex(val => idNeed === val.idNeed);
      name = index >= 0 ? this.needSer.valori[index].name : name;
      index = this.needSer.passioni.findIndex(val => idNeed === val.idNeed);
      name = index >= 0 ? this.needSer.passioni[index].name : name;
      this.oiba.needs.push(name);
    });

    if (!this.oiba.positive) {
      this.oiba.action = this.actionresultF.action.value;
      this.oiba.result = this.actionresultF.result.value;
      this.oiba.disadapted = this.actionresultF.disadapted.value;
      this.oiba.perception = this.macroF.perception.value;

      if (this.oiba.disadapted) {
        this.oiba.empathAlignment = this.alignmentF.empathAlignment.value;
        this.oiba.perceptiveAlignment = this.alignmentF.perceptiveAlignment.value;
        this.oiba.mood_action_result2 = this.alignmentF.mood_action_result2.value;
        console.log(this.oiba);
        if (this.newMentalPattern) {
          // calling them from here cause I need the id of the mentalPattern to set in the document
          this.needSer.addNewMentalPattern(this.newMentalPattern).then(ref => {
            this.needSer.addNewMentalPatternLightList(this.newMentalPattern, ref.id);
            this.oiba.mentalPatternID = ref.id;
            this.needSer.saveOiba(this.oiba);
          })
        } else if (this.mentalPatternF.mentalPattern.value) {
          // prendi sto id e controllo le relazioni e aggiornale
          this.oiba.mentalPatternID = this.mentalPatternF.mentalPattern.value;
          this.needSer.updateMentalPatternRelations(this.oiba.mentalPatternID, this.oiba.relations);
        }
      }
    }
    if (!this.newMentalPattern) { // prevent double saving when the new mental Pattern was saved for the first time
      this.needSer.saveOiba(this.oiba);
    }
  }

  /* emit via @Output to OibaList that the form should be closed */
  backToList() {
    this.doneWithForm.emit(false);
  }


  get macroF() { return this.macroForm.controls;  }
  get actionresultF() { return this.actionresultForm.controls; }
  get mentalPatternF() { return this.mentalPatternForm.controls; }
  get alignmentF() { return this.alignmentForm.controls; }

}