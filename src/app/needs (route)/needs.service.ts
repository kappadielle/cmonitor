import { Injectable } from '@angular/core';
import { DBManagerService } from '../services/dbmanager.service';
import { AuthenticationService } from '../services/authentication.service';
import { AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { take, tap, delay, map } from 'rxjs/operators';
import { Need } from './need';
import { moodsNeg, moodsPos} from './moodList';
import { Oiba } from './oiba-form/oiba (interface)';
import { firestore } from 'firebase/app';
import { MentalPatternService } from '../mental-pattern/mental-pattern.service';
import { StrategyService } from '../strategy/strategy.service';
import { MentalPattern } from '../mental-pattern/mental-pattern (interface)';
import * as moment from 'moment';
@Injectable()
export class NeedsService {

  /*
    Manage the needs data from DB
  */

  // documents to update
  docNeedsList: AngularFirestoreDocument<any>;
  docMoods: AngularFirestoreDocument<any>;
  colOibaList: AngularFirestoreCollection<any>;

  bisogni: Need[] = [];
  valori: Need[] = [];
  passioni: Need[] = [];

  // when oibaForm hits ngOnInit() this object will be taken
  oibaCompilation: Oiba = {positive: false, date: null, lastTimeUpdated: null, situation: '', moods: [], needs: [], needsId: []};

  moodsPos: any[] = moodsPos;
  moodsNeg: any[] = moodsNeg;

  oibaList: Oiba[] = []; // used to update the list without get document from DB. it's updated with saveOiba() and its reference is the same of oibalist-component.oibalist
  constructor(private DB: DBManagerService, private authSer: AuthenticationService, private mentalSer: MentalPatternService, private strategySer: StrategyService) { }

  /*
    return the observable of BVP and fill the local bisogni,valori,passioni
    with the DB value.
  */
  getNeedsList$(): Observable<any> {
    this.docNeedsList = this.DB.getDoc(`users/${this.authSer.userAuth.uid}/needs/BVP`);
    return this.DB.obsOfDoc(this.docNeedsList).pipe(
      take(1),
      tap(obj => {
        if (obj) {
          this.bisogni = obj.bisogni;
          this.valori = obj.valori;
          this.passioni = obj.passioni;
        }
      })
    );
  }

  // return the Observable of the oiba list queried for the date passed by argument
  // it also update the local variable "this.oibaList" for every emission
  getOibaList$(date: moment.Moment): Observable<any> {
    this.colOibaList = this.DB.getCollectionQueriesAndOrder(`users/${this.authSer.userAuth.uid}/needs/OIBA/list`, 'date', 'desc', 'date', '>=', date.startOf('day').toDate(), 'date', '<=', date.endOf('day').toDate());
    return this.DB.getCol$(this.colOibaList).pipe(
      take(1),
      tap(val => {this.oibaList = val;})
    );
  }

  getMentalLightList$(): Observable<any> {
    return this.mentalSer.getMentalPatternLightList$();
  }

  getRelationsList$(): Observable<any> {
    return this.strategySer.getRelations();
  }

  saveNeed() {
    this.DB.updateDoc(this.docNeedsList, {bisogni: this.bisogni, valori: this.valori, passioni: this.passioni});
  }

  /* save or modify an oiba and update the local list (this.oibaList) */
  saveOiba(oiba: Oiba) {
    if (oiba.id) {
      this.DB.updateDoc(`users/${this.authSer.userAuth.uid}/needs/OIBA/list/${oiba.id}`, oiba).then(_ => this.oibaList[this.oibaList.findIndex(el => el.id == oiba.id)] = oiba);
    } else {
      this.DB.addToCol(`users/${this.authSer.userAuth.uid}/needs/OIBA/list`, oiba).then(val => this.oibaList.unshift({...oiba, id: val.id}));
    }
  }


  getMoods$(): Observable<any> {
    this.docMoods = this.DB.getDoc(`users/${this.authSer.userAuth.uid}/needs/moods`);
    return this.DB.obsOfDoc(this.docMoods);
  }


  addNewMood(item) {
    if (this.oibaCompilation.positive) {
      this.DB.updateDoc(this.docMoods, {positive: firestore.FieldValue.arrayUnion(item)})
    } else {
      this.DB.updateDoc(this.docMoods, {negative: firestore.FieldValue.arrayUnion(item)})
    }
  }

  addNewMentalPattern(newMentalPattern: MentalPattern): any {
    return this.mentalSer.addNewMentalPattern(newMentalPattern);
  }
  addNewMentalPatternLightList(newMentalPattern: MentalPattern, id: string) {
    this.mentalSer.addNewMentalPatternLightList(newMentalPattern, id);
  }
  updateMentalPatternRelations(id: string, relations: string[]) {
    this.mentalSer.updateMentalPatternRelations(id, relations);
  }

}
