export const moodsNeg = [
    { display: 'A DISAGIO', value: 'A DISAGIO' },
    { display: 'FIACCO', value: 'FIACCO' },
    { display: 'PIENO DI VERGOGNA', value: 'PIENO DI VERGOGNA' },
    { display: 'ABBATTUTO', value: 'ABBATTUTO' },
    { display: 'FREDDO', value: 'FREDDO' },
    { display: 'PREOCCUPATO', value: 'PREOCCUPATO' },
    { display: 'ADDOLORATO', value: 'ADDOLORATO' },
    { display: 'FRUSTRATO', value: 'FRUSTRATO' },
    { display: 'RACCAPRICCIATO', value: 'RACCAPRICCIATO' },
    { display: 'ADIRATO', value: 'ADIRATO' },
    { display: 'FURIBONDO', value: 'FURIBONDO' },
    { display: 'RAMMARICATO', value: 'RAMMARICATO' },
    { display: 'AFFATICATO', value: 'AFFATICATO' },
    { display: 'FURIOSO', value: 'FURIOSO' },
    { display: 'RATTRISTATO', value: 'RATTRISTATO' },
    { display: 'AFFLITTO', value: 'AFFLITTO' },
    { display: 'GELOSO', value: 'GELOSO' },
    { display: 'RETICENTE', value: 'RETICENTE' },
    { display: 'AFFRANTO', value: 'AFFRANTO' },
    { display: 'IMBARAZZATO', value: 'IMBARAZZATO' },
    { display: 'RILUTTANTE', value: 'RILUTTANTE' },
    { display: 'AGITATO', value: 'AGITATO' },
    { display: 'IMPACCIATO', value: 'IMPACCIATO' },
    { display: 'RISENTITO', value: 'RISENTITO' },
    { display: 'ALLARMATO', value: 'ALLARMATO' },
    { display: 'IMPAURITO', value: 'IMPAURITO' },
    { display: 'SBIGOTTITO', value: 'SBIGOTTITO' },
    { display: 'AMAREGGIATO', value: 'AMAREGGIATO' },
    { display: 'IMPAZIENTE', value: 'IMPAZIENTE' },
    { display: 'SCETTICO', value: 'SCETTICO' },
    { display: 'ANGOSCIATO', value: 'ANGOSCIATO' },
    { display: 'IMPENSIERITO', value: 'IMPENSIERITO' },
    { display: 'SCIOCCATO', value: 'SCIOCCATO' },
    { display: 'ANNOIATO', value: 'ANNOIATO' },
    { display: 'INAPPAGATO', value: 'INAPPAGATO' },
    { display: 'SCOCCIATO', value: 'SCOCCIATO' },
    { display: 'ANSIOSO', value: 'ANSIOSO' },
    { display: 'INASPRITO', value: 'INASPRITO' },
    { display: 'SCONSOLATO', value: 'SCONSOLATO' },
    { display: 'APATICO', value: 'APATICO' },
    { display: 'INCERTO', value: 'INCERTO' },
    { display: 'SCONTENTO', value: 'SCONTENTO' },
    { display: 'ARRABBIATO', value: 'ARRABBIATO' },
    { display: 'INCURANTE', value: 'INCURANTE' },
    { display: 'SCONVOLTO', value: 'SCONVOLTO' },
    { display: 'ASSONNATO', value: 'ASSONNATO' },
    { display: 'INDIFESO', value: 'INDIFESO' },
    { display: 'SCORAGGIATO', value: 'SCORAGGIATO' },
    { display: 'ATTERRITO', value: 'ATTERRITO' },
    { display: 'INDIFFERENTE', value: 'INDIFFERENTE' },
    { display: 'SCOSSO', value: 'SCOSSO' },
    { display: 'AVVERSO', value: 'AVVERSO' },
    { display: 'INFASTIDITO', value: 'INFASTIDITO' },
    { display: 'SECCATO', value: 'SECCATO' },
    { display: 'AVVILITO', value: 'AVVILITO' },
    { display: 'INFELICE', value: 'INFELICE' },
    { display: 'SENZA ENERGIA', value: 'SENZA ENERGIA' },
    { display: 'CONFUSO', value: 'CONFUSO' },
    { display: 'INFERVORATO', value: 'INFERVORATO' },
    { display: 'SFIDUCIATO', value: 'SFIDUCIATO' },
    { display: 'CONTRARIATO', value: 'CONTRARIATO' },
    { display: 'INORRIDITO', value: 'INORRIDITO' },
    { display: 'SFINITO', value: 'SFINITO' },
    { display: 'COSTERNATO', value: 'COSTERNATO' },
    { display: 'INQUIETO', value: 'INQUIETO' },
    { display: 'SGOMENTO', value: 'SGOMENTO' },
    { display: 'CUPO', value: 'CUPO' },
    { display: 'INSICURO', value: 'INSICURO' },
    { display: 'SNERVATO', value: 'SNERVATO' },
    { display: 'DELUSO', value: 'DELUSO' },
    { display: 'INSODDISFATTO', value: 'INSODDISFATTO' },
    { display: 'SOLO', value: 'SOLO' },
    { display: 'DEMORALIZZATO', value: 'DEMORALIZZATO' },
    { display: 'INVIDIOSO', value: 'INVIDIOSO' },
    { display: 'SOPRAFFATTO', value: 'SOPRAFFATTO' },
    { display: 'DEPRESSO', value: 'DEPRESSO' },
    { display: 'IRREQUIETO', value: 'IRREQUIETO' },
    { display: 'SORPRESO', value: 'SORPRESO' },
    { display: 'DI MALUMORE', value: 'DI MALUMORE' },
    { display: 'IRRITABILE', value: 'IRRITABILE' },
    { display: 'SOSPETTOSO', value: 'SOSPETTOSO' },
    { display: 'DIFFIDENTE', value: 'DIFFIDENTE' },
    { display: 'IRRITATO', value: 'IRRITATO' },
    { display: 'SPAVENTATO', value: 'SPAVENTATO' },
    { display: 'DISGUSTATO', value: 'DISGUSTATO' },
    { display: 'LETARGICO', value: 'LETARGICO' },
    { display: 'STANCO', value: 'STANCO' },
    { display: 'DISILLUSO', value: 'DISILLUSO' },
    { display: 'MALINCONICO', value: 'MALINCONICO' },
    { display: 'STORDITO', value: 'STORDITO' },
    { display: 'DISINTERESSATO', value: 'DISINTERESSATO' },
    { display: 'MESTO', value: 'MESTO' },
    { display: 'STRAZIATO', value: 'STRAZIATO' },
    { display: 'DISPERATO', value: 'DISPERATO' },
    { display: 'NERVOSO', value: 'NERVOSO' },
    { display: 'STRESSATO', value: 'STRESSATO' },
    { display: 'DISPIACIUTO', value: 'DISPIACIUTO' },
    { display: 'OSTILE', value: 'OSTILE' },
    { display: 'STUFO', value: 'STUFO' },
    { display: 'DISTACCATO', value: 'DISTACCATO' },
    { display: 'PENSIEROSO', value: 'PENSIEROSO' },
    { display: 'SUSCETTIBILE', value: 'SUSCETTIBILE' },
    { display: 'DOLENTE', value: 'DOLENTE' },
    { display: 'PERPLESSO', value: 'PERPLESSO' },
    { display: 'SVOGLIATO', value: 'SVOGLIATO' },
    { display: 'DUBBIOSO', value: 'DUBBIOSO' },
    { display: 'PERSO D\'ANIMO', value: 'PERSO D\'ANIMO' },
    { display: 'TESO', value: 'TESO' },
    { display: 'ESASPERATO', value: 'ESASPERATO' },
    { display: 'PESSIMISTA', value: 'PESSIMISTA' },
    { display: 'TETRO', value: 'TETRO' },
    { display: 'ESAUSTO', value: 'ESAUSTO' },
    { display: 'PIENO DI PAURA', value: 'PIENO DI PAURA' },
    { display: 'TIMOROSO', value: 'TIMOROSO' },
    { display: 'FEBBRILE', value: 'FEBBRILE' },
    { display: 'PIENO DI RANCORE', value: 'PIENO DI RANCORE' },
    { display: 'TITUBANTE', value: 'TITUBANTE' },
    { display: 'TRISTE', value: 'TRISTE' },
    { display: 'TURBATO', value: 'TURBATO' },
    { display: 'VERGOGNOSO', value: 'VERGOGNOSO' },
]
export const moodsPos = [
    { display: 'A MIO  AGIO', value: 'A MIO AGIO' },
    { display: 'GIUBILANTE', value: 'GIUBILANTE' },
    { display: 'TOCCATO', value: 'TOCCATO' },
    { display: 'AFFASCINATO', value: 'AFFASCINATO' },
    { display: 'GLORIOSO', value: 'GLORIOSO' },
    { display: 'TRANQUILLO', value: 'TRANQUILLO' },
    { display: 'AFFETTUOSO', value: 'AFFETTUOSO' },
    { display: 'GRATO', value: 'GRATO' },
    { display: 'TURBATO', value: 'TURBATO' },
    { display: 'ALLEGRO', value: 'ALLEGRO' },
    { display: 'IMMERSO', value: 'IMMERSO' },
    { display: 'VIGILE', value: 'VIGILE' },
    { display: 'AMICHEVOLE', value: 'AMICHEVOLE' },
    { display:'IN  ARMONIA', value: 'IN ARMONIA' },
    { display: 'VIVACE', value: 'VIVACE' },
    { display: 'AMMALIATO', value: 'AMMALIATO' },
    { display:'IN  PACE', value: 'IN PACE' },
    { display: 'VIVO', value: 'VIVO' },
    { display: 'AMOREVOLE', value: 'AMOREVOLE' },
    { display: 'INCANTATO', value: 'INCANTATO' },
    { display: 'APPAGATO', value: 'APPAGATO' },
    { display: 'INCURIOSITO', value: 'INCURIOSITO' },
    { display: 'APPASSIONATO', value: 'APPASSIONATO' },
    { display: 'INTERNERITO', value: 'INTERNERITO' },
    { display: 'ASSORTO', value: 'ASSORTO' },
    { display: 'INTERESSATO', value: 'INTERESSATO' },
    { display: 'ATTENTO', value: 'ATTENTO' },
    { display: 'ISPIRATO', value: 'ISPIRATO' },
    { display: 'AUDACE', value: 'AUDACE' },
    { display: 'LIBERO', value: 'LIBERO' },
    { display: 'BALDANZOSO', value: 'BALDANZOSO' },
    { display: 'LIETO', value: 'LIETO' },
    { display: 'BEATO', value: 'BEATO' },
    { display: 'MERAVIGLIATO', value: 'MERAVIGLIATO' },
    { display: 'BENDISPOSTO', value: 'BENDISPOSTO' },
    { display: 'ORGOGLIOSO', value: 'ORGOGLIOSO' },
    { display: 'BRILLANTE', value: 'BRILLANTE' },
    { display: 'OTTIMISTA', value: 'OTTIMISTA' },
    { display: 'BRIOSO', value: 'BRIOSO' },
    { display: 'PACIFICO', value: 'PACIFICO' },
    { display: 'CALMO', value: 'CALMO' },
    { display: 'AMMIRATO', value: 'AMMIRATO' },
    { display: 'CALOROSO', value: 'CALOROSO' },
    { display: 'PIENO DI  ENERGIA', value: 'PIENO DI ENERGIA' },
    { display: 'COINVOLTO', value: 'COINVOLTO' },
    { display: 'PLACIDO', value: 'PLACIDO' },
    { display: 'COMMOSSO', value: 'COMMOSSO' },
    { display: 'RAGGIANTE', value: 'RAGGIANTE' },
    { display: 'COMPIACIUTO', value: 'COMPIACIUTO' },
    { display: 'RALLEGRATO', value: 'RALLEGRATO' },
    { display: 'CONTENTO', value: 'CONTENTO' },
    { display: 'RAPITO', value: 'RAPITO' },
    { display: 'CURIOSO', value: 'CURIOSO' },
    { display: 'RASSERENATO', value: 'RASSERENATO' },
    { display: 'DELIZIATO', value: 'DELIZIATO' },
    { display: 'RICONOSCENTE', value: 'RICONOSCENTE' },
    { display:'DI BUON  UMORE', value: 'DI BUON UMORE' },
    { display: 'RILASSATO', value: 'RILASSATO' },
    { display: 'DIVERTITO', value: 'DIVERTITO' },
    { display: 'RINFORZATO', value: 'RINFORZATO' },
    { display: 'ECCITATO', value: 'ECCITATO' },
    { display: 'RINFRESCATO', value: 'RINFRESCATO' },
    { display: 'EFFERVESCENTE', value: 'EFFERVESCENTE' },
    { display: 'RISTORATO', value: 'RISTORATO' },
    { display: 'ELETTRIZZATO', value: 'ELETTRIZZATO' },
    { display: 'RISVEGLIATO', value: 'RISVEGLIATO' },
    { display: 'ENERGIZZATO', value: 'ENERGIZZATO' },
    { display: 'SENSIBILE', value: 'SENSIBILE' },
    { display: 'ENTUSIASTA', value: 'ENTUSIASTA' },
    { display: 'SERENO', value: 'SERENO' },
    { display: 'ESTASIATO', value: 'ESTASIATO' },
    { display: 'SFAVILLANTE', value: 'SFAVILLANTE' },
    { display: 'ESUBERANTE', value: 'ESUBERANTE' },
    { display: 'SICURO', value: 'SICURO' },
    { display: 'ESULTANTE', value: 'ESULTANTE' },
    { display: 'SODDISFATTO', value: 'SODDISFATTO' },
    { display: 'FELICE', value: 'FELICE' },
    { display: 'SOLLEVATO', value: 'SOLLEVATO' },
    { display: 'FESTOSO', value: 'FESTOSO' },
    { display: 'SORPRESO', value: 'SORPRESO' },
    { display: 'FRIZZANTE', value: 'FRIZZANTE' },
    { display: 'SPENSIERATO', value: 'SPENSIERATO' },
    { display: 'GAIO', value: 'GAIO' },
    { display: 'SPERANZOSO', value: 'SPERANZOSO' },
    { display: 'GIOIOSO', value: 'GIOIOSO' },
    { display: 'STUPITO', value: 'STUPITO' },
]
