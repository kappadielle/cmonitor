import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OIBAListComponent } from './oiba-list.component';

describe('OIBAListComponent', () => {
  let component: OIBAListComponent;
  let fixture: ComponentFixture<OIBAListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OIBAListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OIBAListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
