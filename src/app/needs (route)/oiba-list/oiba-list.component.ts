import { Component, OnInit } from '@angular/core';
import { NeedsService } from '../needs.service';
import { Oiba } from '../oiba-form/oiba (interface)';
import { trigger, transition, style, animate } from '@angular/animations';
import { Observable } from 'rxjs';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@Component({
  selector: 'app-oiba-list',
  templateUrl: './oiba-list.component.html',
  styleUrls: ['./oiba-list.component.scss'],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    {provide: MAT_DATE_LOCALE, useValue: 'it'},

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
  animations: [
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ transform: 'translateY(300px)', opacity: 0 }),
            animate('350ms ease-out',
                    style({ transform: 'translateY(0px)', opacity: 1 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ transform: 'translateY(0px)', opacity: 1 }),
            animate('550ms ease-out',
                    style({ transform: 'translateY(600px)', opacity: 0 }))
          ]
        )
      ]
    ),
    trigger(
      'inOutAnimationFromCard',
      [
        transition(
          ':enter',
          [
            style({ height: 0, opacity: 0 }),
            animate('350ms ease-out',
                    style({ height: 65, opacity: 1 }))
          ]
        ),
      ]
    ),
    trigger(
      'inOutAnimationFromNewOiba',
      [
        transition(
          ':enter',
          [
            style({ height: 0, opacity: 0 }),
            animate('350ms ease-out',
                    style({ height: 92, opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ height: 92, opacity: 1 }),
            animate('350ms ease-out',
                    style({ height: 0, opacity: 0 }))
          ]
        ),
      ]
    )
  ]
})
export class OIBAListComponent implements OnInit {
  /*
    Shows the list of the last OIBA compiled,
    and manage the presence of Oiba-form.component
  */

  oibaDateList = moment(); // date used to query the list
  today = moment().endOf('day');
  editorOpen = false; // used to pick what part should be displayed in template
  oibaList: Oiba[] = []; // list of oiba that has the same reference to needSer.oibaList
  oibaList$: Observable<Oiba[]>; // list of oiba
  constructor(private needSer: NeedsService) { }

  ngOnInit() {
    this.needSer.getOibaList$(this.oibaDateList).subscribe(val => {this.oibaList = this.needSer.oibaList; }); // bond the oibaList to the service.oibaList to make it upgradable when a new Oiba is added in local
  }

  /*
    - - - called by template - - - when oiba-form.component emits the value that says to close
  */
  doneWithForm(event: boolean) {
    this.editorOpen = event;
  }

  /*
    - - - called by template - - - when clicking on:
      - new OIBA => item will be {positive: val}
      - modify new Oiba => item will be {}: Oiba
  */
  openOibaEditor(item: Oiba) {
    this.needSer.oibaCompilation = item;
    this.editorOpen = true;
  }

  changeOibaDateList(event: _moment.Moment, op?: string) {
    if (op === '+') {
      event = this.oibaDateList.add(1, 'day');
    } else if (op === '-') {
      event = this.oibaDateList.subtract(1, 'day');
    }
    this.oibaDateList = event.clone();
    this.needSer.getOibaList$(this.oibaDateList).subscribe(val => this.oibaList = this.needSer.oibaList)
    // this.oibaList$.subscribe()
  }
}
