import { TestBed, inject } from '@angular/core/testing';
import { AuthGuard } from './auth.guard';
import { Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

describe('AuthGuard', () => {
  let authSer;
  let aGuard;
  let router;

  const stubAuthenticationService = {
    userAuth: Boolean,
    redirectUrl: String
  };

  // stub parameters for canActivate(route, state)
  const createMockRoute = (id: string) => {
    return {
      // tslint:disable-next-line:object-literal-shorthand
      params: { id: id }
    } as any;
  };
  const createMockRouteState = () => ({url: '/todoList'} as RouterStateSnapshot);

  const route = createMockRoute(null);
  const state = createMockRouteState();

  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard,
        { provide: AuthenticationService, useValue: stubAuthenticationService },
        { provide: Router, useValue: routerSpy }
      ]
    });
    authSer = TestBed.get(AuthenticationService);
    aGuard = TestBed.get(AuthGuard);
    router = TestBed.get(Router);
  });

  it('should create', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  describe('When user authenticated', () => {
    it('should navigate when user authenticated', () => {
      authSer.userAuth = true;
      const activated = aGuard.canActivate(route, state);
      expect(activated).toBe(true);
    });
  });

  describe('when user NOT authenticated', () => {
    it('should NOT navigate when user NOT authenticated', () => {
      authSer.userAuth = false;
      const activated = aGuard.canActivate(route, state);
      expect(activated).toBe(false);
    });
    it('should save current URL cause it redirect', () => {
      authSer.userAuth = false;
      aGuard.canActivate(route, state);
      expect(authSer.redirectUrl).toBe(state.url);
    });
    it('should redirect to /home when user NOT authenticated', () => {
      authSer.userAuth = false;
      aGuard.canActivate(route, state);
      expect(router.navigate).toHaveBeenCalledWith(['/home']);
    });
  });
});
