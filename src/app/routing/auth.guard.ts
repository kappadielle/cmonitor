import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  /*
    manage the permission for navigating in the website
  */
  constructor(private authSer: AuthenticationService, private router: Router) {}

  /*
    1) it works with authenticationService.authSer // state of authentication

    2) listen to urlChanges and prevent user to navigate in no authorized routes
      check if a user is logged for every url change and if noone is logged in,
      it redirect to /home saving the current route for autoLogged users
  */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (!this.authSer.userAuth) {
      this.authSer.redirectUrl = state.url; // when redirect saves the current url
      this.router.navigate(['/home']);
      return false; // NOT authorized
    }
    return true; // authorized
  }
}
