import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { QuicklinkStrategy, QuicklinkModule } from 'ngx-quicklink';
import { TodoListComponent } from '../todolist (route)/todo-list/todo-list.component';
import { HomeComponent } from '../home (route)/home/home.component';
import { ProfileComponent } from '../profile (route)/profile/profile.component';
import { NeedsComponent } from '../needs (route)/needs/needs.component';
import { StrategyComponent } from '../strategy/strategy/strategy.component';
import { MonitorSetupComponent } from '../monitor (route)/monitor-setup/monitor-setup.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},

  // home and profile don't have [authGuard] 'cause a user can always navigate there
  { path: 'home', component: HomeComponent, data: { title: 'home' }},
  { path: 'profile', component: ProfileComponent, data: { title: 'profile' }},
  { path: 'todoList', component: TodoListComponent, data: { title: 'todoList' }, canActivate: [AuthGuard]},
  { path: 'OIBA', component: NeedsComponent, data: { title: 'OIBA' }, canActivate: [AuthGuard]},
  { path: 'monitor/:subject', component: MonitorSetupComponent, data: { title: 'monitor' }, canActivate: [AuthGuard]},
  { path: 'strategy', component: StrategyComponent, data: { title: 'strategy' }, canActivate: [AuthGuard]},
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [
    // RouterModule.forRoot(routes),
    RouterModule.forRoot(routes, { preloadingStrategy: QuicklinkStrategy }),
    QuicklinkModule
  ],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class AppRoutingModule { }
