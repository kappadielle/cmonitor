import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { AuthenticationService } from '../services/authentication.service';
import { MaterialModule } from '../material/material.module';
import { Component, Input } from '@angular/core';
import { SharedEmail } from '../interfaces/shared-email';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let authSer;

  beforeEach(async(() => {
    const authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['logout']);
    TestBed.configureTestingModule({
      imports: [MaterialModule],
      declarations: [ ProfileComponent, StubSignupComponent, StubLoginComponent ],
      providers: [
        { provide: AuthenticationService, useValue: authServiceSpy }
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authSer = TestBed.get(AuthenticationService);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('call logout', () => {
    component.logout();
    expect(authSer.logout).toHaveBeenCalled();
  });

});


@Component({selector: 'app-login', template: ''})
class StubLoginComponent {
  @Input() sharedEmail: SharedEmail;
}

@Component({selector: 'app-signup', template: ''})
class StubSignupComponent {
  @Input() sharedEmail: SharedEmail;
}

