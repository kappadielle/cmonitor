import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { SharedEmail } from 'src/app/home (route)/shared-email (interface)';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  /*
    1) manage the login/signUp of users
    2) profile and side settings
  */
  showLoginIfEntryDirectly = true;
  sharedEmail: SharedEmail; // object shared between profile and home that contains: {email & !!userExist}. it comes from HOME->emailField
  $userAuth: Observable<any>; // observable which contains the state of auth
  $subDestroyer: Subject<boolean> = new Subject<boolean>();
  constructor(private authSer: AuthenticationService) { }

  ngOnInit() {
    this.$userAuth = this.authSer.$userAuth;
    this.sharedEmail = this.authSer.sharedEmail;
  }
  logout() {
    this.authSer.logout();
  }
  ngOnDestroy() {
    // this.$subDestroyer.next(); // trigger the takeUntil($subDestroyer) => unsubscribe()
  }
}
