import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { SharedEmail } from 'src/app/home (route)/shared-email (interface)';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  /*
    manage login of user
  */

  @Input() sharedEmail: SharedEmail;

  loginUserForm: FormGroup;
  showPassword = false; // form
  loginError = false; // it's used to display an error when the try of login fail
  constructor(private formBuilder: FormBuilder, private authSer: AuthenticationService) { }

  ngOnInit() {
    this.loginUserForm = this.formBuilder.group({
      email: [(this.sharedEmail ? this.sharedEmail.email : ''), [
        Validators.required,
        // tslint:disable-next-line:max-line-length
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
      ]],
      password: ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25)
      ]],
    }, {
      // updateOn: 'change'
    });
  }

  // convenience getter for easy access to form fields from HTML
  get f() { return this.loginUserForm.controls; }

  /*
    passthroughMethod that uses the esit of the logintry()
      / it fill the class variable loginError if there's an error
      / log the user if all ok
      / prerequisite: form MUST be valid
  */
  emailLogin() {
    if (this.loginUserForm.valid) {
      const email = this.loginUserForm.value.email;
      const password = this.loginUserForm.value.password;
      this.authSer.emailLogin(email, password).catch(err => {
        console.error('error: bad credentials');
        this.loginError = true;
      });
    }
  }
}
