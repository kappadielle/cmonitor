import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material/material.module';
import { click } from 'src/app/testing/clickHelper';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authSer;
  const user = {
    email: 'abc@123.it',
    password: 'Ciaociao1'
  };

  beforeEach(async(() => {
    const authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['emailLogin']);
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule,  MaterialModule
      ],
      declarations: [ LoginComponent ],
      providers: [
                  { provide: AuthenticationService, useValue: authServiceSpy }
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authSer = TestBed.get(AuthenticationService);
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('FORM testing', () => {
    let email;
    let password;
    let button;

    beforeEach(() => {
      email = fixture.debugElement.nativeElement.querySelector('input[type=\'email\']');
      password = fixture.debugElement.nativeElement.querySelector('input[formControlName=\'password\']');
      button = fixture.debugElement.nativeElement.querySelector('button[type=\'submit\']');
    });

    it('Button disabled with invalid/No Email', () => {
      email.value = 'ciao.it';
      email.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
      email.value = '';
      email.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
    });
    it('Button disabled with invalid/No password', () => {
      password.value = 'ciao';
      password.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
      password.value = '';
      password.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
    });
    it('Button available with validate Email & Password', () => {
      email.value = user.email;
      email.dispatchEvent(new Event('input'));
      password.value = user.password;
      password.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(false);
    });
    it('Call the function after click, with the right args', () => {
      email.value = user.email;
      email.dispatchEvent(new Event('input'));
      password.value = user.password;
      password.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      click(button);
      expect(authSer.emailLogin).toHaveBeenCalledWith(user.email, user.password);
    });
    it('Show error if error comes', () => {
      component.loginError = true;
      fixture.detectChanges();
      const errorParag = fixture.debugElement.nativeElement.querySelector('p.error');
      expect(errorParag.offsetWidth).toBeGreaterThan(0);
      expect(errorParag.offsetHeight).toBeGreaterThan(0);
    });
  });
});
