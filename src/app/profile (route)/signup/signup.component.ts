import { Component, OnInit, Input } from '@angular/core';
import { SharedEmail } from 'src/app/home (route)/shared-email (interface)';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MustMatch } from './must-match.validator';
import { DBManagerService } from 'src/app/services/dbmanager.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  /*
    manage signUp of user
  */

  @Input() sharedEmail: SharedEmail;

  signupUserForm: FormGroup;
  showPassword = false; // form
  signUpError = false; // it's used to display an error when the try of signUp fail
  constructor(private formBuilder: FormBuilder, private authSer: AuthenticationService, private DB: DBManagerService, private route: Router) { }

  ngOnInit() {
    this.signupUserForm = this.formBuilder.group({
      email: [(this.sharedEmail ? this.sharedEmail.email : ''), [
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
      ]],
      password: ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25)
      ]],
      confirmPassword: ['', Validators.required]
    }, {
      // updateOn: 'change', se lo metti non funge validator
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  // convenience getter for easy access to form fields from HTML
  get f() { return this.signupUserForm.controls; }

  /*
    passthroughMethod that uses the esit of the signUptry()
      / it fill the class variable signUpError is there's an error
      / signup the user if all ok
      / prerequisite: form MUST be valid
  */
  emailSignUp() {
    if (this.signupUserForm.valid) {
      const email = this.signupUserForm.value.email;
      const password = this.signupUserForm.value.password;
      this.authSer.emailSignUp(email, password)
      .then(val => {
        // create user & collection here because creating them in Gfunctions  is too slow/have no callback  to redirect at home when data is ready
        const doc = this.DB.getCol('users').doc(val.user.uid);
        const prom0 = doc.set({email});
        const prom1 = doc.collection('outlines').doc('container_Task').set({list: []});
        const prom2 = doc.collection('outlines').doc('pensieri_7_distrazioni').set({list: []});
        const prom3 = doc.collection('planning').doc('routine').set({todolist: []});
        const prom4 = doc.collection('needs').doc('BVP').set({bisogni: [], valori: [], passioni: []});
        const prom5 = doc.collection('needs').doc('moods').set({positive: [], negative: []});
        const prom6 = doc.collection('mentalPattern').doc('list').set({reduce: [], begin: [], remove: [], removed: [], keep: []});
        const prom7 = doc.collection('strategy').doc('relatons').set({list: []});
        Promise.all([prom0, prom1, prom2, prom3, prom4, prom5, prom6]).then(_ => {
          this.route.navigate(['/home']);
        });
      })
      .catch(err => {
        console.error('error: user already exists');
        this.signUpError = true;
      });
    }
  }

}
