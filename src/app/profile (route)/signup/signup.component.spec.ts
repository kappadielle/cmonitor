import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupComponent } from './signup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { click } from 'src/app/testing/clickHelper';
import { Router } from '@angular/router';
import { DBManagerService } from 'src/app/services/dbmanager.service';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let authSer;
  const user = {
    email: 'abc@123.it',
    password: 'Ciaociao1'
  };

  beforeEach(async(() => {
    const authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['emailSignUp']);
    const RouterSpy = jasmine.createSpyObj('Router', ['navigate']);
    const DBServiceSpy = jasmine.createSpyObj('DBManagerService', ['getCollection']);
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, MaterialModule],
      declarations: [ SignupComponent ],
      providers: [
                  { provide: AuthenticationService, useValue: authServiceSpy },
                  { provide: Router, useValue: RouterSpy },
                  { provide: DBManagerService, useValue: DBServiceSpy }
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authSer = TestBed.get(AuthenticationService);
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('FORM testing', () => {
    let email;
    let password;
    let Cpassword;
    let button;

    beforeEach(() => {
      email = fixture.debugElement.nativeElement.querySelector('input[type=\'email\']');
      password = fixture.debugElement.nativeElement.querySelector('input[formControlName=\'password\']');
      Cpassword = fixture.debugElement.nativeElement.querySelector('input[formControlName=\'confirmPassword\']');
      button = fixture.debugElement.nativeElement.querySelector('button[type=\'submit\']');
    });

    it('Button disabled with invalid/No Email', () => {
      email.value = 'ciao.it';
      email.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
      email.value = '';
      email.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
    });
    it('Button disabled with invalid/No password', () => {
      password.value = 'ciao';
      password.dispatchEvent(new Event('input'));
      Cpassword.value = 'ciao';
      Cpassword.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
      password.value = '';
      password.dispatchEvent(new Event('input'));
      Cpassword.value = '';
      Cpassword.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
    });
    it('passwords MUST match', () => {
      email.value = user.email;
      email.dispatchEvent(new Event('input'));
      password.value = user.password;
      password.dispatchEvent(new Event('input'));
      Cpassword.value = user.password + 'a';
      Cpassword.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(true);
    });
    it('Button available with validate Email & Password & ConfirmPassword', () => {
      email.value = user.email;
      email.dispatchEvent(new Event('input'));
      password.value = user.password;
      password.dispatchEvent(new Event('input'));
      Cpassword.value = user.password;
      Cpassword.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(button.disabled).toBe(false);
    });
    it('Call the function after click, with the right args', () => {
      email.value = user.email;
      email.dispatchEvent(new Event('input'));
      password.value = user.password;
      password.dispatchEvent(new Event('input'));
      Cpassword.value = user.password;
      Cpassword.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      click(button);
      expect(authSer.emailSignUp).toHaveBeenCalledWith(user.email, user.password);
    });
    it('Show error if error comes', () => {
      component.signUpError = true;
      fixture.detectChanges();
      const errorParag = fixture.debugElement.nativeElement.querySelector('p.error');
      expect(errorParag.offsetWidth).toBeGreaterThan(0);
      expect(errorParag.offsetHeight).toBeGreaterThan(0);
    });
  });
});
